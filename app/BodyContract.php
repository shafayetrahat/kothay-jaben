<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BodyContract extends Model
{
    protected $fillable = [
    	'body_contract_location',
	];
}
