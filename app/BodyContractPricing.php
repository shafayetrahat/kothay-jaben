<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BodyContractPricing extends Model
{
    protected $fillable = [
		'body_contract_location',
		'car_name',
		'rental_price',
	];
}
