<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $fillable = [
    	'car_name',
	    'car_details',
	    'car_feature_image'
    ];

    public function photos()
    {
    	return $this->hasMany('App\CarPhoto');
    }
}
