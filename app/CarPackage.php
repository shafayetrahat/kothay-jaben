<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarPackage extends Model
{
    protected $fillable = [
        'package_name',
        'previous_price',
        'current_price',
        'car_name',
        'package_image',
        'package_details'
    ];

    public function carPhotos()
    {
        return $this->hasMany('App\CarPackagePhoto');
    }
}
