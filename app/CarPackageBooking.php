<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarPackageBooking extends Model
{
    protected $fillable = [
        'car_package_id',
		'confirmed',
        'car_name',
        'car_rent',
        'check_in',
        'people',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
