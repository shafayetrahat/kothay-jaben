<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarPackagePhoto extends Model
{
    protected $fillable = [
        'photos'
    ];

    public function carPackage()
    {
        return $this->belongsTo('App\CarPackage');
    }

    public function delete()
    {
        \File::delete([
            $this->photos
        ]);

        parent::delete();
    }
}
