<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarPhoto extends Model
{
	protected $fillable = ['photos'];

    public function car()
    {
    	return $this->belongsTo('App\Car');
    }

	public function delete()
	{
		\File::delete([
			$this->photos
		]);

		parent::delete();
	}
}
