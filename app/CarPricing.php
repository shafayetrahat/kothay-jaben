<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarPricing extends Model
{
    protected $fillable = [
		'tourist_trip_location',
		'car_name',
		'lower_price_range',
		'upper_price_range',
	];


}
