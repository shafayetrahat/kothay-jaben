<?php

namespace App;

class Cart {
	public $items = null;
	public $totalQuantity = 0;
	public $totalPrice = 0;

	public static function restoreCart($oldCart)
	{
		$cart = new static();

		if ($oldCart) {
			$cart->items = $oldCart->items;
			$cart->totalQuantity= $oldCart->totalQuantity;
			$cart->totalPrice = $oldCart->totalPrice;
		}

		return $cart;
	}
	/*public function __construct($oldCart)
	{
		if ($oldCart) {
			$this->items = $oldCart->items;
			$this->totalQuantity = $oldCart->totalQuantity;
			$this->totalPrice = $oldCart->totalPrice;
		}
	}*/

	public function add($item, $id)
	{
		$storedItem = ['qty' => 0, 'price' => $item->product_price, 'item' => $item];

		if ($this->items) {
			if (array_key_exists($id, $this->items)) {
				$storedItem = $this->items[$id];
			}
		}
		$storedItem['qty']++;
		$storedItem['product_price'] = $item->product_price * $storedItem['qty'];
		$this->items[$id] = $storedItem;
		$this->totalQuantity++;
		$this->totalPrice += $item->product_price;
	}

	public function reduceByOne($id)
	{
		$this->items[$id]['qty']--;
		$this->items[$id]['product_price'] -= $this->items[$id]['item']['product_price'];
		$this->totalQuantity--;
		$this->totalPrice -= $this->items[$id]['item']['product_price'];

		if ($this->items[$id]['qty'] <= 0) {
			unset($this->items[$id]);
		}
	}

	public function increaseByOnce($id)
	{
		$this->items[$id]['qty']++;
		$this->items[$id]['product_price'] += $this->items[$id]['item']['product_price'];
		$this->totalQuantity++;
		$this->totalPrice += $this->items[$id]['item']['product_price'];
	}

	public function removeItem($id)
	{
		$this->totalQuantity -= $this->items[$id]['qty'];
		$this->totalPrice -= $this->items[$id]['product_price'];

		unset($this->items[$id]);
	}
}