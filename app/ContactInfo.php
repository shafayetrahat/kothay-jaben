<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactInfo extends Model
{
    protected $fillable = [
    	'site_logo',
	    'phone_number',
	    'address',
	    'email'
    ];
}
