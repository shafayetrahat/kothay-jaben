<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DropOffLocation extends Model
{
    protected $fillable = [
    	'drop_off_location'
	];
}
