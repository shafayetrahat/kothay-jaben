<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    protected $fillable = [
        'hotel_name',
	    'hotel_location',
	    'hotel_image',
	    'hotel_details'
    ];

    public function hotelRooms()
    {
    	return $this->hasMany('App\HotelRoom');
    }
}
