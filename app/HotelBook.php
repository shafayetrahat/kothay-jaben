<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelBook extends Model
{
    protected $fillable = [
	    'room_id',
		'confirmed',
	    'hotel_name',
	    'room_title',
		'room_rent',
		'check_in',
		'check_out',
		'adults',
		'children'
    ];

    public function user()
	{
		return $this->belongsTo('App\User');
	}
}
