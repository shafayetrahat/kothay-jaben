<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelPackage extends Model
{
    protected $fillable = [
        'package_name',
        'previous_rent',
        'current_rent',
        'hotel_name',
        'hotel_location',
        'package_image',
        'package_details'
    ];

    public function hotelPackagePhotos()
    {
        return $this->hasMany('App\HotelPackagePhoto');
    }
}
