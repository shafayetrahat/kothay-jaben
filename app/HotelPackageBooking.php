<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelPackageBooking extends Model
{
    protected $fillable = [
        'hotel_package_id',
		'confirmed',
        'hotel_name',
        'hotel_rent',
        'check_in',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
