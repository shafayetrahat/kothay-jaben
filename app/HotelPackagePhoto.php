<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelPackagePhoto extends Model
{
    protected $fillable = [
        'photos'
    ];

    public function hotelPackage()
    {
        return $this->belongsTo('App\HotelPackage');
    }

    public function delete()
    {
        \File::delete([
            $this->photos
        ]);

        parent::delete();
    }
}
