<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelRoom extends Model
{
	protected $fillable = [
		'room_title',
		'room_rent',
		'room_capacity',
		'room_image',
		'room_details'
	];

	public function hotel()
	{
		return $this->belongsTo('App\Hotel');
	}

	public function roomPhotos()
	{
		return $this->hasMany('App\HotelRoomPhoto');
	}
}
