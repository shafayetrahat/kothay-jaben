<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelRoomPhoto extends Model
{
	protected $fillable = [
		'photos'
	];

    public function hotelRoom()
    {
    	return $this->belongsTo('App\HotelRoom');
    }

	public function delete()
	{
		\File::delete([
			$this->photos
		]);

		parent::delete();
	}
}
