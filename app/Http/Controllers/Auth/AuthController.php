<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;



class AuthController extends Controller
{
    // Some methods which were generated with the app

    /**
     **_ Redirect the user to the OAuth Provider.
     * _**
     **_ @return Response
    _**/
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

      public function handleProviderCallback($provider)
      {
      $user = Socialite::driver($provider)->user();

      $authUser = $this->findOrCreateUser($user, $provider);
      Auth::login($authUser, true);
      return redirect('/admin_home');
      }

      /**
     * _ If a user has registered before using social auth, return the user
     * _ else, create a new user object.
     * _ @param  $user Socialite user object
     * _ @param $provider Social auth provider
     * _ @return  User
     */
    public function findOrCreateUser($user, $provider)
    {
        $authUser = User::where('provider_id', $user->id)->first();
        if ($authUser) {
            return $authUser;
        }
        return User::create([
            'name' => $user->name,
            'email' => $user->email,
            'provider' => $provider,
            'provider_id' => $user->id
        ]);
    }
}