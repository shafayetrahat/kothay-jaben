<?php

namespace App\Http\Controllers\BackEndController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BlogPost;
use Image;
use Storage;
use Session;

class BlogPostsController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$posts = BlogPost::latest()->get();

		return view('back-end.blog.components-blog-posts', compact('posts'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('back-end.blog.add-new-post');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'title' => 'required',
			'author' => 'required',
			'featured_image' => 'required|mimes:jpg,jpeg,png,bmp',
			'blog_post' => 'required'
		]);

		$blogPost = new BlogPost;

		$blogPost->title = $request->title;
		$blogPost->author = $request->author;
		$blogPost->blog_post = $request->blog_post;

		if ($request->hasfile('featured_image')) {
			$image = $request->file('featured_image');
			$filename = time() . '.' . $image->getClientOriginalExtension();
			$location = public_path('photos/blog/' . $filename);
			Image::make($image)->resize(754, 418)->save($location);

			$blogPost->featured_image = $filename;
		}

		$blogPost->save();

		Session::flash('success', 'Your post has successfully been created!');

		return redirect()->route('blog.show', $blogPost->id);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$blogPost = BlogPost::find($id);

		return view('back-end.blog.show', compact('blogPost'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$blogPost = BlogPost::find($id);

		return view('back-end.blog.edit', compact('blogPost'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$blogPost = BlogPost::find($id);

		$this->validate($request, [
			'title' => 'required',
			'author' => 'required',
			'blog_post' => 'required'
		]);

		$blogPost->title = $request->title;
		$blogPost->author = $request->author;
		$blogPost->blog_post = $request->blog_post;

		if ($request->hasfile('featured_image')) {
			// Add photo if updated
			$image = $request->file('featured_image');
			$filename = time() . '.' . $image->getClientOriginalExtension();
			$location = public_path('photos/blog/' . $filename);
			Image::make($image)->resize(754, 418)->save($location);

			$oldFileName = $blogPost->featured_image;

			// Update the database
			$blogPost->featured_image = $filename;

			// Delete old image
			Storage::delete($oldFileName);
		}
		$blogPost->save();

		Session::flash('success', 'Your post has successfully been edited!');

		return redirect()->route('blog.show', $blogPost->id);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$blogPost = BlogPost::find($id);
		Storage::delete($blogPost->featured_image);

		$blogPost->delete();

		Session::flash('success', 'Your post has successfully been deleted!');

		return redirect()->route('blog.index');
	}
}

