<?php

namespace App\Http\Controllers\BackEndController\CarController;

use Session;
use App\TypeBodyContractBooking;
use App\Http\Controllers\Controller;

class BodyContractCarBookingController extends Controller
{
	public function index()
	{
		$bookedCars = TypeBodyContractBooking::latest()
			->paginate(10);
		return view('back-end.cars.body-contract.booked-car.index', compact('bookedCars'));
	}

	public function show(TypeBodyContractBooking $book)
	{
		return view('back-end.cars.body-contract.booked-car.show', compact('book'));
	}

	public function confirmedBooking($id)
	{
		$confirmedBook = TypeBodyContractBooking::find($id);

		$confirmedBook->confirmed = 1;

		$confirmedBook->save();

		return back();
	}

	public function unconfirmedBooking($id)
	{
		$confirmedBook = TypeBodyContractBooking::find($id);

		$confirmedBook->confirmed = 0;

		$confirmedBook->save();

		return back();
	}

	public function destroy($id)
	{
		$bookedCar = TypeBodyContractBooking::find($id);

		$bookedCar->delete();

		Session::flash('success', 'Booked car entry was successfully deleted!');

		return back();
	}
}
