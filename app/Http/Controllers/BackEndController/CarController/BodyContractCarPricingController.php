<?php

namespace App\Http\Controllers\BackEndController\CarController;

use Session;
use App\BodyContractPricing;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BodyContractCarPricingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bodyCP = BodyContractPricing::latest()->get();

        return view('back-end.cars.body-contract.car-pricing.index', compact('bodyCP'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$this->validate($request, [
			'body_contract_location'		=> 'required',
			'car_name'					=> 'required',
			'rental_price'				=> 'required',
		]);

		$bodyCP = new BodyContractPricing();

		$bodyCP->body_contract_location		= $request->body_contract_location;
		$bodyCP->car_name					= $request->car_name;
		$bodyCP->rental_price				= $request->rental_price;

		$bodyCP->save();

		Session::flash('success', 'You have successfully created body contract!');

		return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bodyCP = BodyContractPricing::find($id);

        return view('back-end.cars.body-contract.car-pricing.edit', compact('bodyCP'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$bodyCP = BodyContractPricing::find($id);

		$this->validate($request, [
			'body_contract_location'	=> 'required',
			'car_name'					=> 'required',
			'rental_price'				=> 'required',
		]);


		$bodyCP->body_contract_location		= $request->body_contract_location;
		$bodyCP->car_name					= $request->car_name;
		$bodyCP->rental_price				= $request->rental_price;

		$bodyCP->save();

		Session::flash('success', 'You have successfully edited body contract!');

		return redirect('admin/car/body-contract');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bodyCP = BodyContractPricing::find($id);

        $bodyCP->delete();

		Session::flash('success', 'You have successfully deleted body contract!');

		return back();
    }
}
