<?php

namespace App\Http\Controllers\BackEndController\CarController;

use Session;
use App\BodyContract;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BodyContractsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$bodyContract = BodyContract::latest()
										->get();

        return view('back-end.cars.body-contract.index', compact('bodyContract'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        	'body_contract_location'	=> 'required',
		]);

        $bodyContract = new BodyContract();

        $bodyContract->body_contract_location = $request->body_contract_location;

        $bodyContract->save();

        Session::flash('success', 'You have successfully saved body contract location!');

        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bodyContract = BodyContract::find($id);

        return view('back-end.cars.body-contract.edit', compact('bodyContract'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$bodyContract = BodyContract::find($id);

		$this->validate($request, [
			'body_contract_location'	=> 'required',
		]);


		$bodyContract->body_contract_location = $request->body_contract_location;

		$bodyContract->save();

		Session::flash('success', 'You have successfully edited body contract location!');

		return redirect('admin/car/body-contract-location');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bodyContract = BodyContract::find($id);

        $bodyContract->delete();

        Session::flash('success', 'You have successfully deleted your body contract location!');

        return back();
    }
}
