<?php

namespace App\Http\Controllers\BackEndController\CarController;

use Image;
use Session;
use Storage;
use App\Car;
use App\CarBook;
use App\CarPhoto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CarsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$cars = Car::latest()->get();


        return view('back-end.cars.index', compact('cars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back-end.cars.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
	        'car_name'          => 'required',
	        'car_feature_image' => 'required|mimes:jpg,jpeg,png,bmp,svg',
	        'car_details'       => 'required',
        ]);

        $car = new Car;

        $car->car_name      = $request->car_name;
        $car->car_details   = $request->car_details;

	    if ($request->hasfile('car_feature_image')) {
		    $image = $request->file('car_feature_image');
		    $filename = time() . '.' . $image->getClientOriginalExtension();
		    $location = public_path('photos/cars/' . $filename);
		    Image::make($image)->resize(754, 418)->save($location);

		    $car->car_feature_image = $filename;
	    }

        $car->save();

	    Session::flash('success', 'You have successfully created car!');

        return redirect('admin/cars/' . $car->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $car = Car::find($id);

        return view('back-end.cars.show', compact('car'));
    }

	public function addPhoto(Request $request, $id)
	{
		$this->validate($request, [
			'photo' => 'required|mimes:jpg,jpeg,png,bmp,svg'
		]);

		$file = $request->file('photo');

		$name = time() . $file->getClientOriginalName();

		$file->move('photos/cars', $name);

		$car = Car::find($id);

		$car->photos()->create(['photos' => "photos/cars/{$name}"]);

		return back();
	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $car = Car::find($id);

        return view('back-end.cars.edit', compact('car'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $car = Car::find($id);

	    $this->validate($request, [
		    'car_name' => 'required',
		    'car_details' => 'required'
	    ]);

	    $car->car_name = $request->car_name;
	    $car->car_details = $request->car_details;

	    if ($request->hasfile('car_feature_image')) {
		    // Add photo if updated
		    $image = $request->file('car_feature_image');
		    $filename = time() . '.' . $image->getClientOriginalExtension();
		    $location = public_path('photos/cars/' . $filename);
		    Image::make($image)->resize(754, 418)->save($location);

		    $oldFileName = $car->car_feature_image;

		    // Update the database
		    $car->car_feature_image = $filename;

		    // Delete old image
		    unlink(public_path('photos/cars/' . $oldFileName));
	    }
	    $car->save();

		Session::flash('success', 'You have successfully edited car!');

		return redirect()->route('cars.show', $car->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$car = Car::find($id);

	    $photos = $car->photos();

	    foreach ($car->photos as $photo) {
		    unlink(public_path($photo->photos));
	    }

	    $car->photos()->delete();

	    $car->delete();

		Session::flash('success', 'You have successfully deleted car!');

	    return back();
    }

    public function deletePhoto($id) {
	    $photo = CarPhoto::find($id);

	    $photo->delete();

	    return back();
    }
}
