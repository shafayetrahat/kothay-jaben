<?php

namespace App\Http\Controllers\BackEndController\CarController;

use Session;
use App\TypeHourlyBooking;
use App\Http\Controllers\Controller;

class HourlyTripCarBookingController extends Controller
{
	public function index()
	{
		$bookedCars = TypeHourlyBooking::latest()
			->paginate(10);
		return view('back-end.cars.hourly-trip-booking.index', compact('bookedCars'));
	}

	public function show(TypeHourlyBooking $book)
	{
		return view('back-end.cars.hourly-trip-booking.show', compact('book'));
	}

	public function confirmedBooking($id)
	{
		$confirmedBook = TypeHourlyBooking::find($id);

		$confirmedBook->confirmed = 1;

		$confirmedBook->save();

		return back();
	}

	public function unconfirmedBooking($id)
	{
		$confirmedBook = TypeHourlyBooking::find($id);

		$confirmedBook->confirmed = 0;

		$confirmedBook->save();

		return back();
	}

	public function destroy($id)
	{
		$bookedCar = TypeHourlyBooking::find($id);

		$bookedCar->delete();

		Session::flash('success', 'Booked car entry was successfully deleted!');

		return back();
	}
}
