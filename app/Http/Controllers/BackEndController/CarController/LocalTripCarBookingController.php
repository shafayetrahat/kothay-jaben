<?php

namespace App\Http\Controllers\BackEndController\CarController;

use Session;
use App\TypeLocalBooking;
use App\Http\Controllers\Controller;

class LocalTripCarBookingController extends Controller
{
	public function index()
	{
		$bookedCars = TypeLocalBooking::latest()
			->paginate(10);
		return view('back-end.cars.local-trip-booking.index', compact('bookedCars'));
	}

	public function show(TypeLocalBooking $book)
	{
		return view('back-end.cars.local-trip-booking.show', compact('book'));
	}

	public function confirmedBooking($id)
	{
		$confirmedBook = TypeLocalBooking::find($id);

		$confirmedBook->confirmed = 1;

		$confirmedBook->save();

		return back();
	}

	public function unconfirmedBooking($id)
	{
		$confirmedBook = TypeLocalBooking::find($id);

		$confirmedBook->confirmed = 0;

		$confirmedBook->save();

		return back();
	}

	public function destroy($id)
	{
		$bookedCar = TypeLocalBooking::find($id);

		$bookedCar->delete();

		Session::flash('success', 'Booked car entry was successfully deleted!');

		return back();
	}
}
