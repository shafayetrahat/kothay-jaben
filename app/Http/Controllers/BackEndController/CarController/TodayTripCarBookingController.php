<?php

namespace App\Http\Controllers\BackEndController\CarController;

use Session;
use App\TypeTodayBooking;
use App\Http\Controllers\Controller;

class TodayTripCarBookingController extends Controller
{
	public function index()
	{
		$bookedCars = TypeTodayBooking::latest()
			->paginate(10);
		return view('back-end.cars.today-trip-booking.index', compact('bookedCars'));
	}

	public function show(TypeTodayBooking $book)
	{
		return view('back-end.cars.today-trip-booking.show', compact('book'));
	}

	public function confirmedBooking($id)
	{
		$confirmedBook = TypeTodayBooking::find($id);

		$confirmedBook->confirmed = 1;

		$confirmedBook->save();

		return back();
	}

	public function unconfirmedBooking($id)
	{
		$confirmedBook = TypeTodayBooking::find($id);

		$confirmedBook->confirmed = 0;

		$confirmedBook->save();

		return back();
	}

	public function destroy($id)
	{
		$bookedCar = TypeTodayBooking::find($id);

		$bookedCar->delete();

		Session::flash('success', 'Booked car entry was successfully deleted!');

		return back();
	}
}
