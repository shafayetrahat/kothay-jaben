<?php

namespace App\Http\Controllers\BackEndController\CarController;

use Session;
use App\TypeTouristBooking;
use App\Http\Controllers\Controller;

class TouristCarBookingController extends Controller
{
    public function index()
    {
    	$bookedCars = TypeTouristBooking::latest()
		                        ->paginate(10);
	    return view('back-end.cars.tourist-trip-location.booked-car.index', compact('bookedCars'));
    }

    public function show(TypeTouristBooking $book)
    {
		return view('back-end.cars.tourist-trip-location.booked-car.show', compact('book'));
    }

	public function confirmedBooking($id)
	{
		$confirmedBook = TypeTouristBooking::find($id);

		$confirmedBook->confirmed = 1;

		$confirmedBook->save();

		return back();
	}

	public function unconfirmedBooking($id)
	{
		$confirmedBook = TypeTouristBooking::find($id);

		$confirmedBook->confirmed = 0;

		$confirmedBook->save();

		return back();
	}

    public function destroy($id)
    {
    	$bookedCar = TypeTouristBooking::find($id);

    	$bookedCar->delete();

    	Session::flash('success', 'Booked car entry was successfully deleted!');

    	return back();
    }
}
