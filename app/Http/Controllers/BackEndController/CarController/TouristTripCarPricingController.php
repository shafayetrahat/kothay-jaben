<?php

namespace App\Http\Controllers\BackEndController\CarController;

use Session;
use App\CarPricing;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TouristTripCarPricingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$carPricings = CarPricing::latest()->get();

        return view('back-end.cars.tourist-trip-location.tourist-trip-car-pricing.index', compact('carPricings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'tourist_trip_location'		=> 'required',
			'car_name'					=> 'required',
			'lower_price_range'			=> 'required',
			'upper_price_range'			=> 'required',
		]);

        $carPricing = new CarPricing();

		$carPricing->tourist_trip_location		= $request->tourist_trip_location;
		$carPricing->car_name					= $request->car_name;
		$carPricing->lower_price_range			= $request->lower_price_range;
		$carPricing->upper_price_range			= $request->upper_price_range;

		$carPricing->save();

		Session::flash('success', 'You have successfully created tourist trip!');

		return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $carPricing = CarPricing::find($id);

		return view('back-end.cars.tourist-trip-location.tourist-trip-car-pricing.edit', compact('carPricing'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$carPricing = CarPricing::find($id);

		$this->validate($request, [
			'tourist_trip_location'		=> 'required',
			'car_name'					=> 'required',
			'lower_price_range'			=> 'required',
			'upper_price_range'			=> 'required',
		]);


		$carPricing->tourist_trip_location		= $request->tourist_trip_location;
		$carPricing->car_name					= $request->car_name;
		$carPricing->lower_price_range			= $request->lower_price_range;
		$carPricing->upper_price_range			= $request->upper_price_range;

		$carPricing->save();

		Session::flash('success', 'You have successfully edited tourist trip!');

		return redirect('admin/car/tourist-trip');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $carPricing = CarPricing::find($id);

        $carPricing->delete();

		Session::flash('success', 'You have successfully deleted tourist trip!');

		return back();
    }
}
