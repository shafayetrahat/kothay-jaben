<?php

namespace App\Http\Controllers\BackEndController\CarController;

use Session;
use App\TouristTrip;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TouristTripController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$touristTrips = TouristTrip::latest()->get();

		return view('back-end.cars.tourist-trip-location.index', compact('touristTrips'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'tourist_trip_location'  => 'required',
		]);

		$touristTrip = new TouristTrip();

		$touristTrip->tourist_trip_location = $request->tourist_trip_location;

		$touristTrip->save();

		Session::flash('success', 'You have successfully saved tourist trip location!');

		return back();
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$touristTrip = TouristTrip::find($id);

		return view('back-end.cars.tourist-trip-location.edit', compact('touristTrip'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$touristTrip = TouristTrip::find($id);

		$this->validate($request, [
			'tourist_trip_location'  => 'required',
		]);


		$touristTrip->tourist_trip_location = $request->tourist_trip_location;

		$touristTrip->save();

		Session::flash('success', 'You have successfully edited tourist trip location!');

		return redirect('admin/car/tourist-trip-location');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$touristTrip = TouristTrip::find($id);

		$touristTrip->delete();

		Session::flash('success', 'You have successfully deleted tourist trip location!');

		return back();
	}
}
