<?php

namespace App\Http\Controllers\BackEndController\CarController;

use Session;
use App\TypeWeddingBooking;
use App\Http\Controllers\Controller;

class WeddingCarBookingController extends Controller
{
	public function index()
	{
		$bookedCars = TypeWeddingBooking::latest()
			->paginate(10);
		return view('back-end.cars.wedding-booking.index', compact('bookedCars'));
	}

	public function show(TypeWeddingBooking $book)
	{
		return view('back-end.cars.wedding-booking.show', compact('book'));
	}

	public function confirmedBooking($id)
	{
		$confirmedBook = TypeWeddingBooking::find($id);

		$confirmedBook->confirmed = 1;

		$confirmedBook->save();

		return back();
	}

	public function unconfirmedBooking($id)
	{
		$confirmedBook = TypeWeddingBooking::find($id);

		$confirmedBook->confirmed = 0;

		$confirmedBook->save();

		return back();
	}

	public function destroy($id)
	{
		$bookedCar = TypeWeddingBooking::find($id);

		$bookedCar->delete();

		Session::flash('success', 'Booked car entry was successfully deleted!');

		return back();
	}
}
