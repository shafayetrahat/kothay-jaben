<?php

namespace App\Http\Controllers\BackEndController\DashboardController;

use App\Car;
use App\User;
use App\Hotel;
use App\Product;
use App\BlogPost;
use App\CarPackage;
use App\TourPackage;
use App\HotelPackage;
use App\HotelPackageBooking;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashBoardController extends Controller
{
    public function index()
    {
        $users          = User::count();
        $blogPosts      = BlogPost::count();
        $hotels         = Hotel::count();
        $hotelPackages  = HotelPackage::count();
        $tourPackages   = TourPackage::count();
        $carPackages    = CarPackage::count();
        $products       = Product::count();
        $cars			= Car::count();

        return view('back-end.dashboard.index',
            compact('users', 'blogPosts', 'hotels', 'hotelPackages', 'tourPackages', 'carPackages', 'products', 'cars'));
    }

    public function getBookingRequest(Request $request)
    {
        if ($request->ajax()) {
            $hotelBooking = HotelPackageBooking::all();

            return $hotelBooking;
        }
    }
}
