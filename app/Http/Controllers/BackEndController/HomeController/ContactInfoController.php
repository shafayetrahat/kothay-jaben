<?php

namespace App\Http\Controllers\BackEndController\HomeController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;
use Storage;
use App\ContactInfo;

class ContactInfoController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function create()
	{
		$contactInfos = ContactInfo::all();

		return view('back-end.home.contact-info', compact('contactInfos'));
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    $this->validate($request, [
		    'site_logo'     => 'required|mimes:jpg,jpeg,png,bmp',
		    'phone_number'  => 'required',
		    'address'       => 'required',
		    'email'         => 'required|email'
	    ]);

	    $contactInfo = new ContactInfo;

	    $contactInfo->phone_number  = $request->phone_number;
	    $contactInfo->address       = $request->address;
	    $contactInfo->email         = $request->email;

	    if ($request->hasfile('site_logo')) {
		    $image = $request->file('site_logo');
		    $filename = time() . '.' . $image->getClientOriginalExtension();
		    $location = public_path('photos/logo/' . $filename);
		    Image::make($image)->save($location);

		    $contactInfo->site_logo = $filename;
	    }

	    $contactInfo->save();

	    return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
	    $contactInfo = ContactInfo::find($id);

	    return view('back-end.home.edit-contact', compact('contactInfo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $contactInfo = ContactInfo::find($id);

	    $this->validate($request, [
		    'site_logo' => 'mimes:jpg,jpeg,png,bmp',
		    'phone_number' => 'required',
		    'address' => 'required',
		    'email' => 'required|email'
	    ]);

	    $contactInfo->phone_number = $request->phone_number;
	    $contactInfo->address = $request->address;
	    $contactInfo->email = $request->email;

	    if ($request->hasfile('site_logo')) {
		    // Add photo if updated
		    $image = $request->file('site_logo');
		    $filename = time() . '.' . $image->getClientOriginalExtension();
		    $location = public_path('photos/logo/' . $filename);
		    Image::make($image)->save($location);

		    $oldFileName = $contactInfo->site_logo;

		    // Update the database
		    $contactInfo->site_logo = $filename;

		    // Delete old image
		    unlink(public_path('photos/logo/' . $oldFileName));
	    }

	    $contactInfo->save();

	    return redirect('/admin/home/contact');
    }
}
