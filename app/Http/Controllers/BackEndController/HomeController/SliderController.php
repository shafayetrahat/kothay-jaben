<?php

namespace App\Http\Controllers\BackEndController\HomeController;

use Image;
use Session;
use Storage;
use App\SliderPhoto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SliderController extends Controller
{
    public function index()
    {
        $sliderPhotos = SliderPhoto::latest()->get();

        return view('back-end.home.index', compact('sliderPhotos'));
    }

    public function create()
    {
        return view('back-end.home.create');
    }

    public function addSlider(Request $request)
    {
    	$this->validate($request, [
            'slider_text'       => 'required',
            'package_link'      => 'required',
            'slider_image'      => 'required|mimes:jpg,jpeg,png,bmp,svg',
        ]);

    	$sliderPhoto = new SliderPhoto();

    	$sliderPhoto->slider_text   = $request->slider_text;
    	$sliderPhoto->package_link  = $request->package_link;

        if ($request->hasfile('slider_image')) {
            $image = $request->file('slider_image');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('photos/slider/' . $filename);
            Image::make($image)->resize(1920, 1038)->save($location);

            $sliderPhoto->slider_image = $filename;
        }

        $sliderPhoto->save();

        Session::flash('success', 'New Slider has successfully been created!');

        return redirect('admin/all-sliders/');
    }

    public function edit($id)
    {
        $sliderPhoto = SliderPhoto::find($id);

        return view('back-end.home.edit-slider', compact('sliderPhoto'));
    }

    public function update(Request $request, $id)
    {
        $sliderPhoto = SliderPhoto::find($id);

        $this->validate($request, [
            'slider_text'       => 'required',
            'package_link'      => 'required',
        ]);


        $sliderPhoto->slider_text   = $request->slider_text;
        $sliderPhoto->package_link  = $request->package_link;

        if ($request->hasfile('slider_image')) {
            // Add photo if updated
            $image = $request->file('slider_image');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('photos/slider/' . $filename);
            Image::make($image)->save($location);

            $oldFileName = $sliderPhoto->slider_image;

            // Update the database
            $sliderPhoto->slider_image = $filename;

            // Delete old image
            unlink(public_path('photos/slider/' . $oldFileName));
        }

        $sliderPhoto->save();

        Session::flash('success', 'Slider has successfully been updated!');

        return redirect('admin/all-sliders/');
    }

    public function destroy($id)
    {
    	$sliderPhoto = SliderPhoto::find($id);

        unlink(public_path('photos/slider/' . $sliderPhoto->slider_image));

        $sliderPhoto->delete();

        Session::flash('success', 'Your slider has successfully been deleted!');

    	return back();
    }
}
