<?php

namespace App\Http\Controllers\BackEndController\HotelController;

use Session;
use App\HotelBook;
use App\Http\Controllers\Controller;

class HotelBookingController extends Controller
{
    public function index()
    {
    	$bookedHotel = HotelBook::latest()
		                        ->paginate(10);

    	return view('back-end.booked-hotel.index', compact('bookedHotel'));
    }

    public function show(HotelBook $book)
    {
    	return view('back-end.booked-hotel.show', compact('book'));
    }

	public function confirmedBooking($id)
	{
		$confirmedBook = HotelBook::find($id);

		$confirmedBook->confirmed = 1;

		$confirmedBook->save();

		return back();
	}

	public function unconfirmedBooking($id)
	{
		$confirmedBook = HotelBook::find($id);

		$confirmedBook->confirmed = 0;

		$confirmedBook->save();

		return back();
	}

    public function destroy($id)
    {
    	$bookedHotel = HotelBook::find($id);

    	$bookedHotel->delete();

	    Session::flash('success', 'Booked hotel entry was successfully deleted!');

	    return back();
    }
}
