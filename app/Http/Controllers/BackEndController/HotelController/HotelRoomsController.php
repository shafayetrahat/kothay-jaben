<?php

namespace App\Http\Controllers\BackEndController\HotelController;

use App\HotelRoomPhoto;
use Image;
use Storage;
use App\Hotel;
use App\HotelRoom;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class HotelRoomsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Hotel $hotel)
    {
	    return view('back-end.hotel-rooms.create', compact('hotel'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
	    $this->validate($request, [
	    	'room_title'    => 'required',
		    'room_rent'     => 'required',
		    'room_capacity' => 'required',
		    'room_image'    => 'required|mimes:jpg,jpeg,png,bmp,svg',
		    'room_details'  => 'required'
	    ]);
	    $hotel = Hotel::find($id);

	    $hotelRoom = new HotelRoom;

	    $hotelRoom->room_title      = $request->room_title;
	    $hotelRoom->room_rent       = $request->room_rent;
	    $hotelRoom->room_capacity   = $request->room_capacity;
	    $hotelRoom->room_details    = $request->room_details;

	    if ($request->hasfile('room_image')) {
		    $image = $request->file('room_image');
		    $filename = time() . '.' . $image->getClientOriginalExtension();
		    $location = public_path('photos/hotel-rooms/' . $filename);
		    Image::make($image)->resize(754, 418)->save($location);

		    $hotelRoom->room_image = $filename;
	    }

	    $hotelRoom->hotel()->associate($hotel);

	    $hotelRoom->save();

	    Session::flash('success', 'Room details has successfully been created!');

	    return redirect('/admin/hotels/'. $hotelRoom->id .'/hotel-rooms');
    }

	/**
	 * Add multiple to each hotel room
	 */

	public function addPhoto(Request $request, $id)
	{
		$this->validate($request, [
			'photo' => 'required|mimes:jpg,jpeg,png,bmp,svg'
		]);

		$file = $request->file('photo');

		$name = time() . $file->getClientOriginalName();

		$hotelRoom = HotelRoom::find($id);

		$hotelRoom->roomPhotos()->create(['photos' => "photos/hotel-rooms/{$name}"]);

		$file->move('photos/hotel-rooms', $name);

		return back();
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(HotelRoom $hotelRoom)
    {
        return view('back-end.hotel-rooms.show', compact('hotelRoom'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(HotelRoom $hotelRoom)
    {
        return view('back-end.hotel-rooms.edit', compact('hotelRoom'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $hotelRoom = HotelRoom::find($id);

	    //$hotel = Hotel::find($id);

	    $this->validate($request, [
		    'room_title'    => 'required',
		    'room_rent'     => 'required',
		    'room_capacity' => 'required',
		    'room_image'    => 'mimes:jpg,jpeg,png,bmp,svg',
		    'room_details'  => 'required'
	    ]);


	    $hotelRoom->room_title      = $request->room_title;
	    $hotelRoom->room_rent       = $request->room_rent;
	    $hotelRoom->room_capacity   = $request->room_capacity;
	    $hotelRoom->room_details    = $request->room_details;

	    if ($request->hasfile('room_image')) {
		    // Add photo if updated
		    $image = $request->file('room_image');
		    $filename = time() . '.' . $image->getClientOriginalExtension();
		    $location = public_path('photos/hotel-rooms/' . $filename);
		    Image::make($image)->resize(754, 418)->save($location);

		    $oldFileName = $hotelRoom->room_image;

		    // Update the database
		    $hotelRoom->room_image = $filename;

		    // Delete old image
		    unlink(public_path('photos/hotel-rooms/' . $oldFileName));
	    }

	    //$hotelRoom->hotel()->associate($hotel);

	    $hotelRoom->save();

	    Session::flash('success', 'Room details has successfully been updated!');

	    return redirect('/admin/hotels/'. $hotelRoom->id .'/hotel-rooms');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hotelRoom = HotelRoom::find($id);

        foreach ($hotelRoom->roomPhotos as $photo) {
	        unlink(public_path($photo->photos));
        }

	    unlink(public_path('photos/hotel-rooms/' . $hotelRoom->room_image));

        $hotelRoom->delete();

	    Session::flash('success', 'Room has successfully been deleted!');

        return back();
    }

    public function deletePhoto($id)
    {
    	$roomPhoto = HotelRoomPhoto::find($id);

    	$roomPhoto->delete();

    	return back();
    }
}
