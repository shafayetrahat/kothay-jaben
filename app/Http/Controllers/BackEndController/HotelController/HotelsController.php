<?php

namespace App\Http\Controllers\BackEndController\HotelController;

use Image;
use Session;
use Storage;
use App\Hotel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HotelsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$hotels = Hotel::latest()
						->paginate(20);

		return view('back-end.hotels.index', compact('hotels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    return view('back-end.hotels.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'hotel_name'        => 'required',
	        'hotel_location'    => 'required',
	        'hotel_image'       => 'required|mimes:jpg,jpeg,png,bmp,svg',
	        'hotel_details'     => 'required'
        ]);

        $hotel = new Hotel;

	    $hotel->hotel_name      = $request->hotel_name;
	    $hotel->hotel_location  = $request->hotel_location;
	    $hotel->hotel_details   = $request->hotel_details;

	    if ($request->hasfile('hotel_image')) {
		    $image = $request->file('hotel_image');
		    $filename = time() . '.' . $image->getClientOriginalExtension();
		    $location = public_path('photos/hotels/' . $filename);
		    Image::make($image)->resize(754, 418)->save($location);

		    $hotel->hotel_image = $filename;
	    }

	    $hotel->save();

	    Session::flash('success', 'New Hotel has successfully been created!');

	    return redirect()->route('hotels.show', $hotel->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Hotel $hotel)
    {
        return view('back-end.hotels.show', compact('hotel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Hotel $hotel)
    {
        return view('back-end.hotels.edit', compact('hotel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $hotel = Hotel::find($id);

	    $this->validate($request, [
		    'hotel_name'        => 'required',
		    'hotel_location'    => 'required',
		    'hotel_image'       => 'mimes:jpg,jpeg,png,bmp,svg',
		    'hotel_details'     => 'required'
	    ]);


	    $hotel->hotel_name      = $request->hotel_name;
	    $hotel->hotel_location  = $request->hotel_location;
	    $hotel->hotel_details   = $request->hotel_details;

	    if ($request->hasfile('hotel_image')) {
		    // Add photo if updated
		    $image = $request->file('hotel_image');
		    $filename = time() . '.' . $image->getClientOriginalExtension();
		    $location = public_path('photos/hotels/' . $filename);
		    Image::make($image)->resize(754, 418)->save($location);

		    $oldFileName = $hotel->hotel_image;

		    // Update the database
		    $hotel->hotel_image = $filename;

		    // Delete old image
		    unlink(public_path('photos/hotels/' . $oldFileName));
	    }

	    $hotel->save();

	    Session::flash('success', 'Hotel has successfully been updated!');

	    return redirect()->route('hotels.show', $hotel->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	$hotel = Hotel::find($id);

	    foreach ($hotel->hotelRooms as $room) {

		    unlink(public_path('photos/hotel-rooms/' . $room->room_image));
		    foreach ($room->roomPhotos as $photo) {
			    unlink(public_path($photo->photos));
		    }

		    $room->delete();
	    }

	    unlink(public_path('photos/hotels/' . $hotel->hotel_image));

	    $hotel->delete();

	    Session::flash('success', 'Your hotel has successfully been deleted!');

	    return back();
    }
}
