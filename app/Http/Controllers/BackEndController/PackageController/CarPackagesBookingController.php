<?php

namespace App\Http\Controllers\BackEndController\PackageController;

use Session;
use App\CarPackageBooking;
use App\Http\Controllers\Controller;

class CarPackagesBookingController extends Controller
{
    public function index()
    {
        $cpBookings = CarPackageBooking::latest()
            ->paginate(10);

        return view('back-end.packages.car-package.booked-car-package.index', compact('cpBookings'));
    }

    public function show($id)
    {
        $cpBook = CarPackageBooking::find($id);

        return view('back-end.packages.car-package.booked-car-package.show', compact('cpBook'));
    }

	public function confirmedBooking($id)
	{
		$confirmedBook = CarPackageBooking::find($id);

		$confirmedBook->confirmed = 1;

		$confirmedBook->save();

		return back();
	}

	public function unconfirmedBooking($id)
	{
		$confirmedBook = CarPackageBooking::find($id);

		$confirmedBook->confirmed = 0;

		$confirmedBook->save();

		return back();
	}

    public function destroy($id)
    {
        $cpBook = CarPackageBooking::find($id);

        $cpBook->delete();

        Session::flash('success', 'Booked car package entry was successfully deleted!');

        return back();
    }
}
