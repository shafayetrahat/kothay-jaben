<?php

namespace App\Http\Controllers\BackEndController\PackageController;

use Image;
use Session;
use Storage;
use App\CarPackage;
use App\CarPackagePhoto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CarPackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $carPackages = CarPackage::latest()
                                    ->paginate(20);

        return view('back-end.packages.car-package.index', compact('carPackages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back-end.packages.car-package.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'package_name'       => 'required',
            'previous_price'     => 'required',
            'current_price'      => 'required',
            'car_name'           => 'required',
            'package_image'      => 'required|mimes:jpg,jpeg,png,bmp,svg',
            'package_details'    => 'required'
        ]);

        $carPackage = new CarPackage();

        $carPackage->package_name      = $request->package_name;
        $carPackage->previous_price    = $request->previous_price;
        $carPackage->current_price     = $request->current_price;
        $carPackage->car_name          = $request->car_name;
        $carPackage->package_details   = $request->package_details;

        if ($request->hasfile('package_image')) {
            $image = $request->file('package_image');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('photos/packages/' . $filename);
            Image::make($image)->resize(754, 418)->save($location);

            $carPackage->package_image = $filename;
        }

        $carPackage->save();

        Session::flash('success', 'Your car package has successfully been created!');

        return redirect('/admin/packages-car/' . $carPackage->id);
    }

    public function addPhoto(Request $request, $id)
    {
        $this->validate($request, [
            'photo' => 'required|mimes:jpg,jpeg,png,bmp,svg'
        ]);

        $file = $request->file('photo');

        $name = time() . $file->getClientOriginalName();

        $carPackage = CarPackage::find($id);

        $carPackage->carPhotos()->create(['photos' => "photos/cars/{$name}"]);

        $file->move('photos/cars', $name);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $carPackage = CarPackage::find($id);

        return view('back-end.packages.car-package.show', compact('carPackage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $carPackage = CarPackage::find($id);

        return view('back-end.packages.car-package.edit', compact('carPackage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $carPackage = CarPackage::find($id);

        $this->validate($request, [
            'package_name'       => 'required',
            'previous_price'     => 'required',
            'current_price'      => 'required',
            'car_name'           => 'required',
            'package_details'    => 'required'
        ]);


        $carPackage->package_name      = $request->package_name;
        $carPackage->previous_price    = $request->previous_price;
        $carPackage->current_price     = $request->current_price;
        $carPackage->car_name          = $request->car_name;
        $carPackage->package_details   = $request->package_details;

        if ($request->hasfile('package_image')) {
            // Add photo if updated
            $image = $request->file('package_image');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('photos/packages/' . $filename);
            Image::make($image)->resize(754, 418)->save($location);

            $oldFileName = $carPackage->package_image;

            // Update the database
            $carPackage->package_image = $filename;

            // Delete old image
            unlink(public_path('photos/packages/' . $oldFileName));
        }

        $carPackage->save();

        Session::flash('success', 'Your car package has successfully been updated!');

        return redirect('/admin/packages-car/' . $carPackage->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $carPackage = CarPackage::find($id);

        foreach ($carPackage->carPhotos as $photo) {
            unlink(public_path($photo->photos));
        }

        $carPackage->carPhotos()->delete();

        unlink(public_path('photos/packages/' . $carPackage->package_image));

        $carPackage->delete();

        Session::flash('success', 'Your car package has successfully been deleted!');

        return back();
    }

    public function deletePhoto($id)
    {
        $carPackagePhoto = CarPackagePhoto::find($id);

        $carPackagePhoto->delete();

        return back();
    }
}
