<?php

namespace App\Http\Controllers\BackEndController\PackageController;

use Session;
use App\HotelPackageBooking;
use App\Http\Controllers\Controller;

class HotelPackagesBookingController extends Controller
{
    public function index()
    {
        $hpBookings = HotelPackageBooking::latest()
            ->paginate(10);

        return view('back-end.packages.hotel-package.booked-hotel-package.index', compact('hpBookings'));
    }

    public function show($id)
    {
        $hpBook = HotelPackageBooking::find($id);

        return view('back-end.packages.hotel-package.booked-hotel-package.show', compact('hpBook'));
    }

	public function confirmedBooking($id)
	{
		$confirmedBook = HotelPackageBooking::find($id);

		$confirmedBook->confirmed = 1;

		$confirmedBook->save();

		return back();
	}

	public function unconfirmedBooking($id)
	{
		$confirmedBook = HotelPackageBooking::find($id);

		$confirmedBook->confirmed = 0;

		$confirmedBook->save();

		return back();
	}

    public function destroy($id)
    {
        $hpBook = HotelPackageBooking::find($id);

        $hpBook->delete();

        Session::flash('success', 'Booked hotel package entry was successfully deleted!');

        return back();
    }
}
