<?php

namespace App\Http\Controllers\BackEndController\PackageController;

use Image;
use Session;
use Storage;
use App\HotelPackage;
use App\HotelPackagePhoto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HotelPackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hotelPackages = HotelPackage::latest()
                                        ->paginate(20);

        return view('back-end.packages.hotel-package.index', compact('hotelPackages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back-end.packages.hotel-package.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'package_name'      => 'required',
            'previous_rent'     => 'required',
            'current_rent'      => 'required',
            'hotel_name'        => 'required',
            'hotel_location'    => 'required',
            'package_image'     => 'required|mimes:jpg,jpeg,png,bmp,svg',
            'package_details'   => 'required'
        ]);

        $hotelPackage = new HotelPackage();

        $hotelPackage->package_name      = $request->package_name;
        $hotelPackage->previous_rent     = $request->previous_rent;
        $hotelPackage->current_rent      = $request->current_rent;
        $hotelPackage->hotel_name        = $request->hotel_name;
        $hotelPackage->hotel_location    = $request->hotel_location;
        $hotelPackage->package_details   = $request->package_details;

        if ($request->hasfile('package_image')) {
            $image = $request->file('package_image');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('photos/packages/' . $filename);
            Image::make($image)->resize(754, 418)->save($location);

            $hotelPackage->package_image = $filename;
        }

        $hotelPackage->save();

        Session::flash('success', 'Your hotel package has successfully been created!');

        return redirect('/admin/packages-hotels/' . $hotelPackage->id);
    }

    public function addPhoto(Request $request, $id)
    {
        $this->validate($request, [
            'photo' => 'required|mimes:jpg,jpeg,png,bmp,svg'
        ]);

        $file = $request->file('photo');

        $name = time() . $file->getClientOriginalName();

        $hotelPackage = HotelPackage::find($id);

        $hotelPackage->hotelPackagePhotos()->create(['photos' => "photos/hotel-rooms/{$name}"]);

        $file->move('photos/hotel-rooms', $name);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $hotelPackage = HotelPackage::find($id);

        return view('back-end.packages.hotel-package.show', compact('hotelPackage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $hotelPackage = HotelPackage::find($id);

        return view('back-end.packages.hotel-package.edit', compact('hotelPackage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hotelPackage = HotelPackage::find($id);

        $this->validate($request, [
            'package_name'      => 'required',
            'previous_rent'     => 'required',
            'current_rent'      => 'required',
            'hotel_name'        => 'required',
            'hotel_location'    => 'required',
            'package_details'   => 'required'
        ]);


        $hotelPackage->package_name      = $request->package_name;
        $hotelPackage->previous_rent     = $request->previous_rent;
        $hotelPackage->current_rent      = $request->current_rent;
        $hotelPackage->hotel_name        = $request->hotel_name;
        $hotelPackage->hotel_location    = $request->hotel_location;
        $hotelPackage->package_details   = $request->package_details;

        if ($request->hasfile('package_image')) {
            // Add photo if updated
            $image = $request->file('package_image');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('photos/packages/' . $filename);
            Image::make($image)->resize(754, 418)->save($location);

            $oldFileName = $hotelPackage->package_image;

            // Update the database
            $hotelPackage->package_image = $filename;

            // Delete old image
            unlink(public_path('photos/packages/' . $oldFileName));
        }

        $hotelPackage->save();

        Session::flash('success', 'Your hotel package has successfully been edited!');

        return redirect('/admin/packages-hotels/' . $hotelPackage->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hotelPackage = HotelPackage::find($id);

        foreach ($hotelPackage->hotelPackagePhotos as $photo) {
            unlink(public_path($photo->photos));
        }

        $hotelPackage->hotelPackagePhotos()->delete();

        unlink(public_path('photos/packages/' . $hotelPackage->package_image));

        $hotelPackage->delete();

        Session::flash('success', 'Your hotel package has successfully been deleted!');

        return back();
    }

    public function deletePhoto($id)
    {
        $hotelPackagePhoto = HotelPackagePhoto::find($id);

        $hotelPackagePhoto->delete();

        return back();
    }
}
