<?php

namespace App\Http\Controllers\BackEndController\PackageController;

use Session;
use App\TourPackageBooking;
use App\Http\Controllers\Controller;

class TourPackagesBookingController extends Controller
{
    public function index()
    {
        $tpBookings = TourPackageBooking::latest()
            ->paginate(10);

        return view('back-end.packages.tour-package.booked-tour-package.index', compact('tpBookings'));
    }

    public function show($id)
    {
        $tpBook = TourPackageBooking::find($id);

        return view('back-end.packages.tour-package.booked-tour-package.show', compact('tpBook'));
    }

	public function confirmedBooking($id)
	{
		$confirmedBook = TourPackageBooking::find($id);

		$confirmedBook->confirmed = 1;

		$confirmedBook->save();

		return back();
	}

	public function unconfirmedBooking($id)
	{
		$confirmedBook = TourPackageBooking::find($id);

		$confirmedBook->confirmed = 0;

		$confirmedBook->save();

		return back();
	}

    public function destroy($id)
    {
        $tpBook = TourPackageBooking::find($id);

        $tpBook->delete();

        Session::flash('success', 'Booked tour package entry was successfully deleted!');

        return back();
    }
}
