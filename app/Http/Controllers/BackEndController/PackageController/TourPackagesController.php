<?php

namespace App\Http\Controllers\BackEndController\PackageController;

use Image;
use Storage;
use Session;
use App\TourPackage;
use App\TourPackagePhoto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TourPackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tourPackages = TourPackage::latest()
                                    ->paginate(20);

        return view('back-end.packages.tour-package.index', compact('tourPackages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back-end.packages.tour-package.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'package_name'      => 'required',
            'package_price'     => 'required',
            'hotel_name'        => 'required',
            'hotel_location'    => 'required',
            'package_details'   => 'required',
            'package_image'     => 'required|mimes:jpg,jpeg,png,bmp,svg'
        ]);

        $tourPackage = new TourPackage();

        $tourPackage->package_name      = $request->package_name;
        $tourPackage->package_price     = $request->package_price;
        $tourPackage->hotel_name        = $request->hotel_name;
        $tourPackage->hotel_location    = $request->hotel_location;
        $tourPackage->package_details   = $request->package_details;

        if ($request->hasfile('package_image')) {
            $image = $request->file('package_image');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('photos/packages/' . $filename);
            Image::make($image)->resize(754, 418)->save($location);

            $tourPackage->package_image = $filename;
        }

        $tourPackage->save();

        Session::flash('success', 'Your tour package has successfully been created!');

        return redirect('/admin/packages/' . $tourPackage->id);
    }

    public function addPhoto(Request $request, $id)
    {
        $this->validate($request, [
            'photo' => 'required|mimes:jpg,jpeg,png,bmp,svg'
        ]);

        $file = $request->file('photo');

        $name = time() . $file->getClientOriginalName();

        $tourPackage = TourPackage::find($id);

        $tourPackage->packagePhotos()->create(['photos' => "photos/hotel-rooms/{$name}"]);

        $file->move('photos/hotel-rooms', $name);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tourPackage = TourPackage::find($id);

        return view('back-end.packages.tour-package.show', compact('tourPackage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tourPackage = TourPackage::find($id);

        return view('back-end.packages.tour-package.edit', compact('tourPackage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tourPackage = TourPackage::find($id);

        $this->validate($request, [
            'package_name'      => 'required',
            'package_price'     => 'required',
            'hotel_name'        => 'required',
            'hotel_location'    => 'required',
            'package_details'   => 'required',
        ]);


        $tourPackage->package_name      = $request->package_name;
        $tourPackage->package_price     = $request->package_price;
        $tourPackage->hotel_name        = $request->hotel_name;
        $tourPackage->hotel_location    = $request->hotel_location;
        $tourPackage->package_details   = $request->package_details;

        if ($request->hasfile('package_image')) {
            // Add photo if updated
            $image = $request->file('package_image');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('photos/packages/' . $filename);
            Image::make($image)->resize(754, 418)->save($location);

            $oldFileName = $tourPackage->package_image;

            // Update the database
            $tourPackage->package_image = $filename;

            // Delete old image
            unlink(public_path('photos/packages/' . $oldFileName));
        }

        $tourPackage->save();

        Session::flash('success', 'Your tour package has successfully been edited!');

        return redirect('/admin/packages/' . $tourPackage->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tourPackage = TourPackage::find($id);

        foreach ($tourPackage->packagePhotos as $photo) {
            unlink(public_path($photo->photos));
        }

        $tourPackage->packagePhotos()->delete();

        unlink(public_path('photos/packages/' . $tourPackage->package_image));

        $tourPackage->delete();

        Session::flash('success', 'Your package has successfully been deleted!');

        return back();
    }

    public function deletePhoto($id)
    {
        $packagePhoto = TourPackagePhoto::find($id);

        $packagePhoto->delete();

        return back();
    }
}
