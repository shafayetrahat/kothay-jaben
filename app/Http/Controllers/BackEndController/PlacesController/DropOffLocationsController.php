<?php

namespace App\Http\Controllers\BackEndController\PlacesController;

use Session;
use App\DropOffLocation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DropOffLocationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$dropOffLocation = DropOffLocation::latest()->get();

		return view('back-end.places.drop-off.index', compact('dropOffLocation'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$this->validate($request, [
			'drop_off_location'  => 'required',
		]);

		$dropOffLocation = new DropOffLocation();

		$dropOffLocation->drop_off_location = $request->drop_off_location;

		$dropOffLocation->save();

		Session::flash('success', 'You have successfully saved drop off location!');

		return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dropOffLocation = DropOffLocation::find($id);

		return view('back-end.places.drop-off.edit', compact('dropOffLocation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$dropOffLocation = DropOffLocation::find($id);

		$this->validate($request, [
			'drop_off_location'  => 'required',
		]);


		$dropOffLocation->drop_off_location = $request->drop_off_location;

		$dropOffLocation->save();

		Session::flash('success', 'You have successfully edited drop off location!');

		return redirect('admin/drop-off/location');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$dropOffLocation = DropOffLocation::find($id);

		$dropOffLocation->delete();

		Session::flash('success', 'You have successfully deleted drop off location!');

		return back();
    }
}
