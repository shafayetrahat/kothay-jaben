<?php

namespace App\Http\Controllers\BackEndController\PlacesController;

use Session;
use App\PickUpLocation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PickUpLocationsController extends Controller
{
    public function pickUpLocation()
    {
        $pickUpLocations = PickUpLocation::latest()->get();

        return view('back-end.places.pick-up.index', compact('pickUpLocations'));
    }

    public function storePickUpLocation(Request $request)
    {
        $this->validate($request, [
            'pick_up_location'  => 'required',
        ]);

        $pickUpLocation = new PickUpLocation();

        $pickUpLocation->pick_up_location = $request->pick_up_location;

        $pickUpLocation->save();

        Session::flash('success', 'You have successfully saved pick up location!');

        return back();
    }

    public function editPickUpLocation($id)
    {
        $pickUp = PickUpLocation::find($id);

        return view('back-end.places.pick-up.edit', compact('pickUp'));
    }

    public function updatePickUpLocation(Request $request, $id)
    {
        $pickUpLocation = PickUpLocation::find($id);

        $this->validate($request, [
            'pick_up_location'  => 'required',
        ]);


        $pickUpLocation->pick_up_location = $request->pick_up_location;

        $pickUpLocation->save();

        Session::flash('success', 'You have successfully edited pick up location!');

        return redirect('admin/pick-up/location');
    }

    public function destroy($id)
    {
        $pickUpLocation = PickUpLocation::find($id);

        $pickUpLocation->delete();

        Session::flash('success', 'You have successfully deleted pick up location!');

        return back();
    }
}
