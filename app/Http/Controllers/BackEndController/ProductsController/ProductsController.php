<?php

namespace App\Http\Controllers\BackEndController\ProductsController;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use phpDocumentor\Reflection\DocBlock\Tags\See;
use Session;
use Image;
use Storage;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::latest()
	                        ->paginate(15);

        return view('back-end.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back-end.product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
	        'product_name'      => 'required',
	        'product_price'     => 'required',
	        'product_quantity'  => 'required',
	        'product_image'     => 'required|mimes:jpg,jpeg,png,bmp,svg',
	        'product_details'   => 'required'
        ]);

        $product = new Product;

	    $product->product_name      = $request->product_name;
	    $product->product_price     = $request->product_price;
	    $product->product_quantity  = $request->product_quantity;
	    $product->product_details   = $request->product_details;

	    if ($request->hasfile('product_image')) {
		    $image = $request->file('product_image');
		    $filename = time() . '.' . $image->getClientOriginalExtension();
		    $location = public_path('photos/products/' . $filename);
		    Image::make($image)->resize(200, 200)->save($location);

		    $product->product_image = $filename;
	    }

	    $product->save();

	    Session::flash('success', 'Your product has successfully been created!');

	    return redirect('admin/products/');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('back-end.product.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$product = Product::find($id);

	    $this->validate($request, [
		    'product_name'      => 'required',
		    'product_price'     => 'required',
		    'product_quantity'  => 'required',
		    'product_image'     => 'mimes:jpg,jpeg,png,bmp,svg',
		    'product_details'   => 'required'
	    ]);

	    $product->product_name      = $request->product_name;
	    $product->product_price     = $request->product_price;
	    $product->product_quantity  = $request->product_quantity;
	    $product->product_details   = $request->product_details;

	    if ($request->hasfile('product_image')) {
		    // Add photo if updated
		    $image = $request->file('product_image');
		    $filename = time() . '.' . $image->getClientOriginalExtension();
		    $location = public_path('photos/products/' . $filename);
		    Image::make($image)->save($location);

		    $oldFileName = $product->product_image;

		    // Update the database
		    $product->product_image = $filename;

		    // Delete old image
		    unlink(public_path('photos/products/' . $oldFileName));
	    }

	    $product->save();

	    Session::flash('success', 'Your product has successfully been updated!');

	    return redirect('admin/products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);

	    unlink(public_path('photos/products/' . $product->product_image));

        $product->delete();

        Session::flash('success', 'Your product has successfully been deleted!');

        return back();
    }
}
