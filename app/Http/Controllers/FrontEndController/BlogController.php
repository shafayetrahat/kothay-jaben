<?php

namespace App\Http\Controllers\FrontEndController;

use App\BlogPost;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogPosts = BlogPost::latest()
                                ->filter(request()->only(['month', 'year']))
                                ->paginate(3);

        $archives = BlogPost::selectRaw('year(created_at) year, monthname(created_at) month, count(*) published')
                                ->groupBy('year', 'month')
                                ->orderByRaw('min(created_at) desc')
                                ->get()
                                ->toArray();

        return view('front-end.blog.blog', compact('blogPosts', 'archives', 'pegi'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(BlogPost $blogPost)
    {
        return view('front-end.blog.show', compact('blogPost'));
    }
}
