<?php

namespace App\Http\Controllers\FrontEndController\CarController;

use Session;
use App\TypeLocalBooking;
use App\TypeTodayBooking;
use App\TypeHourlyBooking;
use App\TypeWeddingBooking;
use App\TypeTouristBooking;
use Illuminate\Http\Request;
use App\TypeBodyContractBooking;
use App\Http\Controllers\Controller;

class CarBookingTypeController extends Controller
{
    public function tripToday()
    {
        return view('front-end.cars.trip-type.index');
    }

    public function tripTodayBooking(Request $request)
	{
		if (\Auth::check()) {
			$this->validate($request, [
				'car_id' => 'required',
				'car_name' => 'required',
				'pick_up_location' => 'required',
				'drop_off_location' => 'required',
				'pick_up_date' => 'required',
				'pick_up_time' => 'required',
				'total_people' => 'required',
			]);

			$typeTodayBooking = new TypeTodayBooking();

			$typeTodayBooking->user_id				= \Auth::user()->id;
			$typeTodayBooking->car_id				= $request->car_id;
			$typeTodayBooking->car_name				= $request->car_name;
			$typeTodayBooking->pick_up_location		= $request->pick_up_location;
			$typeTodayBooking->drop_off_location	= $request->drop_off_location;
			$typeTodayBooking->pick_up_date			= $request->pick_up_date;
			$typeTodayBooking->pick_up_time			= $request->pick_up_time;
			$typeTodayBooking->total_people			= $request->total_people;

			$typeTodayBooking->save();

			Session::flash('success', 'You have successfully booked your car! We\'ll contact you soon.');

			return redirect('/user-profile');
		}

		Session::flash('danger', 'You have to be logged in to book a car!');

		return redirect('/login');
	}

    public function tripLocal()
    {
        return view('front-end.cars.trip-type.index');
    }

    public function tripLocalBooking(Request $request)
	{
		if (\Auth::check()) {
			$this->validate($request, [
				'car_id' 				=> 'required',
				'car_name' 				=> 'required',
				'pick_up_location' 		=> 'required',
				'drop_off_location'		=> 'required',
				'pick_up_date' 			=> 'required',
				'pick_up_time' 			=> 'required',
				'total_people' 			=> 'required',
			]);

			$typeLocalBooking = new TypeLocalBooking();

			$typeLocalBooking->user_id				= \Auth::user()->id;
			$typeLocalBooking->car_id				= $request->car_id;
			$typeLocalBooking->car_name				= $request->car_name;
			$typeLocalBooking->pick_up_location		= $request->pick_up_location;
			$typeLocalBooking->drop_off_location	= $request->drop_off_location;
			$typeLocalBooking->pick_up_date			= $request->pick_up_date;
			$typeLocalBooking->pick_up_time			= $request->pick_up_time;
			$typeLocalBooking->total_people			= $request->total_people;

			$typeLocalBooking->save();

			Session::flash('success', 'You have successfully booked your car! We\'ll contact you soon.');

			return redirect('/user-profile');
		}

		Session::flash('danger', 'You have to be logged in to book a car!');

		return redirect('/login');
	}

    public function tripTourist()
    {
        return view('front-end.cars.trip-type.index');
    }

    public function tripTouristBooking(Request $request)
	{
		if (\Auth::check()) {
			$this->validate($request, [
				'car_id' => 'required',
				'car_name' => 'required',
				'tourist_trip_location' => 'required',
				'pick_up_date' => 'required',
				'pick_up_time' => 'required',
				'pick_up_location' => 'required',
				'total_people' => 'required',
				'no_of_day' => 'required',
				'hotel_name' => 'required',
				'price_range' => 'required',
			]);

			$typeTouristBooking = new TypeTouristBooking();

			$typeTouristBooking->user_id = \Auth::user()->id;
			$typeTouristBooking->car_id = $request->car_id;
			$typeTouristBooking->car_name = $request->car_name;
			$typeTouristBooking->tourist_trip_location = $request->tourist_trip_location;
			$typeTouristBooking->pick_up_date = $request->pick_up_date;
			$typeTouristBooking->pick_up_time = $request->pick_up_time;
			$typeTouristBooking->pick_up_location = $request->pick_up_location;
			$typeTouristBooking->total_people = $request->total_people;
			$typeTouristBooking->no_of_day = $request->no_of_day;
			$typeTouristBooking->hotel_name = $request->hotel_name;
			$typeTouristBooking->price_range = $request->price_range;

			$typeTouristBooking->save();

			Session::flash('success', 'You have successfully booked a car');

			return redirect('/user-profile');
		}

		Session::flash('danger', 'You have to be logged in to book a car!');

		return redirect('/login');
	}

    public function tripHourly()
    {
        return view('front-end.cars.trip-type.index');
    }

	public function tripHourlyBooking(Request $request)
	{
		if (\Auth::check()) {
			$this->validate($request, [
				'car_id' 				=> 'required',
				'car_name' 				=> 'required',
				'pick_up_location' 		=> 'required',
				'drop_off_location'		=> 'required',
				'pick_up_date' 			=> 'required',
				'pick_up_time' 			=> 'required',
				'time_span'				=> 'required',
				'total_people' 			=> 'required',
			]);

			$typeHourlyBooking = new TypeHourlyBooking();

			$typeHourlyBooking->user_id				= \Auth::user()->id;
			$typeHourlyBooking->car_id				= $request->car_id;
			$typeHourlyBooking->car_name			= $request->car_name;
			$typeHourlyBooking->pick_up_location	= $request->pick_up_location;
			$typeHourlyBooking->drop_off_location	= $request->drop_off_location;
			$typeHourlyBooking->pick_up_date		= $request->pick_up_date;
			$typeHourlyBooking->pick_up_time		= $request->pick_up_time;
			$typeHourlyBooking->time_span			= $request->time_span;
			$typeHourlyBooking->total_people		= $request->total_people;

			$typeHourlyBooking->save();

			Session::flash('success', 'You have successfully booked your car! We\'ll contact you soon.');

			return redirect('/user-profile');
		}

		Session::flash('danger', 'You have to be logged in to book a car!');

		return redirect('/login');
	}

    public function tripBodyContract()
    {
        return view('front-end.cars.trip-type.index');
    }

    public function tripBodyContractBooking(Request $request)
	{
		if (\Auth::check()) {
			$this->validate($request, [
				'car_id'					=> 'required',
				'car_name'					=> 'required',
				'body_contract_location'	=> 'required',
				'pick_up_date'				=> 'required',
				'pick_up_time'				=> 'required',
				'driver_acco'				=> 'required',
				'driver_food'				=> 'required',
				'total_people'				=> 'required',
				'no_of_day'					=> 'required',
				'rental_price'				=> 'required',
			]);

			$typeBodyContractBooking = new TypeBodyContractBooking();

			$typeBodyContractBooking->user_id					= \Auth::user()->id;
			$typeBodyContractBooking->car_id					= $request->car_id;
			$typeBodyContractBooking->car_name					= $request->car_name;
			$typeBodyContractBooking->body_contract_location	= $request->body_contract_location;
			$typeBodyContractBooking->pick_up_date				= $request->pick_up_date;
			$typeBodyContractBooking->pick_up_time				= $request->pick_up_time;
			$typeBodyContractBooking->driver_acco				= $request->driver_acco;
			$typeBodyContractBooking->driver_food				= $request->driver_food;
			$typeBodyContractBooking->total_people				= $request->total_people;
			$typeBodyContractBooking->no_of_day					= $request->no_of_day;
			$typeBodyContractBooking->rental_price				= $request->rental_price;

			$typeBodyContractBooking->save();

			Session::flash('success', 'You have successfully booked a car');

			return redirect('/user-profile');
		}

		Session::flash('danger', 'You have to be logged in to book a car!');

		return redirect('/login');
	}

    public function hireWedding()
    {
        return view('front-end.cars.trip-type.hire-wedding');
    }

	public function weddingBooking(Request $request)
	{
		if (\Auth::check()) {
			$this->validate($request, [
				'car_id'					=> 'required',
				'wedding_car'				=> 'required',
				'wedding_car_decoration'	=> 'required',
				'guest_car'					=> 'required',
				'journey_location'			=> 'required',
				'wedding_date'				=> 'required',
				'center_name'				=> 'required',
				'pick_up_time'				=> 'required',
			]);

			$typeWeddingBooking = new TypeWeddingBooking();

			$typeWeddingBooking->user_id					= \Auth::user()->id;
			$typeWeddingBooking->car_id						= $request->car_id;
			$typeWeddingBooking->wedding_car				= $request->wedding_car;
			$typeWeddingBooking->wedding_car_decoration		= $request->wedding_car_decoration;
			$typeWeddingBooking->guest_car					= $request->guest_car;
			$typeWeddingBooking->journey_location			= $request->journey_location;
			$typeWeddingBooking->wedding_date				= $request->wedding_date;
			$typeWeddingBooking->center_name				= $request->center_name;
			$typeWeddingBooking->pick_up_time				= $request->pick_up_time;

			$typeWeddingBooking->save();

			Session::flash('success', 'You have successfully booked your wedding car! We wi\'ll contact you soon.');

			return redirect('/user-profile');
		}

		Session::flash('danger', 'You have to be logged in to book a car!');

		return redirect('/login');
	}
}
