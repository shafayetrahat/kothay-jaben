<?php

namespace App\Http\Controllers\FrontEndController\CarController;

use App\Car;
use App\CarPricing;
use App\BodyContractPricing;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class CarsController extends Controller
{
    public function index()
    {
        return view('front-end.cars.index');
    }

	public function todayShow()
	{
		$car = Car::where('car_name', Input::get('car_type'))->first();

		return view('front-end.cars.today-trip-car-details',  compact('car'));
	}

	public function localShow()
	{
		$car = Car::where('car_name', Input::get('car_type'))->first();

		return view('front-end.cars.local-trip-car-details',  compact('car'));
	}

    public function touristShow()
    {
		$touristTrip = CarPricing::where([
			['tourist_trip_location', '=', Input::get('tourist_place')],
			['car_name', '=', Input::get('car_type') ]
		])->first();

		$car = Car::where('car_name', Input::get('car_type'))->first();

    	return view('front-end.cars.tourist-car-details',  compact('touristTrip', 'car'));
    }

	public function hourlyShow()
	{
		$car = Car::where('car_name', Input::get('car_type'))->first();

		return view('front-end.cars.hourly-trip-car-details',  compact('car'));
	}

	public function bodyShow()
	{
		$bodyContract = BodyContractPricing::where([
			['body_contract_location', '=', Input::get('body_contract_location')],
			['car_name', '=', Input::get('car_type') ]
		])->first();

		$car = Car::where('car_name', Input::get('car_type'))->first();

		return view('front-end.cars.body-car-details',  compact('bodyContract', 'car'));
	}

	public function weddingShow()
	{
		$car = Car::where('car_name', Input::get('wedding_car'))->first();

		return view('front-end.cars.wedding-car-details', compact('car'));
	}
}
