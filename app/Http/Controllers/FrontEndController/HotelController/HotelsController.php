<?php

namespace App\Http\Controllers\FrontEndController\HotelController;

use Session;
use App\Hotel;
use App\HotelBook;
use App\HotelRoom;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HotelsController extends Controller
{
    public function index()
    {
    	$hotels = Hotel::latest()
		                ->paginate(10);

    	return view('front-end.hotels.hotels-list', compact('hotels'));
    }

    public function showRoom($id)
    {
    	$hotels = Hotel::latest()->find($id);

    	return view('front-end.hotels.show-rooms', compact('hotels'));
    }

    public function roomDetails($id)
    {
    	$hotelRooms = HotelRoom::find($id);

    	return view('front-end.hotels.room-details', compact('hotelRooms'));
    }

    public function store(Request $request)
    {
        if (\Auth::check()) {
            $this->validate($request, [
                'room_id'       => 'required',
                'hotel_name'    => 'required',
                'room_title'    => 'required',
                'room_rent'     => 'required',
                'check_in'      => 'required',
                'check_out'     => 'required',
                'adults'        => 'required',
                'children'      => 'required'
            ]);

            $hotelBook = new HotelBook;

            $hotelBook->user_id         = \Auth::user()->id;
            $hotelBook->room_id         = $request->room_id;
            $hotelBook->hotel_name      = $request->hotel_name;
            $hotelBook->room_title      = $request->room_title;
            $hotelBook->room_rent       = $request->room_rent;
            $hotelBook->check_in        = $request->check_in;
            $hotelBook->check_out       = $request->check_out;
            $hotelBook->adults          = $request->adults;
            $hotelBook->children        = $request->children;

            $hotelBook->save();

            Session::flash('success', 'You have successfully booked hotel');

            return redirect('/user-profile');
        }

        Session::flash('danger', 'You have to be logged in to book hotel!');

        return redirect('/login');
    }
}
