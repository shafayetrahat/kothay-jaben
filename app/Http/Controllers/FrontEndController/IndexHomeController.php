<?php

namespace App\Http\Controllers\FrontEndController;

use App\CarPackage;
use App\HotelPackage;
use App\SliderPhoto;
use App\Http\Controllers\Controller;
use App\TourPackage;

class IndexHomeController extends Controller
{
    public function index() {
    	$sliderImages   = SliderPhoto::all();
    	$carPackages    = CarPackage::all();
    	$tourPackages   = TourPackage::all();
    	$hotelPackages  = HotelPackage::all();

    	return view('front-end.home.index', compact('sliderImages', 'carPackages', 'tourPackages', 'hotelPackages'));
    }
}
