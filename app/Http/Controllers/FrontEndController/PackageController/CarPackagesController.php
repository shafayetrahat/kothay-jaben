<?php

namespace App\Http\Controllers\FrontEndController\PackageController;

use Session;
use App\CarPackage;
use App\CarPackageBooking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CarPackagesController extends Controller
{
    public function detailedPackage($id)
    {
        $carPackage = CarPackage::find($id);

        return view('front-end.cars.car-package.detailed-car-package', compact('carPackage'));
    }

    public function carBook(Request $request)
    {
        if (\Auth::check()) {
            $this->validate($request, [
                'car_package_id'    => 'required',
                'car_name'          => 'required',
                'car_rent'          => 'required',
                'check_in'          => 'required',
                'people'            => 'required',
            ]);

            $carPBooking = new CarPackageBooking();

            $carPBooking->user_id           = \Auth::user()->id;
            $carPBooking->car_package_id    = $request->car_package_id;
            $carPBooking->car_name          = $request->car_name;
            $carPBooking->car_rent          = $request->car_rent;
            $carPBooking->check_in          = $request->check_in;
            $carPBooking->people            = $request->people;

            $carPBooking->save();

            Session::flash('success', 'You have successfully booked your car package! We will get back to you soon.');

            return redirect('/user-profile');
        }

        Session::flash('danger', 'You have to be logged in to book car package!');

        return redirect('/login');
    }
}