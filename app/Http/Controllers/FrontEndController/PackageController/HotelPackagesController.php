<?php

namespace App\Http\Controllers\FrontEndController\PackageController;

use Session;
use App\HotelPackage;
use Illuminate\Http\Request;
use App\HotelPackageBooking;
use App\Http\Controllers\Controller;

class HotelPackagesController extends Controller
{
    public function hotelPackage()
    {
        $hotelPackages = HotelPackage::latest()
                                        ->paginate(10);

        return view('front-end.hotels.hotel-package.hotel-package', compact('hotelPackages'));
    }

    public function detailedPackage($id)
    {
        $hotelPackage = HotelPackage::find($id);

        return view('front-end.hotels.hotel-package.detailed-hotel-package', compact('hotelPackage'));
    }

    public function hpBook(Request $request)
    {
        if (\Auth::check()) {
            $this->validate($request, [
                'hotel_package_id'      => 'required',
                'hotel_name'            => 'required',
                'hotel_rent'            => 'required',
                'check_in'              => 'required',
            ]);

            $hpBooking = new HotelPackageBooking();

            $hpBooking->user_id             = \Auth::user()->id;
            $hpBooking->hotel_package_id    = $request->hotel_package_id;
            $hpBooking->hotel_name          = $request->hotel_name;
            $hpBooking->hotel_rent          = $request->hotel_rent;
            $hpBooking->check_in            = $request->check_in;

            $hpBooking->save();

            Session::flash('success', 'You have successfully booked your hotel package! We will get back to you soon.');

            return redirect('/user-profile');
        }

        Session::flash('danger', 'You have to be logged in to book hotel package!');

        return redirect('/login');
    }
}
