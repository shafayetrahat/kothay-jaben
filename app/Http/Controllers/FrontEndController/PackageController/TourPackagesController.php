<?php

namespace App\Http\Controllers\FrontEndController\PackageController;

use Session;
use App\TourPackage;
use App\Http\Controllers\Controller;
use App\TourPackageBooking;
use Illuminate\Http\Request;

class TourPackagesController extends Controller
{
    public function tourPackage()
    {
        $tourPackages = TourPackage::latest()
                                    ->paginate(10);

        return view('front-end.tour-package.tour-package', compact('tourPackages'));
    }

    public function detailedPackage($id)
    {
        $tourPackage = TourPackage::find($id);

        return view('front-end.tour-package.detailed-tour-package', compact('tourPackage'));
    }

    public function tourPBook(Request $request)
    {
        if (\Auth::check()) {
            $this->validate($request, [
                'tour_package_id'       => 'required',
                'tour_package_name'     => 'required',
                'package_price'         => 'required',
                'total_people'          => 'required',
                'tour_date'             => 'required',
            ]);

            $tourPBooking = new TourPackageBooking();

            $tourPBooking->user_id              = \Auth::user()->id;
            $tourPBooking->tour_package_id      = $request->tour_package_id;
            $tourPBooking->tour_package_name    = $request->tour_package_name;
            $tourPBooking->package_price        = $request->package_price;
            $tourPBooking->total_people         = $request->total_people;
            $tourPBooking->tour_date            = $request->tour_date;

            $tourPBooking->save();

            Session::flash('success', 'You have successfully booked your tour package! We will get back to you soon.');

            return redirect('/user-profile');
        }

        Session::flash('danger', 'You have to be logged in to book tour package!');

        return redirect('/login');
    }
}
