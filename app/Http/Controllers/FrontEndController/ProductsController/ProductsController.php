<?php

namespace App\Http\Controllers\FrontEndController\ProductsController;

use App\Car;
use App\Cart;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class ProductsController extends Controller
{
    public function index()
    {
    	$products = Product::latest()
		                    ->paginate(12);

    	return view('front-end.products.products', compact('products'));
    }

	public function getAddToCart(Request $request, $id)
    {
    	$product = Product::find($id);
    	$oldCart = Session::has('cart') ? Session::get('cart') : null;
    	$cart = Cart::restoreCart($oldCart);

    	$cart->add($product, $product->id);
    	$request->session()->put('cart', $cart);
    	return back();
    }

	public function getReduceByOne($id)
	{
		$oldCart = Session::has('cart') ? Session::get('cart') : null;
		$cart = Cart::restoreCart($oldCart);

		$cart->reduceByOne($id);
		if (count($cart->items) > 0) {
			Session::put('cart', $cart);
		} else {
			Session::forget('cart');
		}

		return back();
	}

	public function getIncreaseByOne($id)
	{
		$oldCart = Session::has('cart') ? Session::get('cart') : null;
		$cart = Cart::restoreCart($oldCart);

		$cart->increaseByOnce($id);
		Session::put('cart', $cart);

		return back();
	}

	public function getRemoveItem($id)
	{
		$oldCart = Session::has('cart') ? Session::get('cart') : null;
		$cart = Cart::restoreCart($oldCart);

		$cart->removeItem($id);
		if (count($cart->items) > 0) {
			Session::put('cart', $cart);
		} else {
			Session::forget('cart');
		}

		return back();
	}

	public function getCart()
    {
    	if (!Session::has('cart')) {
    		return view('front-end.products.shopping-cart');
	    }
	    $oldCart = Session::get('cart');
    	$cart = Cart::restoreCart($oldCart);

    	return view('front-end.products.shopping-cart', ['products' => $cart->items, 'totalPrice' => $cart->totalPrice]);
    }
}
