<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PickUpLocation extends Model
{
    protected $fillable = [
        'pick_up_location'
    ];
}
