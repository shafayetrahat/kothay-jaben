<?php

namespace App\Providers;

use App\DropOffLocation;
use App\HotelPackageBooking;
use App\PickUpLocation;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use App\BlogPost;
use App\ContactInfo;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        view()->composer('front-end.layouts.footer', function ($view) {
            $blogPosts = BlogPost::latest()->paginate(3);
            $view->with('blogPosts', $blogPosts);
        });

        view()->composer('front-end.layouts.footer', function ($view) {
            $view->with('contactInfo', ContactInfo::all());
        });

        view()->composer('front-end.contact', function ($view) {
            $view->with('contactInfo', ContactInfo::all());
        });

        view()->composer('front-end.layouts.navbar', function ($view) {
            $view->with('contactInfo', ContactInfo::all());
        });

        view()->composer('back-end.layouts.navbar', function ($view) {
            $hotelPackage = HotelPackageBooking::all();
            $view->with('hotelPackage', $hotelPackage);
        });

		view()->composer([
			'front-end.cars.trip-type.trip-today',
			'front-end.cars.trip-type.trip-local',
			'front-end.cars.trip-type.trip-tourist',
			'front-end.cars.trip-type.trip-hourly',
		], function ($view) {
			$view
				->with('pickUpLocation', PickUpLocation::orderBy('pick_up_location')->get())
				->with('dropOffLocation', DropOffLocation::orderBy('drop_off_location')->get());
		});

		view()->composer('front-end.cars.trip-type.trip-body-contract', function ($view) {
			$view->with('pickUpLocation', PickUpLocation::orderBy('pick_up_location')->get());
		});
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
