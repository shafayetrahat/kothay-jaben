<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SliderPhoto extends Model
{
    protected $fillable = [
        'slider_text',
        'package_link',
        'slider_image'
    ];
}
