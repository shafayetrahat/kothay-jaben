<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourPackage extends Model
{
    protected $fillable = [
        'package_name',
        'package_price',
        'hotel_name',
        'hotel_location',
        'package_image',
        'package_details'
    ];

    public function packagePhotos()
    {
        return $this->hasMany('App\TourPackagePhoto');
    }
}
