<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourPackageBooking extends Model
{
    protected $fillable = [
        'tour_package_id',
		'confirmed',
        'tour_package_name',
        'package_price',
        'total_people',
        'tour_date',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
