<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourPackagePhoto extends Model
{
    protected $fillable = [
        'photos'
    ];

    public function tourPackage()
    {
        return $this->belongsTo('App\TourPackage');
    }

    public function delete()
    {
        \File::delete([
            $this->photos
        ]);

        parent::delete();
    }
}
