<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TouristTrip extends Model
{
    protected $fillable = [
    	'tourist_trip_location'
	];
}
