<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeBodyContractBooking extends Model
{
    protected $fillable = [
		'car_id',
		'car_name',
		'confirmed',
		'body_contract_location',
		'pick_up_date',
		'pick_up_time',
		'driver_acco',
		'driver_food',
		'total_people',
		'no_of_day',
		'rental_price',
	];

	public function user()
	{
		return $this->belongsTo('App\User');
	}
}
