<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeHourlyBooking extends Model
{
	protected $fillable = [
		'car_id',
		'car_name',
		'confirmed',
		'pick_up_location',
		'drop_off_location',
		'pick_up_date',
		'pick_up_time',
		'time_span',
		'total_people',
	];

	public function user()
	{
		return $this->belongsTo('App\User');
	}
}
