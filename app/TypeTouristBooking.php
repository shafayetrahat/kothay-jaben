<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeTouristBooking extends Model
{
    protected $fillable = [
		'car_id',
		'car_name',
		'confirmed',
		'tourist_trip_location',
		'pick_up_date',
		'pick_up_time',
		'pick_up_location',
		'total_people',
		'no_of_day',
		'hotel_name',
		'price_range',
	];

	public function user()
	{
		return $this->belongsTo('App\User');
	}
}
