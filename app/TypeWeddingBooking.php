<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeWeddingBooking extends Model
{
    protected $fillable = [
		'car_id',
		'wedding_car',
		'confirmed',
		'wedding_car_decoration',
		'guest_car',
		'journey_location',
		'wedding_date',
		'center_name',
		'pick_up_time',
	];

	public function user()
	{
		return $this->belongsTo('App\User');
	}
}
