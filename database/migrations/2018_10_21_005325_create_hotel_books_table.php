<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_books', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
	        $table->integer('room_id')->unsigned();
			$table->integer('confirmed')->default(0);
	        $table->string('hotel_name');
	        $table->string('room_title');
	        $table->integer('room_rent')->unsigned();
	        $table->string('check_in');
	        $table->string('check_out');
	        $table->integer('adults')->unsigned();
	        $table->integer('children')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel_books');
    }
}
