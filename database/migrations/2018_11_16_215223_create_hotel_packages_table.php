<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('package_name');
            $table->integer('previous_rent')->unsigned();
            $table->integer('current_rent')->unsigned();
            $table->string('hotel_name');
            $table->string('hotel_location');
            $table->string('package_image');
            $table->text('package_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel_packages');
    }
}
