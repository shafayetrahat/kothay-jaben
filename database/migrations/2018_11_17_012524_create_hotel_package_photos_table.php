<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelPackagePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_package_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hotel_package_id')->unsigned();
            $table->foreign('hotel_package_id')->references('id')->on('hotel_packages')->onDelete('cascade');
            $table->string('photos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel_package_photos');
    }
}
