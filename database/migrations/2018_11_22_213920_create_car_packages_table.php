<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('package_name');
            $table->integer('previous_price')->unsigned();
            $table->integer('current_price')->unsigned();
            $table->string('car_name');
            $table->string('package_image');
            $table->text('package_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_packages');
    }
}
