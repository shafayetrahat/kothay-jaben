<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarPackagePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_package_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('car_package_id')->unsigned();
            $table->foreign('car_package_id')->references('id')->on('car_packages')->onDelete('cascade');
            $table->string('photos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_package_photos');
    }
}
