<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypeTouristBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_tourist_bookings', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->integer('car_id')->unsigned();
			$table->integer('confirmed')->default(0);
			$table->string('car_name');
			$table->string('tourist_trip_location');
			$table->string('pick_up_date');
			$table->string('pick_up_time');
			$table->string('pick_up_location');
			$table->integer('total_people')->unsigned();
			$table->integer('no_of_day')->unsigned();
			$table->string('hotel_name');
			$table->string('price_range');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('type_tourist_bookings');
    }
}
