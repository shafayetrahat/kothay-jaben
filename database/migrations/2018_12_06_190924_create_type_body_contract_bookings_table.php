<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypeBodyContractBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_body_contract_bookings', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->integer('car_id')->unsigned();
			$table->integer('confirmed')->default(0);
			$table->string('car_name');
			$table->string('body_contract_location');
			$table->string('pick_up_date');
			$table->string('pick_up_time');
			$table->string('driver_acco');
			$table->string('driver_food');
			$table->integer('total_people')->unsigned();
			$table->integer('no_of_day')->unsigned();
			$table->string('rental_price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('type_body_contract_bookings');
    }
}
