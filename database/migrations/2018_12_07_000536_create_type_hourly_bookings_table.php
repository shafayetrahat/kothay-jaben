<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypeHourlyBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_hourly_bookings', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->integer('car_id')->unsigned();
			$table->integer('confirmed')->default(0);
			$table->string('car_name');
			$table->string('pick_up_location');
			$table->string('drop_off_location');
			$table->string('pick_up_date');
			$table->string('time_span');
			$table->string('pick_up_time');
			$table->integer('total_people')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('type_hourly_bookings');
    }
}
