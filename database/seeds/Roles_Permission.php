<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use App\Permission;


class Roles_Permission extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $admin = new Role();
        $admin->name         = 'admin';
        $admin->display_name = 'User Administrator'; // optional
        $admin->description  = 'User is allowed to manage and edit other users'; // optional
        $admin->save();
////
        $default = new Role();
        $default->name         = 'default';
        $default->display_name = 'User '; // optional
        $default->description  = 'User is allowed '; // optional
        $default->save();

//        $Award_power = new Role();
//        $Award_power->name         = 'Award_giving_power';
//        $Award_power->display_name = 'Volunteer '; // optional
//        $Award_power->description  = 'Volunteer is allowed '; // optional
//        $Award_power->save();

//        $editUser = new Permission();
//        $editUser->name         = 'edit-event';
//        $editUser->display_name = 'Edit event'; // optional
//// Allow a user to...
//        $editUser->description  = 'edit existing event'; // optional
//        $editUser->save();
//
//        $admin->attachPermission($editUser);
//
        $hash = App::make('hash');
//
//
//        $root = new User();
//        $root->name = 'admin';
//        $root->email = 'admin@gmail.com';
//        $user_id= 1;
//        $root->id=$user_id;
//        $coin_store->user_id=$user_id;
//        $root->password = $hash->make('bot123');
//        $root->save();
//        $coin_store->save();
//        $root->attachRole($admin);


        $root1 = new User();
        $root1->name = 'admin';
        $root1->email = 'admin@gmail.com';
        $user_id1= 2;
        $root1->id=$user_id1;
        $root1->password = $hash->make('bot123');
        $root1->save();
        $root1->attachRole($admin);



//            $coin_store1 = new Coin_store();
//            $root1 = new User();
//            $root1->name = "LFS" ;
//            $root1->email = "LFS@gmail.com";
//            $user_id1 = 2;
//            $root1->id = $user_id1;
//            $coin_store1->user_id = $user_id1;
//            $root1->password = $hash->make("man23");
//            $root1->save();
//            $coin_store1->save();
//            $root1->attachRole($default);





// equivalent to $admin->perms()->sync(array($createPost->id));

    }
}