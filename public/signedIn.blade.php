<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <!-- Firebase App is always required and must be first -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <script src="https://www.gstatic.com/firebasejs/5.2.0/firebase-app.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Add additional services that you want to use -->
    <script src="https://www.gstatic.com/firebasejs/5.2.0/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.2.0/firebase-functions.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.2.0/firebase.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <title>Sign in</title>
    <script>
        var config = {
            apiKey: "AIzaSyDDgN4LDeGQySnAuQjIw7cW9rATZmG2n70",
            authDomain: "kothay-jaben-44733.firebaseapp.com",
            databaseURL: "https://kothay-jaben-44733.firebaseio.com",
            projectId: "kothay-jaben-44733",
            storageBucket: "kothay-jaben-44733.appspot.com",
            messagingSenderId: "469048855801"
        };
        firebase.initializeApp(config);
    </script>
    <script type="text/javascript">
        // FirebaseUI config.
        var uiConfig = {
            signInSuccessUrl: 'http://localhost:8000/mobile_auth',
            signInOptions: [
                // Leave the lines as is for the providers you want to offer your users.
                firebase.auth.PhoneAuthProvider.PROVIDER_ID
            ],
            // Terms of service url.
            tosUrl: '<your-tos-url>'
        };

    </script>
    <script type="text/javascript">
        var phoneNumber;
        initApp = function() {
            firebase.auth().onAuthStateChanged(function(user) {
                if (user) {
                    // User is signed in.
                    phoneNumber = user.phoneNumber;
//                    console.log(phoneNumber);
                    $.ajax({
                        type:'POST',
                        dataType: 'json',
                        url:'/mobile_auth/confirm',
                        data: {
                            number:phoneNumber,
//                    user_id : user
                        },
                        success:function(data){
                            console.log(data);
                    window.location.href="/admin_home";
                        }
                    });
                } else {
                    // User is signed out.
                    document.getElementById('account-details').textContent = 'null';
                }
            }, function(error) {
                console.log(error);
            });
        };

        window.addEventListener('load', function() {
            initApp()
        });
    </script>
</head>
</html>