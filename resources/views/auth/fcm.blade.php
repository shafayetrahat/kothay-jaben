<!DOCTYPE html>
<head>
    {{ csrf_field() }}
    <meta charset="UTF-8">
    <!-- Firebase App is always required and must be first -->
    <script src="https://www.gstatic.com/firebasejs/5.2.0/firebase-app.js"></script>

    <!-- Add additional services that you want to use -->
    <script src="https://www.gstatic.com/firebasejs/5.2.0/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.2.0/firebase-functions.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.2.0/firebase.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script>
        // Initialize Firebase
        <script src="https://www.gstatic.com/firebasejs/5.2.0/firebase.js"></script>
    <script>
        // Initialize Firebase
        var config = {
            apiKey: "AIzaSyDDgN4LDeGQySnAuQjIw7cW9rATZmG2n70",
            authDomain: "kothay-jaben-44733.firebaseapp.com",
            databaseURL: "https://kothay-jaben-44733.firebaseio.com",
            projectId: "kothay-jaben-44733",
            storageBucket: "kothay-jaben-44733.appspot.com",
            messagingSenderId: "469048855801"
        };
        firebase.initializeApp(config);
    </script>

          <script src="https://cdn.firebase.com/libs/firebaseui/3.1.1/firebaseui.js"></script>
        <link type="text/css" rel="stylesheet" href="https://cdn.firebase.com/libs/firebaseui/3.1.1/firebaseui.css" />
        <script type="text/javascript">
            // FirebaseUI config.
            var uiConfig = {
                signInSuccessUrl: 'http://localhost:8000/mobile_auth/confirm',
                signInOptions: [
                    // Leave the lines as is for the providers you want to offer your users.
                    {
                        provider: firebase.auth.PhoneAuthProvider.PROVIDER_ID,
                        defaultCountry: 'BD'
                    }
                ],
                // Terms of service url.
                tosUrl: '<your-tos-url>'
            };

            // Initialize the FirebaseUI Widget using Firebase.
            var ui = new firebaseui.auth.AuthUI(firebase.auth());
            // The start method will wait until the DOM is loaded.
            ui.start('#firebaseui-auth-container', uiConfig);
        </script>
    </head>
<body>
<div class="container-fluid" style="width: 39rem;">
    <br>
    <div class="panel panel-default style">
        <div class="panel-body">
            One more step to go. Please confirm that it's you.
            We will give you message about your result, promo and many more.
        </div>
    </div>
</div>

<!-- The surrounding HTML is left untouched by FirebaseUI.
     Your app may use that space for branding, controls and other customizations.-->

<div id="firebaseui-auth-container"></div>
</body>