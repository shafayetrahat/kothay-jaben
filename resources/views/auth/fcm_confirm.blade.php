<!DOCTYPE html>
<html>
<head>
    {{ csrf_field() }}
    <meta charset="UTF-8">
    <!-- Firebase App is always required and must be first -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <script src="https://www.gstatic.com/firebasejs/5.2.0/firebase-app.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Add additional services that you want to use -->
    <script src="https://www.gstatic.com/firebasejs/5.2.0/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.2.0/firebase-functions.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.2.0/firebase.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <script>
        // Initialize Firebase
        var config = {
            apiKey: "AIzaSyCLQagTWQpnqkmNeew8Ufc5598xll2OMrI",
            authDomain: "fun-with-science.firebaseapp.com",
            databaseURL: "https://fun-with-science.firebaseio.com",
            projectId: "fun-with-science",
            storageBucket: "fun-with-science.appspot.com",
            messagingSenderId: "308934822336"
        };
        firebase.initializeApp(config);
    </script>
        <meta charset="UTF-8">
        <title>Sample FirebaseUI App</title>
          <script type="text/javascript">
            // FirebaseUI config.
            var uiConfig = {
                signInSuccessUrl: 'http://localhost:8000/mobile_auth',
                signInOptions: [
                    // Leave the lines as is for the providers you want to offer your users.
                    {
                        provider: firebase.auth.PhoneAuthProvider.PROVIDER_ID,
                        defaultCountry: 'BD'
                    }
                ],
                // Terms of service url.
                tosUrl: '<your-tos-url>'
            };

        </script>
            <script type="text/javascript">
                var phoneNumber;
                initApp = function() {
                    firebase.auth().onAuthStateChanged(function(user) {
                        if (user) {
                            // User is signed in.
                            phoneNumber = user.phoneNumber;

                        } else {
                            // User is signed out.
                            document.getElementById('account-details').textContent = 'null';
                        }
                    }, function(error) {
                        console.log(error);
                    });
                };

                window.addEventListener('load', function() {
                    initApp()
                });

            </script>


    </head>




<body>
<div id="account-details"></div>
<div class="container-fluid" style="width: 39rem;">
    <br></br>
    <div class="panel panel-default style">
        <div class="panel-body">
            Your mobile number is verified. Please confirm your submission

        <div id="requestdata"></div>
        <form id="phone_num" action="#">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" id="user_id" value="{{\Illuminate\Support\Facades\Auth::user()->id}}" class="form-control">
            <div class="col-md-4 text-center">
            <input type="submit" value="Confirm" class="btn btn-primary">
            </div>
        </form>
        </div>
        <script type="text/javascript">

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(document).ready(function () {
                $('#phone_num').submit(function () {
                    var user_id = $('#user_id').val();
                    $.ajax({
                        type:'POST',
                        dataType: 'json',
                        url:'/mobile_auth/confirm',
                        data: {
                            number:phoneNumber,
                            user_id : user_id
                        },
                        success:function(data){

                            $('#requestdata').append(data);
                            console.log(data);
                            window.location.href="/home";
                        }
                });
            })
            })
        </script>
    </div>
</div>

<!-- The surrounding HTML is left untouched by FirebaseUI.
     Your app may use that space for branding, controls and other customizations.-->

</body>
</html>