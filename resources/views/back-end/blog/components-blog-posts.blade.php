@extends('back-end.layouts.master')

@section('styles')
	<style>
		.margin-b {
			padding: 35px 20px 5px 20px;
			margin: -30px 0 0 0;
		}
		.no-post {
			padding: 80px 0;
		}
	</style>
@endsection

@section('content')
	
	<div class="main-content-container container-fluid px-4">
		<!-- Page Header -->
		<div class="page-header row no-gutters py-4">
			<div class="col-12 col-sm-4 text-center text-sm-left mb-0">
			<span class="text-uppercase page-subtitle">Components</span>
			<h3 class="page-title">Blog Posts</h3>
			</div>
		</div>
        @include('back-end.layouts.messages')
		<!-- End Page Header -->
		<div class="row">
			@if (count($posts))

			@foreach ($posts as $post)
				<div class="col-lg-6 col-xl-4 col-md-6 col-sm-12 mb-4">
					<div class="card card-small card-post card-post--1">
						<div class="card-post__image" style="background-image: url({{ asset('/photos/blog/' . $post->featured_image) }});">
							<a href="#" class="card-post__category badge badge-pill badge-primary">Travel</a>
						</div>
						<div class="card-body">
							<h5 class="card-title">
								<a class="text-fiord-blue" href="/admin/blog/{{ $post->id }}">{{ $post->title }}</a>
							</h5>
							<p class="card-text d-inline-block mb-3"><a class="text-fiord-blue" href="/admin/blog/{{ $post->id }}">{{ substr(strip_tags($post->blog_post), 0, 100) }}{{ strlen(strip_tags($post->blog_post)) > 100 ? "..." : "" }}</a></p>
							<span class="text-muted">{{ $post->created_at->toFormattedDateString() }}</span>
						</div>
						<div class="row">
							<div class="col-lg-6 margin-b">
								<form method="POST" action="/admin/blog/{{ $post->id}}">
									{{ csrf_field() }}
									{{ method_field('DELETE') }}
							
									<div class="form-group">
										<input type="submit" class="btn btn-danger delete-post btn-block" value="Delete Post">
									</div>
								</form>
							</div>
							<div class="col-lg-6 form-group margin-b">
								<a href="{{ url('admin/blog/'. $post->id) .'/edit' }}"><button type="button" class="btn btn-info btn-block">Edit Post</button></a>
							</div>
						</div>
					</div>
				</div>
			@endforeach

			@else
			<div class="col-lg-12 col-md-12 col-sm-12 mb-4">
				<div class="card card-small card-post card-post--1 no-post">
					<h2 class="text-center">No post added yet</h2>
					<div class="row">
						<div class="col-md-4 offset-md-4 text-center mt-4">
							<a href="/admin/blog/create" class="btn btn-success">Add New</a>
						</div>
					</div>
				</div>
			</div>
			@endif
		</div>
	</div>
@endsection

@section('footer')
	<script>
		$('.delete-post').click(function(e){
			e.preventDefault() // Don't post the form, unless confirmed
			if (confirm('Are you sure, you want to delete?')) {
				// Post the form
				$(e.target).closest('form').submit() // Post the surrounding form
			}
		});
	</script>
@endsection
