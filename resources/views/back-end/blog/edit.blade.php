@extends('back-end.layouts.master')

@section('styles')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/quill/1.3.6/quill.snow.css">
	
	<style>
			.m-top {
				margin-top: 1rem
			}
		</style>
@endsection

		
@section('content')
	
	<div class="main-content-container container-fluid px-4">
		<!-- Page Header -->
		<div class="page-header row no-gutters py-4">
			<div class="col-12 col-sm-4 text-center text-sm-left mb-0">
			<span class="text-uppercase page-subtitle">Blog Post</span>
			<h3 class="page-title">Edit Post</h3>
			</div>
		</div>
		<!-- End Page Header -->
		<div class="row">
			<div class="col-lg-12 col-md-12">
			<!-- Add New Post Form -->
			<div class="card card-small mb-3">
				<div class="card-body">

					@include('back-end.layouts.messages')

                    {!! Form::open(['action' => ['BackEndController\BlogPostsController@update', $blogPost->id], 'method' => 'PUT', 'files' => true]) !!}
						{{ Form::text('title', $blogPost->title, ['class' => 'form-control form-control-lg mb-3']) }}

						{{ Form::text('author', $blogPost->author, ['class' => 'form-control form-control-lg mb-3']) }}

						{{ Form::label('featured_image', 'Featured Image:') }}
						{{ Form::file('featured_image', ['class' => 'form-control form-control-lg mb-3']) }}

						{{ Form::textarea('blog_post', $blogPost->blog_post, ['id' => 'article-ckeditor', 'class' => 'form-control form-control-lg mb-3']) }}
						
						{{ Form::submit('Update', ['class' => 'btn btn-info form-control m-top']) }}
					{!! Form::close() !!}
				</div>
			</div>
			<!-- / Add New Post Form -->
			</div>
		</div>
	</div>
@endsection

@section('footer')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/quill/1.3.6/quill.min.js"></script>
	<script src="/js/scripts/app/app-blog-new-post.1.0.0.js"></script>
	<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'article-ckeditor' );
		CKEDITOR.config.extraPlugins = 'justify,oembed,blockquote';
		CKEDITOR.config.entities = false;
        CKEDITOR.config.disableNativeSpellChecker = false;
    </script>
@endsection
