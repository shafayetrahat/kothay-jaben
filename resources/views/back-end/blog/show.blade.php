@extends('back-end.layouts.master')

@section('styles')
    <link rel="stylesheet" type="text/css" href="/css/styles/blog_styles.css">
    <link rel="stylesheet" type="text/css" href="/css/styles/blog_responsive.css">

    <style>
        .blog_post_text p {
            color: black;
        }

        .blog_post h1 {
            color:black;
        }

        .blog_post_text ul {
            margin-left: 30px;
            color: #3e3e3e;
            list-style: disc;
        }

        .blog_post_text ol {
            margin-left: 30px;
	        color: #3e3e3e;
        }
    </style>
@endsection

@section('content')
<div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Blog Post</span>
        <h3 class="page-title">Post Details</h3>
        </div>
    </div>
    @include('back-end.layouts.messages')
    <!-- End Page Header -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <!-- Add New Post Form -->
            <div class="card card-small mb-3">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-8">
							<div class="blog_post">
                                <div class="blog_post_image">
                                    <a href="/admin/blog/{{ $blogPost->id }}/edit" class="float-right btn btn-success" style="margin-bottom: 20px;">Edit</a>
                                    <img src="{{ asset('/photos/blog/' . $blogPost->featured_image) }}" alt="Blog Image">
                                    <div class="blog_post_date d-flex flex-column align-items-center justify-content-center">
                                        <div class="blog_post_month">{{ $blogPost->created_at->toFormattedDateString() }}</div>
                                    </div>
                                </div>
                                <div class="blog_post_meta">
                                    <ul>
                                        <li class="blog_post_meta_item"><a href="#">by {{ $blogPost->author }}</a></li>
                                    </ul>
                                </div>
                                <h1 class="text-center">{{ $blogPost->title }}</h1>
                                <div class="blog_post_text">
                                    <p>{!! $blogPost->blog_post !!}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2"></div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
