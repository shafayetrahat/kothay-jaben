@extends('back-end.layouts.master')

@section('styles')
	<link rel="stylesheet" type="text/css" href="/css/styles/blog_styles.css">
	<link rel="stylesheet" type="text/css" href="/css/styles/blog_responsive.css">

	<style>
		.blog_post_text p {
			color: black;
		}

		.blog_post h1 {
			color:black;
		}
		p {
			line-height: 0;
			color: #3d5170;
			margin-bottom: 10px;
		}
		.card-text b {
			font-weight: 600;
			color: #000;
		}
	</style>
	<?php
		$start = \Carbon\Carbon::parse( $book->check_in);
		$end = \Carbon\Carbon::parse( $book->check_out);

		$days = $end->diffInDays($start);
	?>
@endsection

@section('content')
	<div class="main-content-container container-fluid px-4">
		<!-- Page Header -->
		<div class="page-header row no-gutters py-4">
			<div class="col-12 col-sm-4 text-center text-sm-left mb-0">
				<span class="text-uppercase page-subtitle">Hotel Booking</span>
				<h3 class="page-title">Booking Details</h3>
			</div>
		</div>
		<!-- End Page Header -->
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<!-- Add New Post Form -->
				<div class="row">
					<div class="col-lg-2"></div>
					<div class="col-lg-8">
						<div class="blog_post">
							<div class="card">
								<h3 style="font-size: 30px;" class="card-header text-center">Booking Request</h3>
								<p class="text-center">For</p>
								<h4 class="text-center">{{ $book->hotel_name }}</h4>
								<hr>
								<div class="card-body">
									<h3 class="card-title">
										<b style="font-weight: 600;">{{ $book->room_title }}</b>
										<i style="font-size: 12px;">Booked on {{ $book->created_at->toFormattedDateString() }} (at {{ $book->created_at->format('g:i:s a') }})</i>
									</h3>
									<p class="mb-3">User Name: {{ ucfirst($book->user->name) }}</p>
									<p class="mb-3">User Email: {{ $book->user->email }}</p>
									<p class="mb-4">User Phone: {{ $book->user->mobile_number }}</p>

									<span style="cursor: pointer" class="badge badge-warning" data-toggle="modal" data-target="#confirmByEmail">
										Confirm by Email
									</span>

									<span style="cursor: pointer" class="badge badge-warning" data-toggle="modal" data-target="#confirmByMessage">
										Confirm by Message
									</span>

									<!-- Modal For Email Confirmation -->
									<div class="modal fade" id="confirmByEmail" tabindex="-1" role="dialog" aria-labelledby="confirmByEmailTitle" aria-hidden="true">
										<div class="modal-dialog modal-dialog-centered" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title" id="exampleModalLongTitle">Confirm By Email</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<div class="modal-body">
													<form action="" method="POST">
														<div class="form-group row">
															<label for="fromEmail" class="col-sm-2 col-form-label">From:</label>
															<div class="col-sm-10">
																<input type="email" class="form-control" id="fromEmail" placeholder="Email">
															</div>
														</div>
														<div class="form-group row">
															<label for="toEmail" class="col-sm-2 col-form-label">To:</label>
															<div class="col-sm-10">
																<input type="email" value="{{ $book->user->email }}" class="form-control" id="toEmail">
															</div>
														</div>
														<div class="form-group">
															<label for="message">Write Message:</label>
															<textarea id="message" rows="7" class="form-control"></textarea>
														</div>
														<button type="submit" class="btn btn-primary" data-dismiss="modal">Send</button>
													</form>
												</div>
											</div>
										</div>
									</div>

									<!-- Modal For Message Confirmation -->
									<div class="modal fade" id="confirmByMessage" tabindex="-1" role="dialog" aria-labelledby="confirmByMessageTitle" aria-hidden="true">
										<div class="modal-dialog modal-dialog-centered" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title" id="exampleModalLongTitle">Confirm By Message</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<div class="modal-body">
													<form action="" method="POST">
														<div class="form-group row">
															<label for="fromEmail" class="col-sm-2 col-form-label">From:</label>
															<div class="col-sm-10">
																<input type="text" value="{{ $book->user->mobile_number }}" class="form-control" id="fromEmail">
															</div>
														</div>
														<div class="form-group">
															<label for="messagePhone">Write Message:</label>
															<textarea id="messagePhone" rows="7" class="form-control"></textarea>
														</div>
														<button type="submit" class="btn btn-primary" data-dismiss="modal">Send</button>
													</form>
												</div>
											</div>
										</div>
									</div>

									{{--End of Modals--}}


									<em style="color: red;"><sup>*</sup>Hotel Booked For: {{ $days }} nights</em>
									<p class="card-text">
										<table class="table table-dark">
											<thead>
											<tr>
												<th scope="col">Check-In Date</th>
												<th scope="col">Check-Out Date</th>
												<th scope="col">Adults</th>
												<th scope="col">Children</th>
												<th scope="col">Booking Fee</th>
											</tr>
											</thead>
											<tbody>
											<tr>
												<th>{{ $book->check_in }}</th>
												<td>{{ $book->check_out }}</td>
												<td>{{ $book->adults }}</td>
												<td> {{ $book->children }}</td>
												<td>৳ {{ number_format($book->room_rent) }}</td>
											</tr>
											</tbody>
										</table>
									</p>
									<div class="row">
										<div class="col-md-4 offset-md-8">
											<a href="/admin/hotels/{{ $book->room_id }}/hotel-rooms" target="_blank" class="btn btn-primary text-center">This Room Was Booked</a>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-2"></div>
				</div>
			</div>
		</div>
	</div>
	</div>
@endsection
