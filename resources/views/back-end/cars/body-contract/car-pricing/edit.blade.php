@extends('back-end.layouts.master')

@section('styles')
	<link rel="stylesheet" type="text/css" href="/css/styles/blog_styles.css">
	<link rel="stylesheet" type="text/css" href="/css/styles/blog_responsive.css">

	<style>
		.btn {
			margin-top: 10px;
		}
	</style>
@endsection

@section('content')
	<div class="main-content-container container-fluid px-4">
		<!-- Page Header -->
		<div class="page-header row no-gutters py-4">
			<div class="col-12 col-sm-4 text-center text-sm-left mb-0">
				<span class="text-uppercase page-subtitle">Places</span>
				<h3 class="page-title">Edit Body Contract Location</h3>
			</div>
		</div>
	@include('back-end.layouts.messages')
	<!-- End Page Header -->
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<!-- Add New Post Form -->
				<div class="card card-small mb-3">
					<div class="card-body">
						<div class="row">
							<div class="col-lg-6 left_border">
								<div class="blog_post">
									<form action="/admin/car/body-contract/{{ $bodyCP->id }}" method="POST">
										{{ csrf_field() }}
										{{ method_field('PATCH') }}
										<div class="form-group">
											<input type="text" value="{{ $bodyCP->body_contract_location }}" name="body_contract_location" class="form-control" placeholder="Tourist Trip Location">
										</div>
										<div class="form-group">
											<input type="text" value="{{ $bodyCP->car_name }}" name="car_name" class="form-control" placeholder="Car Name">
										</div>
										<div class="form-group">
											<input type="text" value="{{ $bodyCP->rental_price }}" name="rental_price" class="form-control" placeholder="Rental Price">
										</div>
										<button type="submit" class="btn btn-info mb-4">Submit</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
