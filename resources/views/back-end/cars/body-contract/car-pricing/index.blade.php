@extends('back-end.layouts.master')

@section('styles')
	<link rel="stylesheet" type="text/css" href="/css/styles/blog_styles.css">
	<link rel="stylesheet" type="text/css" href="/css/styles/blog_responsive.css">

	<style>
		.btn {
			margin-top: 10px;
		}
		.hori-row {
			margin-bottom: -80px;
		}
		.chng-2 {
			color: white;
			background: green;
			padding: 1px 3px 1px 3px;
			position: relative;
			bottom: 0px;
			right: 20px;
		}
		.chng {
			color: white;
			background: red;
			padding: 1px 3px 1px 3px;
			border: none;
			position: relative;
			bottom: 22px;
			left: 20px;
			cursor: pointer;
		}
		td {
			color: #000;
		}
		@media only screen and (max-width: 768px) {
			.chng {
				bottom: 28px;
				left: 6px;
			}
		}
	</style>
@endsection

@section('content')
	<div class="main-content-container container-fluid px-4">
		<!-- Page Header -->
		<div class="page-header row no-gutters py-4">
			<div class="col-12 col-sm-4 text-center text-sm-left mb-0">
				<span class="text-uppercase page-subtitle">Tourist Trip</span>
				<h3 class="page-title">Body Contract</h3>
			</div>
		</div>
	@include('back-end.layouts.messages')
	<!-- End Page Header -->
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<!-- Add New Post Form -->
				<div class="card card-small mb-3">
					<div class="card-body">
						<div class="row">
							<div class="col-lg-8 offset-md-2">
								<div class="blog_post">
									<form action="/admin/car/body-contract" method="POST">
										{{ csrf_field() }}
										<div class="form-group">
											<input type="text" value="{{ old('body_contract_location') }}" name="body_contract_location" class="form-control" placeholder="Body Contract Location">
										</div>
										<div class="form-group">
											<input type="text" value="{{ old('car_name') }}" name="car_name" class="form-control" id="name" aria-describedby="product_name" placeholder="Car Name">
										</div>
										<div class="form-group">
											<input type="number" value="{{ old('rental_price') }}" name="rental_price" class="form-control" placeholder="Rental Price">
										</div>
										<button type="submit" class="btn btn-info mb-4">Submit</button>
									</form>
									<hr>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-8 offset-md-2">
								<h3>Body Contract Details</h3>
								<table class="table table-striped">
									<thead>
									<tr>
										<th scope="col"></th>
										<th scope="col"></th>
										<th scope="col"></th>
										<th scope="col"></th>
									</tr>
									</thead>
									<tbody>
								@foreach($bodyCP as $carPricing)
										<tr>
											<td scope="row">{{ $carPricing->body_contract_location }}</td>
											<td>{{ $carPricing->car_name }}</td>
											<td>৳ {{ $carPricing->rental_price }}</td>
											<td>
												<a href="/admin/car/body-contract/{{ $carPricing->id }}/edit" class="ml-5 chng-2 float-right">
													<i class="material-icons" style="color: #fff !important;">
														edit
													</i>
												</a>
												<form method="POST" action="/admin/car/body-contract/{{ $carPricing->id}}">
													{{ csrf_field() }}
													{{ method_field('DELETE') }}
													<div class="form-group">
														<button type="submit" class="delete-place chng float-right" style="margin-top: 5px;">
															<i class="material-icons" style="color: #fff !important;">
																clear
															</i>
														</button>
													</div>
												</form>
											</td>
										</tr>
								@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('footer')
	<script>
		$('.delete-place').click(function(e){
			e.preventDefault() // Don't post the form, unless confirmed
			if (confirm('Are you sure, you want to delete?')) {
				// Post the form
				$(e.target).closest('form').submit() // Post the surrounding form
			}
		});
	</script>
@endsection
