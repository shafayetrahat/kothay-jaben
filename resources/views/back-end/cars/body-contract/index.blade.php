@extends('back-end.layouts.master')

@section('styles')
	<link rel="stylesheet" type="text/css" href="/css/styles/blog_styles.css">
	<link rel="stylesheet" type="text/css" href="/css/styles/blog_responsive.css">

	<style>
		.btn {
			margin-top: 10px;
		}
		.left_border {
			border-right: 1px solid #333;
		}
		.hori-row {
			margin-bottom: -40px;
		}
		.chng-2 {
			color: white;
			background: green;
			padding: 1px 3px 1px 3px;
		}
		.chng {
			color: white;
			background: red;
			padding: 1px 3px 1px 3px;
			border: none;
			position: relative;
			bottom: 22.5px;
			left: 35px;
			cursor: pointer;
		}

		@media only screen and (max-width: 991px) {
			.left_border {
				border-right: none;
			}
		}
	</style>
@endsection

@section('content')
	<div class="main-content-container container-fluid px-4">
		<!-- Page Header -->
		<div class="page-header row no-gutters py-4">
			<div class="col-12 col-sm-4 text-center text-sm-left mb-0">
				<span class="text-uppercase page-subtitle">Body Contract</span>
				<h3 class="page-title">Body Contract Location</h3>
			</div>
		</div>
	@include('back-end.layouts.messages')
	<!-- End Page Header -->
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<!-- Add New Post Form -->
				<div class="card card-small mb-3">
					<div class="card-body">
						<div class="row">
							<div class="col-lg-6 left_border">
								<div class="blog_post">
									<form action="/admin/car/body-contract-location" method="POST">
										{{ csrf_field() }}
										<div class="form-group">
											<input type="text" value="{{ old('body_contract_location') }}" name="body_contract_location" class="form-control" placeholder="Body Contract Location">
										</div>
										<button type="submit" class="btn btn-info mb-4">Add Location</button>
									</form>
								</div>
							</div>
							<div class="col-lg-6">
								<h3>Body Contract Locations</h3>
								@foreach($bodyContract as $location)
									<div class="row hori-row">
										<div class="div col-md-8 offset-md-2">
											<p class="float-left">{{ $location->body_contract_location }}</p>
											<a href="/admin/car/body-contract-location/{{ $location->id }}/edit" class="ml-5 chng-2 float-right">
												<i class="material-icons" style="color: #fff !important;">
													edit
												</i>
											</a>
											<form method="POST" action="/admin/car/body-contract-location/{{ $location->id}}">
												{{ csrf_field() }}
												{{ method_field('DELETE') }}
												<div class="form-group">
													<button type="submit" class="delete-place chng float-right" style="margin-top: 5px;">
														<i class="material-icons" style="color: #fff !important;">
															clear
														</i>
													</button>
												</div>
											</form>
										</div>
										<div class="col-md-6">

										</div>
									</div>
									<hr>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('footer')
	<script>
		$('.delete-place').click(function(e){
			e.preventDefault() // Don't post the form, unless confirmed
			if (confirm('Are you sure, you want to delete?')) {
				// Post the form
				$(e.target).closest('form').submit() // Post the surrounding form
			}
		});
	</script>
@endsection
