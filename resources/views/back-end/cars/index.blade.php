@extends('back-end.layouts.master')

@section('styles')
	<style>
		.margin-b {
			padding: 35px 20px 5px 20px;
			margin: -30px 0 0 0;
		}
		.no-car {
			padding: 80px 0;
		}
	</style>
@endsection

@section('content')

	<div class="main-content-container container-fluid px-4">
		<!-- Page Header -->
		<div class="page-header row no-gutters py-4">
			<div class="col-12 col-sm-4 text-center text-sm-left mb-0">
				<span class="text-uppercase page-subtitle">Cars</span>
				<h3 class="page-title">All Cars</h3>
			</div>
		</div>
		<!-- End Page Header -->
		<div class="row">
			@if (count($cars))
				@foreach ($cars as $car)
					<div class="col-lg-6 col-xl-4 col-md-6 col-sm-12 mb-4">
						<div class="card card-small card-post card-post--1">
							<div class="card-post__image" style="background-image: url({{ asset('/photos/cars/' . $car->car_feature_image) }});">
								<a href="#" class="card-post__category badge badge-pill badge-primary">Car No. {{ $car->id }}</a>
							</div>
							<div class="card-body">
								<h5 class="card-title">
									<a class="text-fiord-blue" href="/admin/cars/{{ $car->id }}">{{ $car->car_name }}</a>
								</h5>
								{{--<p class="card-text d-inline-block mb-3"><a class="text-fiord-blue" href="/admin/cars/{{ $car->id }}">{!! substr(strip_tags($car->car_details), 0, 50) !!}{!! strlen(strip_tags($car->car_details)) > 50 ? "..." : "" !!}</a></p>--}}
								<p class="text-muted">{{ $car->created_at->toFormattedDateString() }}</p>
							</div>
							<div class="row">
								<div class="col-lg-6 margin-b">
									<form method="POST" action="/admin/cars/{{ $car->id}}">
										{{ csrf_field() }}
										{{ method_field('DELETE') }}

										<div class="form-group">
											<input type="submit" class="btn btn-danger delete-car btn-block" value="Delete Entry">
										</div>
									</form>
								</div>
								<div class="col-lg-6 form-group margin-b">
									<a href="{{ url('admin/cars/'. $car->id) .'/edit' }}"><button type="button" class="btn btn-info btn-block">Edit Details</button></a>
								</div>
							</div>
						</div>
					</div>
				@endforeach

			@else
				<div class="col-lg-12 col-md-12 col-sm-12 mb-4">
					<div class="card card-small card-post card-post--1 no-car">
						<h2 class="text-center">No Car Added Yet</h2>
						<div class="row">
							<div class="col-md-4 offset-md-4 text-center mt-4">
								<a href="/admin/cars/create" class="btn btn-success">Add New</a>
							</div>
						</div>
					</div>
				</div>
			@endif
		</div>
	</div>
@endsection

@section('footer')
	<script>
		$('.delete-car').click(function(e){
			e.preventDefault() // Don't post the form, unless confirmed
			if (confirm('Are you sure, you want to delete?')) {
				// Post the form
				$(e.target).closest('form').submit() // Post the surrounding form
			}
		});
	</script>
@endsection
