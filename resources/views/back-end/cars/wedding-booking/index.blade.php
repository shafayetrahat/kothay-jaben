@extends('back-end.layouts.master')

@section('styles')
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
	<style>
		.margin-b {
			padding: 20px;
		}
		.no-car {
			padding: 80px 0;
		}
		.gray_worm {
			background-color: #868e96;
		}
	</style>
@endsection

@section('content')

	<div class="main-content-container container-fluid px-4">
		<!-- Page Header -->
		<div class="page-header row no-gutters py-4">
			<div class="col-12 col-sm-4 text-center text-sm-left mb-0">
				<span class="text-uppercase page-subtitle">Cars</span>
				<h3 class="page-title">Booked Cars</h3>
			</div>
		</div>
		@include('back-end.layouts.messages')
	<!-- End Page Header -->
		<div class="row">
			@if (count($bookedCars))
				@foreach ($bookedCars as $car)
					<div class="col-lg-6 col-xl-4 col-md-6 col-sm-12 mb-4">
						<div class="card card-small card-post card-post--1 {{ $car->confirmed == 1 ? 'gray_worm' : ''}}">
							<div class="card-body">
								<h5 class="card-title">
									<a class="text-fiord-blue" href="/admin/car/wedding-booking/list/{{ $car->id }}">{{ $car->wedding_car }}</a>
								</h5>
								<a href="/admin/car/wedding-booking/list/{{ $car->id }}"><p>User Name: {{ ucfirst($car->user->name) }}</p></a>
								<a href="/admin/car/wedding-booking/list/{{ $car->id }}"><p>Car booking request sent on {{ $car->created_at->toFormattedDateString() }} (at {{ $car->created_at->format('g:i:s a') }})</p></a>
							</div>
							<div class="row">
								<div class="col-md-2"></div>
								<div class="col-md-4 col-sm-3 col-xs-3">
									<form method="POST" action="/admin/car/wedding-booking/list/{{ $car->id}}">
										{{ csrf_field() }}
										{{ method_field('DELETE') }}

										<div class="form-group">
											<input type="submit" class="btn btn-danger btn-block delete-booking" value="Delete">
										</div>
									</form>
								</div>
								@if($car->confirmed == 0)
									<div class="col-md-5 col-sm-3 col-xs-3">
										<form method="POST" action="/admin/car/wedding-booking/list/{{ $car->id}}">
											{{ csrf_field() }}
											{{ method_field('PATCH') }}

											<div class="form-group">
												<input type="submit" class="btn btn-success btn-block" value="Confirmed">
											</div>
										</form>
									</div>
								@else
									<div class="col-md-5 col-sm-3 col-xs-3">
										<form method="POST" action="/admin/car/wedding-booking/list/unconfirmed/{{ $car->id}}">
											{{ csrf_field() }}
											{{ method_field('PATCH') }}

											<div class="form-group">
												<input type="submit" class="btn btn-success btn-block" value="Unconfirmed">
											</div>
										</form>
									</div>
								@endif
							</div>
						</div>
					</div>
				@endforeach

			@else
				<div class="col-lg-12 col-md-12 col-sm-12 mb-4">
					<div class="card card-small card-post card-post--1 no-car">
						<h2 class="text-center">No Cars Booked Yet</h2>
					</div>
				</div>
			@endif
		</div>
		<p class="text-center">{{ $bookedCars->links() }}</p>
	</div>
@endsection

@section('footer')
	<script>
		$('.delete-booking').click(function(e){
			e.preventDefault() // Don't post the form, unless confirmed
			if (confirm('Are you sure, you want to delete?')) {
				// Post the form
				$(e.target).closest('form').submit() // Post the surrounding form
			}
		});
	</script>
@endsection
