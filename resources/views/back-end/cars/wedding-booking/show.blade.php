@extends('back-end.layouts.master')

@section('styles')
	<link rel="stylesheet" type="text/css" href="/css/styles/blog_styles.css">
	<link rel="stylesheet" type="text/css" href="/css/styles/blog_responsive.css">

	<style>
		.blog_post_text p {
			color: black;
		}

		.blog_post h1 {
			color:black;
		}

		.blog_post_text ul {
			margin-left: 30px;
			color: #3e3e3e;
			list-style: disc;
		}

		.blog_post_text ol {
			margin-left: 30px;
			color: #3e3e3e;
		}
		.card-text b {
			font-weight: 600;
			color: #000;
		}
	</style>
@endsection

@section('content')
	<div class="main-content-container container-fluid px-4">
		<!-- Page Header -->
		<div class="page-header row no-gutters py-4">
			<div class="col-12 col-sm-4 text-center text-sm-left mb-0">
				<span class="text-uppercase page-subtitle">Car Booking</span>
				<h3 class="page-title">Wedding Booking Details</h3>
			</div>
		</div>
		<!-- End Page Header -->
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<!-- Add New Post Form -->
				<div class="row">
					<div class="col-lg-2"></div>
					<div class="col-lg-8">
						<div class="blog_post">
							<div class="card">
								<h3 style="font-size: 30px;" class="card-header text-center">Booking Request</h3>
								<hr>
								<div class="card-body">
									<h3 class="card-title">
										<b style="font-weight: 600;">{{ $book->wedding_car }}</b>
										<i style="font-size: 12px;">Booked on {{ $book->created_at->toFormattedDateString() }} (at {{ $book->created_at->format('g:i:s a') }})</i>
									</h3>
									<p style="color: #000;margin-bottom: 0;">User Name: {{ ucfirst($book->user->name) }}</p>
									<p style="color: #000;margin-bottom: 0;">User Email: {{ $book->user->email }}</p>
									<p style="color: #000;margin-bottom: 0;">User Mobile: {{ $book->user->mobile_number }}</p>
									<div class="row">
										<div class="col-md-8 offset-md-2 text-center mt-4">
											<h3>Booking Details</h3>
										</div>
										<div class="col-md-8 offset-md-2">
											<p style="float: left; font-size: 16px; color: #000;">Wedding Car:</p>
											<p style="float: right; font-size: 16px; color: #000;">{{ $book->wedding_car }}</p>
										</div>
										<div class="col-md-8 offset-md-2">
											<p style="float: left; font-size: 16px; color: #000;">Car Decoration:</p>
											<p style="float: right; font-size: 16px; color: #000;">{{ $book->wedding_car_decoration }}</p>
										</div>
										<div class="col-md-8 offset-md-2">
											<p style="float: left; font-size: 16px; color: #000;">Guest Car:</p>
											<p style="float: right; font-size: 16px; color: #000;">{{ $book->guest_car }}</p>
										</div>
										<div class="col-md-8 offset-md-2">
											<p style="float: left; font-size: 16px; color: #000;">Journey Location:</p>
											<p style="float: right; font-size: 16px; color: #000;">{{ $book->journey_location }}</p>
										</div>
										<div class="col-md-8 offset-md-2">
											<p style="float: left; font-size: 16px; color: #000;">Wedding Date:</p>
											<p style="float: right; font-size: 16px; color: #000;">{{ $book->wedding_date }}</p>
										</div>
										<div class="col-md-8 offset-md-2">
											<p style="float: left; font-size: 16px; color: #000;">Center Name:</p>
											<p style="float: right; font-size: 16px; color: #000;">{{ $book->center_name }}</p>
										</div>
										<div class="col-md-8 offset-md-2">
											<p style="float: left; font-size: 16px; color: #000;">Pick-up Time:</p>
											<p style="float: right; font-size: 16px; color: #000;">{{ $book->pick_up_time }}</p>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4 offset-md-8">
										<a href="{{ route('cars.show', $book->car_id) }}" target="_blank" class="btn btn-primary text-center">This Car Was Booked</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-2"></div>
				</div>
			</div>
		</div>
	</div>
	</div>
@endsection
