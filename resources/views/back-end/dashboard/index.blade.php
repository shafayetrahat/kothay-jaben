@extends('back-end.layouts.master')

@section('content')
	
	<div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Dashboard</span>
                <h3 class="page-title">Website Overview</h3>
            </div>
        </div>
        <!-- End Page Header -->
        <!-- Small Stats Blocks -->
		@if(isset($blogPosts) && isset($hotels) && isset($hotelPackages) && isset($users) && isset($tourPackages) && isset($carPackages) && isset($products) && isset($cars))
        <div class="row">
            <div class="col-lg col-md-6 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                    <div class="card-body p-0 d-flex">
                        <div class="d-flex flex-column m-auto">
                            <div class="stats-small__data text-center">
                                <span class="stats-small__label text-uppercase">Blog Posts</span>
                                <h6 class="stats-small__value count my-3">{{ $blogPosts }}</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg col-md-6 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                    <div class="card-body p-0 d-flex">
                        <div class="d-flex flex-column m-auto">
                            <div class="stats-small__data text-center">
                                <span class="stats-small__label text-uppercase">Hotels</span>
                                <h6 class="stats-small__value count my-3">{{ $hotels }}</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg col-md-4 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                    <div class="card-body p-0 d-flex">
                        <div class="d-flex flex-column m-auto">
                            <div class="stats-small__data text-center">
                                <span class="stats-small__label text-uppercase">Hotel Packages</span>
                                <h6 class="stats-small__value count my-3">{{ $hotelPackages }}</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg col-md-4 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                    <div class="card-body p-0 d-flex">
                        <div class="d-flex flex-column m-auto">
                            <div class="stats-small__data text-center">
                                <span class="stats-small__label text-uppercase">Users</span>
                                <h6 class="stats-small__value count my-3">{{ $users }}</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        {{--OverView Second Part--}}

        <div class="row">
			<div class="col-lg col-md-4 col-sm-12 mb-4">
				<div class="stats-small stats-small--1 card card-small">
					<div class="card-body p-0 d-flex">
						<div class="d-flex flex-column m-auto">
							<div class="stats-small__data text-center">
								<span class="stats-small__label text-uppercase">Tour Packages</span>
								<h6 class="stats-small__value count my-3">{{ $tourPackages }}</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
            <div class="col-lg col-md-6 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                    <div class="card-body p-0 d-flex">
                        <div class="d-flex flex-column m-auto">
                            <div class="stats-small__data text-center">
                                <span class="stats-small__label text-uppercase">Car Packages</span>
                                <h6 class="stats-small__value count my-3">{{ $carPackages }}</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg col-md-6 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                    <div class="card-body p-0 d-flex">
                        <div class="d-flex flex-column m-auto">
                            <div class="stats-small__data text-center">
                                <span class="stats-small__label text-uppercase">Products</span>
                                <h6 class="stats-small__value count my-3">{{ $products }}</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg col-md-4 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                    <div class="card-body p-0 d-flex">
                        <div class="d-flex flex-column m-auto">
                            <div class="stats-small__data text-center">
                                <span class="stats-small__label text-uppercase">Cars</span>
                                <h6 class="stats-small__value count my-3">{{ $cars }}</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			{{--<div class="col-lg col-md-4 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                    <div class="card-body p-0 d-flex">
                        <div class="d-flex flex-column m-auto">
                            <div class="stats-small__data text-center">
                                <span class="stats-small__label text-uppercase">Users</span>
                                <h6 class="stats-small__value count my-3">{{ $users }}</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg col-md-4 col-sm-12 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                    <div class="card-body p-0 d-flex">
                        <div class="d-flex flex-column m-auto">
                            <div class="stats-small__data text-center">
                                <span class="stats-small__label text-uppercase">Tour Packages</span>
                                <h6 class="stats-small__value count my-3">{{ $tourPackages }}</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>--}}
        </div>
	<!-- End Small Stats Blocks -->
		@endif
	</div>
@endsection

@section('footer')
	<script src="/js/scripts/app/app-blog-overview.1.0.0.js"></script>
@endsection
