@extends('back-end.layouts.master')

@section('styles')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/quill/1.3.6/quill.snow.css">

	<style>
		.logo {
			margin: 0 0 10px 50px;
		}
		span.d-flex, i.material-icons {
			top: 5px;
		}
		.mlemail {
			margin-left: 36px;
		}
		.mlimage {
			margin-left: 95px;
		}
	</style>
@endsection


@section('content')

	<div class="main-content-container container-fluid px-4">
		<!-- Page Header -->
		<div class="page-header row no-gutters py-4">
			<div class="col-12 col-sm-4 text-center text-sm-left mb-0">
				<span class="text-uppercase page-subtitle">Address</span>
				@if(count($contactInfos) == 0)
					<h3 class="page-title">Add Your Contact Address</h3>
				@else
					<h3 class="page-title">Your Contact Address</h3>
				@endif
			</div>
		</div>
		<!-- End Page Header -->
		<div class="row">
			@if(count($contactInfos) == 0)
			<div class="col-lg-6 col-md-12">
				<!-- Add New Post Form -->
				<div class="card card-small mb-3">
					<div class="card-body">

						@include('back-end.layouts.messages')

						{!! Form::open(['action' => 'BackEndController\HomeController\ContactInfoController@store', 'method' => 'POST', 'files' => true]) !!}

							{{ Form::label('site_logo', 'Site Logo:') }}
							{{ Form::file('site_logo', ['class' => 'form-control form-control-lg mb-3']) }}

							{{ Form::tel('phone_number', '', ['class' => 'form-control form-control-lg mb-3', 'placeholder' => 'Your Contact Number']) }}

							{{ Form::text('address', '', ['class' => 'form-control form-control-lg mb-3', 'placeholder' => 'Your Office Location']) }}

							{{ Form::email('email', '', ['class' => 'form-control form-control-lg mb-3', 'placeholder' => 'Your Email Address']) }}

							{{ Form::submit('Submit', ['class' => 'btn btn-info form-control m-top']) }}
						{!! Form::close() !!}
					</div>
				</div>
			<!-- / Add New Post Form -->
			</div>
			@else
			<div class="col-lg-6 col-md-12">
				<!-- Post Overview -->
				@foreach($contactInfos as $info)
					<div class='card card-small mb-3'>
						<div class="card-header border-bottom">
							<h6 class="m-0">Address</h6>
						</div>
						<div class='card-body p-0'>
							<ul class="list-group list-group-flush">
								<li class="list-group-item p-3">
	                        <span class="d-flex mb-2">
	                          <i class="material-icons ml-2">image</i>
	                          <strong class="ml-2">Logo:</strong> <img class="logo mlimage" src="/photos/logo/{{ $info->site_logo }}">
	                        </span>
									<span class="d-flex mb-2">
	                          <i class="material-icons ml-2">location_on</i>
	                          <strong class="ml-2">Office Location:</strong>
	                          <strong class="text-warning  ml-4">{{ $info->address }}</strong>
	                        </span>
									<span class="d-flex mb-2">
	                          <i class="material-icons ml-2">contact_phone</i>
	                          <strong class="ml-2">Contact Number:</strong> <span class="text-warning ml-3">{{ $info->phone_number }}</span>
	                        </span>
									<span class="d-flex">
	                          <i class="material-icons ml-2">email</i>
	                          <strong class="ml-2">Email Address:</strong>
	                          <strong class="text-warning mlemail">{{ $info->email }}</strong>
	                        </span>
								</li>
								<li class="list-group-item d-flex px-3">
									<a href="/admin/home/info/{{ $info->id }}/edit">
									<button class="btn btn-sm btn-accent ml-auto">
										<i class="material-icons">file_copy</i> Edit
									</button>
									</a>
								</li>
							</ul>
						</div>
					</div>
				@endforeach
				<!-- / Post Overview -->
			</div>
			@endif
		</div>
	</div>
@endsection