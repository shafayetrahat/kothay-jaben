@extends('back-end.layouts.master')

@section('styles')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/quill/1.3.6/quill.snow.css">
@endsection


@section('content')

	<div class="main-content-container container-fluid px-4">
		<!-- Page Header -->
		<div class="page-header row no-gutters py-4">
			<div class="col-12 col-sm-4 text-center text-sm-left mb-0">
				<span class="text-uppercase page-subtitle">Address</span>
				<h3 class="page-title">Edit Your Contact Address</h3>
			</div>
		</div>
		<!-- End Page Header -->
		<div class="row">
			<div class="col-lg-6 col-md-12">
				<!-- Add New Post Form -->
				<div class="card card-small mb-3">
					<div class="card-body">
						@include('back-end.layouts.messages')

						{!! Form::open(['action' => ['BackEndController\HomeController\ContactInfoController@update',$contactInfo->id], 'method' => 'PUT', 'files' => true]) !!}

							{{ Form::label('site_logo', 'Site Logo:') }}
							{{ Form::file('site_logo', ['class' => 'form-control form-control-lg mb-3']) }}

							{{ Form::tel('phone_number', $contactInfo->phone_number, ['class' => 'form-control form-control-lg mb-3']) }}

							{{ Form::text('address', $contactInfo->address, ['class' => 'form-control form-control-lg mb-3']) }}

							{{ Form::email('email', $contactInfo->email, ['class' => 'form-control form-control-lg mb-3']) }}

							{{ Form::submit('Submit', ['class' => 'btn btn-info form-control m-top']) }}
						{!! Form::close() !!}
					</div>
				</div>
				<!-- / Add New Post Form -->
			</div>
		</div>
	</div>
@endsection