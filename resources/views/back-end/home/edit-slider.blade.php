@extends('back-end.layouts.master')

@section('styles')
    <link rel="stylesheet" type="text/css" href="/css/styles/blog_styles.css">
    <link rel="stylesheet" type="text/css" href="/css/styles/blog_responsive.css">
@endsection

@section('content')
    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Slider</span>
                <h3 class="page-title">Edit Slider</h3>
            </div>
        </div>
        <!-- End Page Header -->
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <!-- Add New Post Form -->
                <div class="card card-small mb-3">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8 offset-md-2">
                                @include('back-end.layouts.messages')
                                <form action="/admin/slider/{{ $sliderPhoto->id }}" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    {{ method_field('PATCH') }}
                                    <div class="form-group">
                                        <input type="text" value="{{ $sliderPhoto->slider_text }}" name="slider_text" class="form-control" aria-describedby="emailHelp" placeholder="Slider Text">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" value="{{ $sliderPhoto->package_link }}" name="package_link" class="form-control" aria-describedby="emailHelp" placeholder="Package Link">
                                    </div>
                                    <div class="form-group">
                                        <label for="package_image">Upload Slider Image</label>
                                        <input type="file" name="slider_image" class="form-control" id="package_image" aria-describedby="emailHelp">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection