@extends('back-end.layouts.master')

@section('styles')
    <style>
        .margin-b {
            padding: 35px 20px 5px 20px;
            margin: -30px 0 0 0;
        }
        .no-hotel {
            padding: 80px 0;
        }
    </style>
@endsection

@section('content')

    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Sliders</span>
                <h3 class="page-title">All Sliders</h3>
            </div>
        </div>
    @include('back-end.layouts.messages')
    <!-- End Page Header -->
        <div class="row">
            @if (count($sliderPhotos))

                @foreach ($sliderPhotos as $sliderPhoto)
                    <div class="col-lg-6 col-xl-4 col-md-6 col-xl-4 col-sm-12 mb-4">
                        <div class="card card-small card-post card-post--1">
                            <div class="card-post__image" style="background-image: url({{ asset('/photos/slider/' . $sliderPhoto->slider_image) }});">
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">
                                    <a class="text-fiord-blue" href="#">{{ $sliderPhoto->slider_text }}</a>
                                </h5>
                                {{--<p class="text-muted">at {{ $sliderPhoto->hotel_location }}</p>--}}
                                <p class="text-muted">{{ $sliderPhoto->created_at->toFormattedDateString() }}</p>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 margin-b">
                                    <form method="POST" action="/admin/sliders/{{ $sliderPhoto->id}}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-danger delete-hotel btn-block" value="Delete">
                                        </div>
                                    </form>
                                </div>
                                <div class="col-lg-6 form-group margin-b">
                                    <a href="{{ url('admin/sliders/'. $sliderPhoto->id) .'/edit' }}"><button type="button" class="btn btn-info btn-block">Edit Slider</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

            @else
                <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
                    <div class="card card-small card-post card-post--1 no-hotel">
                        <h2 class="text-center">No Slider Added Yet</h2>
                        <div class="row">
                            <div class="col-md-4 offset-md-4 text-center mt-4">
                                <a href="/admin/home" class="btn btn-success">Add New</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection

@section('footer')
    <script>
        $('.delete-hotel').click(function(e){
            e.preventDefault() // Don't post the form, unless confirmed
            if (confirm('Are you sure, you want to delete?')) {
                // Post the form
                $(e.target).closest('form').submit() // Post the surrounding form
            }
        });
    </script>
@endsection
