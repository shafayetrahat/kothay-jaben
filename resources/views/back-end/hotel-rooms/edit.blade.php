@extends('back-end.layouts.master')

@section('styles')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css" />
	<link rel="stylesheet" type="text/css" href="/css/styles/blog_styles.css">
	<link rel="stylesheet" type="text/css" href="/css/styles/blog_responsive.css">

	<style>
		.btn {
			margin-top: 10px;
		}
	</style>
@endsection

@section('content')
	<div class="main-content-container container-fluid px-4">
		<!-- Page Header -->
		<div class="page-header row no-gutters py-4">
			<div class="col-12 col-sm-4 text-center text-sm-left mb-0">
				<span class="text-uppercase page-subtitle">Edit Hotel Rooms</span>
				<h3 class="page-title">{{ $hotelRoom->hotel->hotel_name }}</h3>
			</div>
		</div>
		<!-- End Page Header -->
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<!-- Add New Post Form -->
				<div class="card card-small mb-3">
					<div class="card-body">
						<div class="row">
							<div class="col-lg-8 offset-md-2">
								<div class="blog_post">
									@include('back-end.layouts.messages')
									{!! Form::open(['action' => ['BackEndController\HotelController\HotelRoomsController@update', $hotelRoom->id ], 'method' => 'PUT', 'files' => true]) !!}
										{{ csrf_field() }}
										{{ method_field('PATCH') }}
										<div class="form-group">
											<input type="text" name="room_title" value="{{ $hotelRoom->room_title }}" class="form-control" id="rental_fee" aria-describedby="emailHelp" placeholder="Room Title">
										</div>
										<div class="form-group">
											<input type="number" name="room_rent" value="{{ $hotelRoom->room_rent }}" class="form-control" id="name" aria-describedby="emailHelp" placeholder="Rent For Per Night">
										</div>
										<div class="form-group">
											<input type="text" name="room_capacity" value="{{ $hotelRoom->room_capacity }}" class="form-control" id="rental_fee" aria-describedby="emailHelp" placeholder="Room Capacity">
										</div>
										<div class="form-group">
											<label for="room_image">Upload Feature Image</label>
											<input type="file" name="room_image" class="form-control" id="room_image" aria-describedby="emailHelp">
										</div>
										<textarea id="article-ckeditor" name="room_details" class="form-control" placeholder="Add details about car, it would best if you write in list item">{{ $hotelRoom->room_details }}</textarea>
										<button type="submit" class="btn btn-info">Submit</button>
									{!! Form::close() !!}


								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('footer')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>
	<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
	<script>
		Dropzone.options.photosForm = {
			paramName: 'photo',
			acceptedFiles: '.jpg, .jpeg, .png, .bmp, .svg',
		}

		CKEDITOR.replace( 'article-ckeditor' );
		CKEDITOR.config.entities = false;
		CKEDITOR.config.disableNativeSpellChecker = false;
	</script>
@endsection