@extends('back-end.layouts.master')

@section('styles')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css" />
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
	<link rel="stylesheet" type="text/css" href="/css/styles/blog_styles.css">
	<link rel="stylesheet" type="text/css" href="/css/styles/blog_responsive.css">

	<style>
		h4 {
			margin: 45px 0 45px 0;
		}
		.blog_post_text p {
			color: black;
		}

		.blog_post h1, p {
			color:black;
		}

		.blog_post_text ul {
			margin-left: 30px;
			color: #3e3e3e;
			list-style: disc;
		}

		.blog_post_text ol {
			margin-left: 30px;
			color: #3e3e3e;
		}
		.delete-rooms {
			color: red;
			position: relative;
			left: 90%;
			font-size: 40px;
			background: white;
			border: none;
			cursor: pointer;
		}
		.bx-wrapper .bx-controls-direction a {
			z-index: 1;
		}
		/*.bx-controls-direction, a.bx-next, a.bx-prev {
			bottom: 50% !important;
		}*/
		@media screen and (max-width: 680px) {
			.delete-rooms {
				left: 80%;
			}
		}
	</style>
@endsection

@section('content')
	<div class="main-content-container container-fluid px-4">
		<!-- Page Header -->
		<div class="page-header row no-gutters py-4">
			<div class="col-12 col-sm-4 text-center text-sm-left mb-0">
				<span class="text-uppercase page-subtitle">{{ $hotelRoom->hotel->hotel_name }}</span>
				<h3 class="page-title">Room Details</h3>
			</div>
		</div>
		<!-- End Page Header -->
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<!-- Add New Post Form -->
				<div class="card card-small mb-3">
					<div class="card-body">
						<div class="row">
							<div class="col-lg-2"></div>
							<div class="col-lg-8">
								<div class="blog_post">
									<div class="blog_post_image">
										<a href="/admin/hotels/{{ $hotelRoom->id }}/hotel-rooms/edit" class="float-right btn btn-success" style="margin-bottom: 20px;">Edit</a>
										<img src="{{ asset('/photos/hotel-rooms/' . $hotelRoom->room_image) }}" alt="{{ $hotelRoom->room_title }}">
									</div>
									<h1 class="text-center">{{ $hotelRoom->room_title }}</h1><span> (Room id: {{ $hotelRoom->id }})</span>
									<p class="text-center ">Fee per night: ৳ {{ number_format($hotelRoom->room_rent) }}</p>
									<div class="blog_post_text">
										<p>{!! $hotelRoom->room_details !!}</p>
									</div>
									@if (count($hotelRoom->roomPhotos))
										<div class="blog_post_image">
											<h4 class="text-center">Your uploaded room photos</h4>
											<div class="row">
												<div class="col-md-10 offset-md-1">
													<ul class="slider">
														@foreach($hotelRoom->roomPhotos as $photo)
															<li>
																<form method="POST" action="/admin/hotels/{{ $photo->id }}/room-photo">
																	{!! csrf_field() !!}
																	<input type="hidden" name="_method" value="DELETE">
																	<button class="delete-rooms" type="submit"><i class="far fa-times-circle"></i></button>
																</form>
																<img src="/{{ $photo->photos }}">

															</li>
														@endforeach
													</ul>
												</div>
											</div>
										</div>
									@endif

									<h3 style="margin:40px 0 40px 0" class="text-center">Upload hotel room images</h3>

									<form action="/admin/hotels/{{ $hotelRoom->id }}/hotel-room/addPhoto" method="POST" class="dropzone" id="photosForm" style="margin-bottom: 30px;">
										{{ csrf_field() }}
									</form>
								</div>
							</div>
							<div class="col-lg-2"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('footer')
	<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>
	<script>
		Dropzone.options.photosForm = {
			paramName: 'photo',
			acceptedFiles: '.jpg, .jpeg, .png, .bmp, .svg',
		}

		$(document).ready(function(){
			$('.slider').bxSlider({
				slideWidth: 1000
			});
		});

		$('.delete-rooms').click(function(e){
			e.preventDefault() // Don't post the form, unless confirmed
			if (confirm('Are you sure, you want to delete?')) {
				// Post the form
				$(e.target).closest('form').submit() // Post the surrounding form
			}
		});
	</script>
@endsection