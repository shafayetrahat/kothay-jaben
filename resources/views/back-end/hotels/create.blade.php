@extends('back-end.layouts.master')

@section('styles')
	<link rel="stylesheet" type="text/css" href="/css/styles/blog_styles.css">
	<link rel="stylesheet" type="text/css" href="/css/styles/blog_responsive.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css" />

	<style>
		.btn {
			margin-top: 20px;
		}
	</style>
@endsection

@section('content')
	<div class="main-content-container container-fluid px-4">
		<!-- Page Header -->
		<div class="page-header row no-gutters py-4">
			<div class="col-12 col-sm-4 text-center text-sm-left mb-0">
				<span class="text-uppercase page-subtitle">Hotel</span>
				<h3 class="page-title">Add New Hotel</h3>
			</div>
		</div>
		<!-- End Page Header -->
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<!-- Add New Post Form -->
				<div class="card card-small mb-3">
					<div class="card-body">
						<div class="row">
							<div class="col-lg-8 offset-md-2">
								<div class="blog_post">
									@include('back-end.layouts.messages')
									<form action="{{ route('hotels.store') }}" method="POST" enctype="multipart/form-data">
										{{ csrf_field() }}
										<div class="form-group">
											<input type="text" value="{{ old('hotel_name') }}" name="hotel_name" class="form-control" id="name" aria-describedby="emailHelp" placeholder="Enter Hotel Name">
										</div>
										<div class="form-group">
											<input type="text" value="{{ old('hotel_location') }}" name="hotel_location" class="form-control" id="rental_fee" aria-describedby="emailHelp" placeholder="Enter Hotel Location">
										</div>
										<div class="form-group">
											<label for="car_image">Upload Feature Image</label>
											<input type="file" name="hotel_image" class="form-control" id="car_image" aria-describedby="emailHelp">
										</div>
										<textarea id="article-ckeditor" name="hotel_details" class="form-control" placeholder="Add details about car, it would best if you write in list item">{{ old('hotel_details') }}</textarea>
										<button type="submit" class="btn btn-info">Submit</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('footer')

	<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
	<script>
		CKEDITOR.replace( 'article-ckeditor' );
		CKEDITOR.config.entities = false;
		CKEDITOR.config.disableNativeSpellChecker = false;
	</script>
@endsection