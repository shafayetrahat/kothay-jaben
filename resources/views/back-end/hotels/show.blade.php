@extends('back-end.layouts.master')

@section('styles')
	<link rel="stylesheet" type="text/css" href="/css/styles/blog_styles.css">
	<link rel="stylesheet" type="text/css" href="/css/styles/blog_responsive.css">

	<style>
		h4 {
			margin: 45px 0 45px 0;
		}
		.blog_post_text p {
			color: black;
		}

		.blog_post h1, p {
			color:black;
		}
		@media only screen and (max-width: 765px) {
			.media-img {
				text-align: center!important;
			}
			.mb-2 {
				margin-bottom: -20px;
			}
		}
	</style>
@endsection

@section('content')
	<div class="main-content-container container-fluid px-4">
		<!-- Page Header -->
		<div class="page-header row no-gutters py-4">
			<div class="col-12 col-sm-4 text-center text-sm-left mb-0">
				<span class="text-uppercase page-subtitle">Hotel</span>
				<h3 class="page-title">Hotel Details</h3>
			</div>
		</div>
	@include('back-end.layouts.messages')
	<!-- End Page Header -->
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<!-- Add New Post Form -->
				<div class="card card-small mb-3">
					<div class="card-body">
						<div class="row">
							<div class="col-lg-10 offset-md-1">
								<div class="blog_post">
									<div class="blog_post_image">
										<a href="/admin/hotels/{{ $hotel->id }}/hotel-room/create" class="btn btn-success" style="margin-bottom: 20px;">Add Hotel Rooms</a>
										<a href="/admin/hotels/{{ $hotel->id }}/edit" class="float-right btn btn-success" style="margin-bottom: 20px;">Edit</a>
										<img src="{{ asset('/photos/hotels/' . $hotel->hotel_image) }}" alt="{{ $hotel->hotel_name }}">
									</div>
									<h1 class="text-center">{{ $hotel->hotel_name }}</h1>
									<p class="text-center">{{ $hotel->hotel_location }}</p>

									<p>{!! $hotel->hotel_details !!}</p>
								</div>

								<h3>Rooms of {{ $hotel->hotel_name }}</h3>
								<hr>
								@if(count($hotel->hotelRooms))
								@foreach($hotel->hotelRooms as $room)

									<div class="row">
										<div class="col-md-5 media-img">
											<img class="" src="{{ '/photos/hotel-rooms/' . $room->room_image}}" alt="{{ $room->room_title }}" width="220px" height="158px">
										</div>
										<div class="col-md-4 text-center">
											<h3>Capacity</h3>
											<p>{{ $room->room_capacity }}</p>
										</div>
										<div class="col-md-3 text-center">
											<p>৳ {{ number_format($room->room_rent) }}</p>
											<a href="/admin/hotels/{{ $room->id }}/hotel-rooms" class="btn btn-info mb-2">See Details</a>
											<form method="POST" action="/admin/hotels/{{ $room->id }}/delete-room">
												{!! csrf_field() !!}
												<input type="hidden" name="_method" value="DELETE">
												<button class="btn btn-danger delete-car">Delete Room</button>
											</form>
										</div>
									</div>
									<hr>
								@endforeach
								@else
									<div class="col-lg-12 col-md-12 col-sm-12 mb-4">
										<div class="card card-small card-post card-post--1 no-hotel">
											<h2 class="text-center mt-5">No Room Added Yet</h2>
											<div class="row">
												<div class="col-md-4 offset-md-4 text-center mt-4 mb-5">
													<a href="/admin/hotels/{{ $hotel->id }}/hotel-room/create" class="btn btn-success">Add New</a>
												</div>
											</div>
										</div>
									</div>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('footer')
	<script>
		$('.delete-car').click(function(e){
			e.preventDefault() // Don't post the form, unless confirmed
			if (confirm('Are you sure, you want to delete?')) {
				// Post the form
				$(e.target).closest('form').submit() // Post the surrounding form
			}
		});
	</script>
@endsection
