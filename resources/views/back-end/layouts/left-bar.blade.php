<!-- Main Sidebar -->
<aside class="main-sidebar col-12 col-md-3 col-lg-2 px-0">
	<div class="main-navbar">
		<nav class="navbar align-items-stretch navbar-light bg-white flex-md-nowrap border-bottom p-0">
			<a class="navbar-brand w-100 mr-0" href="/admin/dashboard" style="line-height: 25px;">
				<div class="d-table m-auto">
					<img id="main-logo" class="d-inline-block align-top mr-1" style="max-width: 25px;"
					     src="/images/shards-dashboards-logo.svg" alt="Shards Dashboard">
					<span class="d-none d-md-inline ml-1">KothayJaben</span>
				</div>
			</a>
			<a class="toggle-sidebar d-sm-inline d-md-none d-lg-none">
				<i class="material-icons">&#xE5C4;</i>
			</a>
		</nav>
	</div>
	<form action="#" class="main-sidebar__search w-100 border-right d-sm-flex d-md-none d-lg-none">
		<div class="input-group input-group-seamless ml-3">
			<div class="input-group-prepend">
				<div class="input-group-text">
					<i class="fas fa-search"></i>
				</div>
			</div>
			<input class="navbar-search form-control" type="text" placeholder="Search for something..."
			       aria-label="Search"></div>
	</form>
	<div class="nav-wrapper">
		<ul class="nav flex-column">
			<li class="nav-item">
				<a class="nav-link " href="/admin/dashboard">
					<i class="material-icons">edit</i>
					<span>Dashboard</span>
				</a>
			</li>
			<li class="nav-item dropdown">
				<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true"
				   aria-expanded="false"> <i class="material-icons">vertical_split</i>Home</a>
				<ul class="dropdown-menu">
					<li class="margin-nav"><a class="nav-link" href="/admin/all-sliders">All Sliders</a></li>
					<li class="margin-nav"><a class="nav-link" href="/admin/home">Add Slider</a></li>
					<li class="margin-nav"><a class="nav-link" href="/admin/home/contact">Add Contact Info</a></li>
				</ul>
			</li>
			<li class="nav-item dropdown">
				<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true"
				   aria-expanded="false"> <i class="material-icons">notes</i>Blog</a>
				<ul class="dropdown-menu">
					<li class="margin-nav"><a class="nav-link" href="/admin/blog">Posts</a></li>
					<li class="margin-nav"><a class="nav-link" href="/admin/blog/create">Add New</a></li>
				</ul>
				</a>
			</li>

			<li class="nav-item dropdown">
				<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true"
				   aria-expanded="false"> <i class="material-icons">local_taxi</i>Cars</a>
				<ul class="dropdown-menu">
					<li class="margin-nav"><a class="nav-link" href="/admin/cars">All Cars</a></li>
					<li class="margin-nav"><a class="nav-link" href="/admin/cars/create">Add New</a></li>
				</ul>
				</a>
			</li>

			<li class="nav-item dropdown">
				<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true"
				   aria-expanded="false"> <i class="material-icons">beach_access</i>Tourist Trip</a>
				<ul class="dropdown-menu">
					<li class="margin-nav"><a class="nav-link" href="/admin/car/tourist-trip">Add Details</a></li>
					<li class="margin-nav"><a class="nav-link" href="/admin/car/tourist-trip-location">Locations</a></li>
				</ul>
				</a>
			</li>

			<li class="nav-item dropdown">
				<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true"
				   aria-expanded="false"> <i class="material-icons">control_camera</i>Body Contract</a>
				<ul class="dropdown-menu">
					<li class="margin-nav"><a class="nav-link" href="/admin/car/body-contract">Add Details</a></li>
					<li class="margin-nav"><a class="nav-link" href="/admin/car/body-contract-location">Location</a></li>
				</ul>
				</a>
			</li>

			<li class="nav-item dropdown">
				<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true"
				   aria-expanded="false"> <i class="material-icons">local_taxi</i>Car Booking</a>
				<ul class="dropdown-menu">
					<li class="margin-nav"><a class="nav-link" href="/admin/car/today-booking/list">Today Booking</a></li>
					<li class="margin-nav"><a class="nav-link" href="/admin/car/local-booking/list">Local Booking</a></li>
					<li class="margin-nav"><a class="nav-link" href="/admin/car/tourist-booking/list">Tourist Booking</a></li>
					<li class="margin-nav"><a class="nav-link" href="/admin/car/hourly-booking/list">Hourly Booking</a></li>
					<li class="margin-nav"><a class="nav-link" href="/admin/car/body-contract-booking/list">Body Booking</a></li>
					<li class="margin-nav"><a class="nav-link" href="/admin/car/wedding-booking/list">Wedding Booking</a></li>
				</ul>
				</a>
			</li>

            <li class="nav-item dropdown">
                <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false"> <i class="material-icons">location_on</i>Locations</a>
                <ul class="dropdown-menu">
                    <li class="margin-nav"><a class="nav-link" href="/admin/pick-up/location">Pick Up</a></li>
                    <li class="margin-nav"><a class="nav-link" href="/admin/drop-off/location">Drop Off</a></li>
                </ul>
                </a>
            </li>

			<li class="nav-item dropdown">
				<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true"
				   aria-expanded="false"> <i class="material-icons">location_city</i>Hotels</a>
				<ul class="dropdown-menu">
					<li class="margin-nav"><a class="nav-link" href="/admin/hotels">All Hotels</a></li>
					<li class="margin-nav"><a class="nav-link" href="/admin/hotels/create">Add New</a></li>
					<li class="margin-nav"><a class="nav-link" href="/admin/hotel/booking/list">Orders</a></li>
				</ul>
				</a>
			</li>

            <li class="nav-item dropdown">
                <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false"> <i class="fas fa-box"></i>Car Package</a>
                <ul class="dropdown-menu">
                    <li class="margin-nav"><a class="nav-link" href="/admin/packages-car">All Car Package</a></li>
					<li class="margin-nav"><a class="nav-link" href="/admin/packages-car/create">Add New</a></li>
					<li class="margin-nav"><a class="nav-link" href="/admin/car/package-booking/list">Orders</a></li>
                </ul>
                </a>
            </li>

            <li class="nav-item dropdown">
                <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false"> <i class="fas fa-box"></i>Tour Package</a>
                <ul class="dropdown-menu">
                    <li class="margin-nav"><a class="nav-link" href="/admin/packages">All Tour Package</a></li>
					<li class="margin-nav"><a class="nav-link" href="/admin/packages/create">Add New</a></li>
					<li class="margin-nav"><a class="nav-link" href="/admin/tour/package-booking/list">Oders</a></li>
                </ul>
                </a>
            </li>

            <li class="nav-item dropdown">
                <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false"> <i class="fas fa-box"></i>Hotle Package</a>
                <ul class="dropdown-menu">
                    <li class="margin-nav"><a class="nav-link" href="/admin/packages-hotels">All Hotel Package</a></li>
					<li class="margin-nav"><a class="nav-link" href="/admin/packages-hotels/create">Add New</a></li>
					<li class="margin-nav"><a class="nav-link" href="/admin/hotel/package-booking/list">Orders</a></li>
                </ul>
                </a>
            </li>

			<li class="nav-item dropdown">
				<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" aria-haspopup="true"
				   aria-expanded="false"> <i class="material-icons">add_shopping_cart</i>Products</a>
				<ul class="dropdown-menu">
					<li class="margin-nav"><a class="nav-link" href="/admin/products">All Products</a></li>
					<li class="margin-nav"><a class="nav-link" href="/admin/products/create">Add New</a></li>
					<li class="margin-nav"><a class="nav-link" href="/admin/car/booking/list">Orders</a></li>
				</ul>
				</a>
			</li>
		</ul>
	</div>
</aside>
<!-- End Main Sidebar -->
