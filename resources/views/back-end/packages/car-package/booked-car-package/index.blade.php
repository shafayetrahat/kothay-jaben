@extends('back-end.layouts.master')

@section('styles')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <style>
        .margin-b {
            padding: 20px;
        }
        .no-post {
            padding: 80px 0;
        }
        .botM {
            margin-bottom: 2px;
        }
		.gray_worm {
			background-color: #868e96;
		}
    </style>
    <?php
/*    if (count($hpBookings)) {
        foreach ($hpBookings as $cpBook) {
            $start = \Carbon\Carbon::parse( $cpBook->check_in);
            $end = \Carbon\Carbon::parse( $cpBook->check_out);
        }

        $days = $end->diffInDays($start);
    }
    */?>
@endsection

@section('content')

    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Car Package</span>
                <h3 class="page-title">Booked Car Package</h3>
            </div>
        </div>
    @include('back-end.layouts.messages')
    <!-- End Page Header -->
        <div class="row">
            @if (count($cpBookings))
                @foreach ($cpBookings as $cpBook)
                    <div class="col-lg-4 col-md-6 col-xl-4 col-sm-12 mb-4">
                        <div class="card card-small card-post card-post--1 {{ $cpBook->confirmed == 1 ? 'gray_worm' : ''}}">
                            <a href="#" class="card-post__category badge badge-pill badge-primary">Car: {{ $cpBook->car_name }}</a>
                            <div class="card-body">
                                <h5 class="card-title">
                                    <a class="text-fiord-blue" href="/admin/car/package-booking/{{ $cpBook->id }}">{{ $cpBook->car_name }}</a>
                                </h5>
                                <a href="/admin/car/package-booking/{{ $cpBook->id }}"><p class="botM">Hotel bill: <span style="font-size: 20px;">৳</span> {{ number_format($cpBook->car_rent) }}</p></a>
                                <a href="/admin/car/package-booking/{{ $cpBook->id }}"><p class="botM">Booked By: {{ ucfirst($cpBook->user->name) }}</p></a>
                                <a href="/admin/car/package-booking/{{ $cpBook->id }}"><p class="botM">Email: {{ $cpBook->user->email }}</p></a>
                                <a href="/admin/car/package-booking/{{ $cpBook->id }}"><p class="botM">Phone: {{ $cpBook->user->mobile_number }}</p></a>
                                <a href="/admin/car/package-booking/{{ $cpBook->id }}"><p class="botM">Hotel booking request sent on {{ $cpBook->created_at->toFormattedDateString() }} (at {{ $cpBook->created_at->format('g:i:s a') }})</p></a>
                            </div>
                            <div class="row">
								<div class="col-md-2"></div>
								<div class="col-md-4 col-sm-3 col-xs-3">
									<form method="POST" action="/admin/car/package-booking/{{ $cpBook->id}}">
										{{ csrf_field() }}
										{{ method_field('DELETE') }}

										<div class="form-group">
											<input type="submit" class="btn btn-danger btn-block delete-booking" value="Delete">
										</div>
									</form>
								</div>
								@if($cpBook->confirmed == 0)
									<div class="col-md-5 col-sm-3 col-xs-3">
										<form method="POST" action="/admin/car/package-booking/{{ $cpBook->id}}">
											{{ csrf_field() }}
											{{ method_field('PATCH') }}

											<div class="form-group">
												<input type="submit" class="btn btn-success btn-block" value="Confirmed">
											</div>
										</form>
									</div>
								@else
									<div class="col-md-5 col-sm-3 col-xs-3">
										<form method="POST" action="/admin/car/package-booking/unconfirmed/{{ $cpBook->id}}">
											{{ csrf_field() }}
											{{ method_field('PATCH') }}

											<div class="form-group">
												<input type="submit" class="btn btn-success btn-block" value="Unconfirmed">
											</div>
										</form>
									</div>
								@endif
                            </div>
                        </div>
                    </div>
                @endforeach

            @else
                <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
                    <div class="card card-small card-post card-post--1 no-post">
                        <h2 class="text-center">No Car Booked Yet</h2>
                    </div>
                </div>
            @endif
        </div>
        <p class="text-center">{{ $cpBookings->links() }}</p>
    </div>
@endsection

@section('footer')
    <script>
        $('.delete-booked-hotel').click(function(e){
            e.preventDefault() // Don't post the form, unless confirmed
            if (confirm('Are you sure, you want to delete?')) {
                // Post the form
                $(e.target).closest('form').submit() // Post the surrounding form
            }
        });
    </script>
@endsection
