@extends('back-end.layouts.master')

@section('styles')
	<link rel="stylesheet" type="text/css" href="/css/styles/blog_styles.css">
	<link rel="stylesheet" type="text/css" href="/css/styles/blog_responsive.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css" />

	<style>
		.btn {
			margin-top: 10px;
		}
	</style>
@endsection

@section('content')
	<div class="main-content-container container-fluid px-4">
		<!-- Page Header -->
		<div class="page-header row no-gutters py-4">
			<div class="col-12 col-sm-4 text-center text-sm-left mb-0">
				<span class="text-uppercase page-subtitle">Products</span>
				<h3 class="page-title">Add New Product</h3>
			</div>
		</div>
		<!-- End Page Header -->
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<!-- Add New Post Form -->
				<div class="card card-small mb-3">
					<div class="card-body">
						<div class="row">
							<div class="col-lg-2"></div>
							<div class="col-lg-8">
								<div class="blog_post">
									@include('back-end.layouts.messages')
									<form action="{{ route('products.store') }}" method="POST" enctype="multipart/form-data">
										{{ csrf_field() }}
										<div class="form-group">
											<input type="text" value="{{ old('product_name') }}" name="product_name" class="form-control" id="name" aria-describedby="product_name" placeholder="Enter Product Name">
										</div>
										<div class="form-group">
											<input type="number" value="{{ old('product_price') }}" name="product_price" class="form-control" id="rental_fee" aria-describedby="product_price" placeholder="Enter Product Price">
										</div>
										<div class="form-group">
											<input type="text" value="{{ old('product_quantity') }}" name="product_quantity" class="form-control" id="rental_fee" aria-describedby="product_quantity" placeholder="Enter Product Quantity">
										</div>
										<div class="form-group">
											<label for="product_image">Upload Feature Image</label>
											<input type="file" name="product_image" class="form-control" id="product_image" aria-describedby="product_imageHelp">
										</div>
										<div class="form-group">
											<textarea class="form-control" name="product_details" rows="5" placeholder="Enter Product Details">{{ old('product_details') }}</textarea>
										</div>
										<button type="submit" class="btn btn-info">Submit</button>
									</form>
								</div>
							</div>
							<div class="col-lg-2"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection