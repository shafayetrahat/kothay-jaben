@extends('back-end.layouts.master')

@section('styles')
	<style>
		.margin-b {
			padding: 35px 20px 5px 20px;
			margin: -30px 0 0 0;
		}
		.price {
			margin-top: 30px;
		}
		.card-title {
			margin-bottom: -28px;
		}
		.no-product {
			padding: 80px 0;
		}
		button.button_modal {
			position: absolute;
			left: 34%;
			top: 50%;
			display: none;
			transition: all .5s;
		}
		.card-post__image:hover .card-post__category {
			display: none;
		}
		.card-post__image:hover .button_modal {
			display: block;
		}
	</style>
@endsection

@section('content')

	<div class="main-content-container container-fluid px-4">
		<!-- Page Header -->
		<div class="page-header row no-gutters py-4">
			<div class="col-12 col-sm-4 text-center text-sm-left mb-0">
				<span class="text-uppercase page-subtitle">Products</span>
				<h3 class="page-title">All Products</h3>
			</div>
		</div>
		@include('back-end.layouts.messages')
		<!-- End Page Header -->
		<div class="row">
			@if (count($products))

				@foreach ($products as $product)
					<div class="col-lg-3 col-md-3 col-sm-6 mb-4">
						<div class="card card-small card-post card-post--1">
							<div class="card-post__image" style="background-image: url({{ asset('/photos/products/' . $product->product_image) }});">
								<a href="#" class="card-post__category badge badge-pill badge-primary">Quantity: {{ $product->product_quantity }}</a>
								<a href="#" class="card-post__category badge badge-pill badge-primary price">Price: ৳ {{ number_format($product->product_price) }}</a>
								<button type="button" class="btn btn-primary button_modal" data-toggle="modal" data-target="#exampleModalCenter">
									Details
								</button>
								<!-- Modal -->
								<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
									<div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLongTitle">{{ $product->product_name }}</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												{!! $product->product_details !!}
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="card-body">
								<h5 class="card-title">
									{{ $product->product_name }}
								</h5>
								{{--<p class="card-text d-inline-block mb-3"><a class="text-fiord-blue" href="/admin/cars/{{ $product->id }}">{!! substr(strip_tags($product->car_details), 0, 50) !!}{!! strlen(strip_tags($product->car_details)) > 50 ? "..." : "" !!}</a></p>
								<p class="text-muted">{{ $product->created_at->toFormattedDateString() }}</p>--}}
							</div>
							<div class="row">
								<div class="col-lg-6 margin-b">
									<form method="POST" action="/admin/products/{{ $product->id}}">
										{{ csrf_field() }}
										{{ method_field('DELETE') }}

										<div class="form-group">
											<input type="submit" class="btn btn-danger delete-product btn-block" value="Delete">
										</div>
									</form>
								</div>
								<div class="col-lg-6 form-group margin-b">
									<a href="{{ url('admin/products/'. $product->id) .'/edit' }}"><button type="button" class="btn btn-info btn-block">Edit</button></a>
								</div>
							</div>
						</div>
					</div>
				@endforeach

			@else
				<div class="col-lg-12 col-md-12 col-sm-12 mb-4">
					<div class="card card-small card-post card-post--1 no-product">
						<h2 class="text-center">No Products Added Yet</h2>
						<div class="row">
							<div class="col-md-4 offset-md-4 text-center mt-4">
								<a href="/admin/products/create" class="btn btn-success">Add New</a>
							</div>
						</div>
					</div>
				</div>
			@endif
		</div>
	</div>
@endsection

@section('footer')
	<script>
		$('.delete-product').click(function(e){
			e.preventDefault() // Don't post the form, unless confirmed
			if (confirm('Are you sure, you want to delete?')) {
				// Post the form
				$(e.target).closest('form').submit() // Post the surrounding form
			}
		});
	</script>
@endsection