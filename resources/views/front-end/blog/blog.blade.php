@extends ('front-end.layouts.master')

@section ('styles')
	<link href="/plugins/colorbox/colorbox.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="/css/styles/blog_styles.css">
	<link rel="stylesheet" type="text/css" href="/css/styles/blog_responsive.css">
@endsection

@section ('content')
	<!-- Home -->

	<div class="home">
		<div class="home_background parallax-window" data-parallax="scroll" data-image-src="/images/blog_background.jpg"></div>
		<div class="home_content">
			<div class="home_title">the blog</div>
		</div>
	</div>

	<!-- Blog -->

	<div class="blog">
		<div class="container">
			<div class="row">

				<!-- Blog Content -->

				<div class="col-lg-8">
					<div class="blog_post_container">
						<!-- Blog Post -->
						@foreach ($blogPosts as $post)
							<div class="blog_post">
								<div class="blog_post_image">
									<img src="{{ asset('/photos/blog/' . $post->featured_image) }}" alt="https://unsplash.com/@anniespratt">
									<div class="blog_post_date d-flex flex-column align-items-center justify-content-center">
										{{-- <div class="blog_post_day">{{ $post->created_at->toFormattedDateString() }}</div> --}}
										<div class="blog_post_month">{{ $post->created_at->toFormattedDateString() }}</div>
									</div>
								</div>
								<div class="blog_post_meta">
									<ul>
										<li class="blog_post_meta_item"><a href="#">by {{ $post->author }}</a></li>
									</ul>
								</div>
								<div class="blog_post_title"><a href="/blog-post/{{ $post->id }}">{{ $post->title }}</a></div>
								<div class="blog_post_text">
									{{ substr(strip_tags($post->blog_post), 0, 300) }}{{ strlen(strip_tags($post->blog_post)) > 300 ? "..." : "" }}
								</div>
								<div class="blog_post_link"><a href="/blog-post/{{ $post->id }}">read more</a></div>
							</div>
						@endforeach
					</div>
					
					<div class="blog_navigation">
						{!! $blogPosts->links() !!}
					</div>
				</div>

				<!-- Blog Sidebar -->

				<div class="col-lg-4 sidebar_col">
					
					<!-- Sidebar Archives -->
					<div class="sidebar_archives">
						<div class="sidebar_title">Archives</div>
						<div class="sidebar_list">
							<ul>
								@foreach ($archives as $stats)
									<li>
										<a href='{{ route('blog-post.index') }}/?month={{ $stats['month'] }}&year={{ $stats['year'] }}'>
											{{ $stats['month'] . ' ' . $stats['year'] }}
										</a>										
									</li>
								@endforeach
							</ul>
						</div>
					</div>

					<!-- Sidebar Latest Posts -->

					<div class="sidebar_latest_posts">
						<div class="sidebar_title">Latest Posts</div>
						<div class="latest_posts_container">
							<ul>

								<!-- Latest Post -->
								@foreach ($blogPosts as $post)
									<li class="latest_post clearfix">
										<div class="latest_post_image">
											<a href="/blog-post/{{ $post->id }}"><img style="width: 73px; height: 73px;" src="/photos/blog/{{ $post->featured_image }}" alt=""></a>
										</div>
										<div class="latest_post_content">
											<div class="latest_post_title trans_200"><a href="/blog-post/{{ $post->id }}">{{ $post->title }}</a></div>
											<div class="latest_post_meta">
												<div class="latest_post_author trans_200"><a href="/blog-post/{{ $post->id }}">{{ $post->author }}</a></div>
												<div class="latest_post_date trans_200"><a href="/blog-post/{{ $post->id }}">{{ $post->created_at->toFormattedDateString() }}</a></div>
											</div>
										</div>
									</li>
								@endforeach
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
	
@section ('footer')
	<script src="/js/jquery-3.2.1.min.js"></script>
	<script src="/css/styles/bootstrap4/popper.js"></script>
	<script src="/css/styles/bootstrap4/bootstrap.min.js"></script>
	<script src="/plugins/colorbox/jquery.colorbox-min.js"></script>
	<script src="/plugins/parallax-js-master/parallax.min.js"></script>
	<script src="/js/blog_custom.js"></script>
@endsection
