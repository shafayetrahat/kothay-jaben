@extends ('front-end.layouts.master')

@section ('styles')
	<link href="/plugins/colorbox/colorbox.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="/css/styles/blog_styles.css">
    <link rel="stylesheet" type="text/css" href="/css/styles/blog_responsive.css">
    <style>
        .blog_post_text p {
            color: black;
        }

        .blog_post h1 {
            color:black;
        }

        .blog_post_text ul {
            margin-left: 30px;
            color: #3e3e3e;
            list-style: disc;
        }

        .blog_post_text ol {
            margin-left: 30px;
            color: #3e3e3e;
        }
    </style>
@endsection

@section ('content')
    <!-- Home -->
    <div class="home">
        <div class="home_background parallax-window" data-parallax="scroll" data-image-src="/images/blog_background.jpg"></div>
        <div class="home_content">
            <div class="home_title">the blog</div>
        </div>
    </div>
    
    <!-- Blog -->
    <div class="blog">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">

                </div>
                <div class="col-lg-8">
                    <div class="blog_post_container">
                        <!-- Blog Post -->
                            <div class="blog_post">
                                <div class="blog_post_image">
                                    <img src="{{ asset('/photos/blog/' . $blogPost->featured_image) }}" alt="https://unsplash.com/@anniespratt">
                                    <div class="blog_post_date d-flex flex-column align-items-center justify-content-center">
                                        {{-- <div class="blog_post_day">{{ $post->created_at->toFormattedDateString() }}</div> --}}
                                        <div class="blog_post_month">{{ $blogPost->created_at->toFormattedDateString() }}</div>
                                    </div>
                                </div>
                                <div class="blog_post_meta">
                                    <ul>
                                        <li class="blog_post_meta_item"><a href="#">by {{ $blogPost->author }}</a></li>
                                    </ul>
                                </div>
                                <h1 class="text-center">{{ $blogPost->title }}</h1>
                                <div class="blog_post_text">
                                    <p>{!! $blogPost->blog_post !!}</p>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    
                </div>
            </div>
        </div>
    </div>
@endsection

@section ('footer')
	<script src="/js/jquery-3.2.1.min.js"></script>
	<script src="/css/styles/bootstrap4/popper.js"></script>
	<script src="/css/styles/bootstrap4/bootstrap.min.js"></script>
	<script src="/plugins/colorbox/jquery.colorbox-min.js"></script>
	<script src="/plugins/parallax-js-master/parallax.min.js"></script>
	<script src="/js/blog_custom.js"></script>
@endsection
