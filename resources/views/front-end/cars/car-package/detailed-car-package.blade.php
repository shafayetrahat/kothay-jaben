@extends ('front-end.layouts.master')

@section ('styles')
    <script src="/js/jquery-3.2.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
    <link href="/plugins/colorbox/colorbox.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="/css/styles/blog_styles.css">
    <link rel="stylesheet" type="text/css" href="/css/styles/blog_responsive.css">

    <style>
        .home_background {
            background: rgba(0, 0, 0, 0.5);
        }
        .home_title {
            font-size: 59px;
        }
        .blog {
            padding-top: 0;
        }
        h1, h2, h3, h4, h5 {
            color: #000;
        }
        .blog_post_text p {
            color: #000;
        }
        hr {
            color: #31124b;
            margin-top: 2rem;
        }
        .blog_post_text ul {
            margin-left: 30px;
            color: #000;
            list-style: disc;
        }

        .blog_post_text ol {
            margin-left: 30px;
            color: #3e3e3e;
        }
        .card-text, .col-md-6 {
            color: #000;
        }
        .card-header {
            background: linear-gradient(to right, #fa9e1b, #8d4fff);
        }

        .car-fa-icon {
            color: #fa9e1b;
            font-size: 24px;
            position: relative;
            float: right;
            top: -30px;
            left: -4px;
        }
        .ui-datepicker-trigger {
            background: none;
            color: #fa9e1b;
            font-size: 24px;
            position: relative;
            float: right;
            top: -37px;
            right: 10px;
            border: none;
        }
        .text_bg_color {
            background: #fa9e1b;
            width: 100%;
            padding-top: 8px;
            color: #fff;
            height: 55px;
            text-align: center;
            line-height: 2;
            border-radius: 10px;
        }
        @media screen and (max-width: 680px) {
            .blog {
                padding-top: 90px;
            }
        }
        @media screen and (max-width: 980px) {
            .home_title {
                font-size: 30px;
            }
        }
    </style>

    <script>
        $(document).ready(function () {
            $("#checkin").datepicker({
                dateFormat: 'dd-mm-yy',
                minDate: 0,
                showOn: 'both',
                buttonText: "<i class=\"far fa-calendar-alt\"></i>"
            }).datepicker('setDate', 'today');
        })
    </script>
@endsection

@section('content')
    <div class="home">
        <div class="home_background parallax-window" data-parallax="scroll" style="height: 0px" data-image-src="/photos/packages/{{ $carPackage->package_image }}"></div>
        <div class="home_content search_car">
            <div class="home_title">{{ $carPackage->package_name }}</div>
        </div>
    </div>

    <!-- Blog -->

    <div class="blog">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @if (count($carPackage->carPhotos))
                        <div class="blog_post_image">
                            <h4 class="text_bg_color" style="margin-bottom: 40px;">{{ $carPackage->car_name }}'s photos</h4>
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    <ul class="slider">
                                        @foreach($carPackage->carPhotos as $photo)
                                            <li>
                                                <img src="/{{ $photo->photos }}">
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>

                <div class="col-md-12" style="margin-top: 45px;">
                    <h4 class="text_bg_color">Car Package Details</h4>
                    <div class="row">
                        <div class="col-md-10 offset-md-1">
                            <div class="blog_post_text">
                                <p>{!! $carPackage->package_details !!}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12" style="margin-top: 45px;">
                    <h4 class="text_bg_color">Pricing Details</h4>
                    <div class="row">
                        <div class="col-md-6 offset-md-3">
                            <p style="float: left; font-size: 16px; color: #000;">Regular Price</p>
                            <p style="float: right; font-size: 16px; color: #000;">৳ {{ number_format($carPackage->previous_price) }}</p>
                            <hr>
                        </div>
                        <div class="col-md-6 offset-md-3">
                            <p style="float: left; font-size: 16px; color: #000;">Current Price</p>
                            <p style="float: right; font-size: 16px; color: #000;">৳ {{ number_format($carPackage->current_price) }}</p>
                            <hr>
                        </div>
                        <div class="col-md-6 offset-md-3">
                            <p style="float: left; font-size: 16px; color: #000;">You Save</p>
                            <p style="float: right; font-size: 16px; color: #000;">৳ {{ number_format($carPackage->previous_price - $carPackage->current_price) }}</p>
                            <hr>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <h4 class="text-center text_bg_color" style="margin: 50px 0;">Book Now</h4>
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            @include('back-end.layouts.messages')
                            <form action="/car-package-booking" method="POST" style="margin-bottom: 30px;">
                                {!! csrf_field() !!}
                                <div class="form-group d-none">
                                    <input type="number" name="car_package_id" value="{{ $carPackage->id }}" class="form-control">
                                </div>
                                <div class="form-group d-none">
                                    <input type="text" name="car_name" value="{{ $carPackage->car_name }}" class="form-control">
                                </div>
                                <div class="form-group d-none">
                                    <input type="number" name="car_rent" value="{{ $carPackage->current_price }}" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="checkin">Hiring Date</label>
                                    <input type="text" id="checkin" name="check_in" class="form-control" required="required" placeholder="DD-MM-YYYY">
                                </div>
                                <div class="form-group">
                                    <label for="people">Total People</label>
                                    <input type="number" min="1" id="people" name="people" class="form-control" required="required" placeholder="Total People">
                                </div>
                                <div class="row">
                                    <div class="col-md-6 offset-md-3 text-center">
                                        <button style="cursor: pointer;" type="submit" class="btn btn-outline-warning btn-block mt-5">Book Now</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
    </div>
@endsection



@section ('footer')
    <script src="/css/styles/bootstrap4/popper.js"></script>
    <script src="/css/styles/bootstrap4/bootstrap.min.js"></script>
    <script src="/plugins/greensock/TweenMax.min.js"></script>
    <script src="/plugins/greensock/TimelineMax.min.js"></script>
    <script src="/plugins/scrollmagic/ScrollMagic.min.js"></script>
    <script src="/plugins/greensock/animation.gsap.min.js"></script>
    <script src="/plugins/greensock/ScrollToPlugin.min.js"></script>
    <script src="/plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
    <script src="/plugins/easing/easing.js"></script>
    <script src="/plugins/parallax-js-master/parallax.min.js"></script>
    <script src="/js/about_custom.js"></script>
    <script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>

    <script>
        $('#datepicker').datepicker();

        $(document).ready(function(){
            $('.slider').bxSlider({
                slideWidth: 1000
            });
        });
    </script>
@endsection
