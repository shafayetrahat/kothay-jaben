@extends ('front-end.layouts.master')

@section ('styles')
    <script src="/js/jquery-3.2.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
    <link href="/plugins/colorbox/colorbox.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="/css/styles/about_styles.css">
    <link rel="stylesheet" type="text/css" href="/css/styles/about_responsive.css">

    <style>
        .home_background {
            background: rgba(0, 0, 0, 0.5);
        }
        .search_car {
            bottom: 85px;
        }
        .card {
            margin: 0 auto;
            float: none;
            margin-bottom: 10px;
            color: #000;
            transition: all .3s;
        }
        .card:hover {
            color: #fff;
            background: linear-gradient(to right, #fa9e1b, #8d4fff);
        }
        .mt-5 {
            margin-top: 4.5rem!important;
        }
		.home_title {
			font-size: 46px;
		}
        @media screen and (max-width: 1040px) {
            .home_title {
                font-size: 40px;
            }
            /*.mt-5 {
                margin-top: 3.5rem!important;
            }*/
            .mt-5 {
                margin-top: 4.7rem!important;
            }
            .
        }
        @media screen and (max-width: 991px) {
            .mt-5 {
                margin-top: 3.5rem!important;
            }
            .disobeyed {
                margin-top: 4.7rem!important;
            }
        }
        @media screen and (max-width: 690px) {
            .home_title {
                font-size: 30px;
            }
            .disobeyed {
                margin-top: 3.5rem!important;
            }
        }
    </style>
@endsection


@section('content')
    <div class="home">
        <div class="home_background parallax-window" data-parallax="scroll" data-image-src="/images/car_type.jpg"></div>
        <div class="home_content search_car">
            <div class="home_title">rent your desired car</div>
        </div>
    </div>

    <div class="intro">
        <div class="container">
            <div class="row">
                <div class="col-md-4 mb-5">
                    <div class="card" style="height: 12rem;">
                        <div class="card-body">
                            <h4 class="card-subtitle mb-2">Today</h4>
                            <span>Want to visit today we can arrange that!</span>
                            <a href="/trip-today" class="mt-5 obeyed btn btn-outline-warning float-right">Book Now</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-5">
                    <div class="card" style="height: 12rem;">
                        <div class="card-body">
                            <h4 class="card-subtitle mb-2">Local Trip</h4>
                            <span>Travel locally with the best deal!</span>
                            <a href="/trip-local" class="mt-5 disobeyed btn btn-outline-warning float-right">Book Now</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-5">
                    <div class="card" style="height: 12rem;">
                        <div class="card-body">
                            <h4 class="card-subtitle mb-2">Tourist Trip</h4>
                            <span>We have the best cars for tourist!</span>
                            <a href="/trip-tourist" class="mt-5 disobeyed btn btn-outline-warning float-right">Book Now</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 mb-5">
                    <div class="card" style="height: 12rem;">
                        <div class="card-body">
                            <h4 class="card-subtitle mb-2">Hourly Trip</h4>
                            <span>The most convenient way to make a trip!</span>
                            <a href="/trip-hourly" class="mt-5 obeyed btn btn-outline-warning float-right">Book Now</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-5">
                    <div class="card" style="height: 12rem;">
                        <div class="card-body">
                            <h4 class="card-subtitle mb-2">Body Contract</h4>
                            <span>Hire once, visit as much as you want!</span>
                            <a href="/trip-body-contract" class="mt-5 obeyed btn btn-outline-warning float-right">Book Now</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-5">
                    <div class="card" style="height: 12rem;">
                        <div class="card-body">
                            <h4 class="card-subtitle mb-2">Wedding</h4>
                            <span>We can decorate the car you want!</span>
                            <a href="/hire-wedding" class="mt-5 disobeyed btn btn-outline-warning float-right">Book Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section ('footer')
    <script src="/css/styles/bootstrap4/popper.js"></script>
    <script src="/css/styles/bootstrap4/bootstrap.min.js"></script>
    <script src="/plugins/greensock/TweenMax.min.js"></script>
    <script src="/plugins/greensock/TimelineMax.min.js"></script>
    <script src="/plugins/scrollmagic/ScrollMagic.min.js"></script>
    <script src="/plugins/greensock/animation.gsap.min.js"></script>
    <script src="/plugins/greensock/ScrollToPlugin.min.js"></script>
    <script src="/plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
    <script src="/plugins/easing/easing.js"></script>
    <script src="/plugins/parallax-js-master/parallax.min.js"></script>
    <script src="/js/about_custom.js"></script>
@endsection
