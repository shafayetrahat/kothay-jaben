@extends ('front-end.layouts.master')

@section ('styles')
    <script src="/js/jquery-3.2.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
    <link href="/plugins/colorbox/colorbox.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="/css/styles/about_styles.css">
    <link rel="stylesheet" type="text/css" href="/css/styles/about_responsive.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

    <style>
        .form-check-inline .form-check-label {
            vertical-align: middle;
            margin-left: -15px;
            margin-top: -5px;
        }

        h1 {
            text-align: center;
        }

        /* Mark input boxes that gets an error on validation: */
        input.invalid {
            background-color: #ffdddd;
        }

        /* Hide all steps by default: */
        .tab {
            display: none;
        }

        /* Make circles that indicate the steps of the form: */
        .step {
            height: 15px;
            width: 15px;
            margin: 0 2px;
            background-color: #fa9e1b;
            border: none;
            border-radius: 50%;
            display: inline-block;
            opacity: 0.7;
        }

        .step.active {
            opacity: 1;
        }

        /* Mark the steps that are finished and valid: */
        .step.finish {
            background-color: #8d4fff;
        }
        .home_background {
            background: rgba(0, 0, 0, 0.5);
        }

        .search_car {
            bottom: 85px;
        }
		.home_title {
			font-size: 46px;
		}
        @media screen and (max-width: 1040px) {
            .home_title {
                font-size: 40px;
            }
        }
        @media screen and (max-width: 690px) {
            .home_title {
                font-size: 30px;
            }
        }
        .ui-datepicker-trigger {
            background: none;
            color: #fa9e1b;
            font-size: 24px;
            position: relative;
            float: right;
            top: -43px;
            right: 10px;
            border: none;
            margin-bottom: -50px;
        }
    </style>
    <script>
        $(document).ready(function () {
            $("#tripDate").datepicker({
                dateFormat: 'dd-mm-yy',
                minDate: 0,
                showOn: 'both',
                buttonText: "<i class=\"far fa-calendar-alt\"></i>"
            }).datepicker('setDate', 'today');

            $('.timepicker').timepicker({
                timeFormat: 'h:mm p',
                dynamic: false,
                dropdown: false,
                scrollbar: false
            });
        });
    </script>
@endsection

@section('content')
<div class="home">
    <div class="home_background parallax-window" data-parallax="scroll" data-image-src="/images/wedding_car.jpg"></div>
    <div class="home_content search_car">
        <div class="home_title">rent your desired car</div>
    </div>
</div>

<div class="intro">
    <div class="container" style="color: #000;">
        <div class="row">
            <div class="col-md-6 offset-md-3 mb-5 text-center">
                <h2>Book Car For Wedding</h2>
            </div>
        </div>
        <div class="col-md-8 offset-md-2">
            <form id="regForm" action="/wedding-car-details">
                <!-- One "tab" for each step in the form: -->
                <div class="tab form-group">
                    <p>
						<label for="tripDate">Wedding Date:</label>
						<input type="text" id="tripDate" name="wedding_date" class="destination  form-control" required="required" placeholder="DD-MM-YYYY">
					</p>
                    <p>
                        <input class="timepicker form-control" name="pick_up_time" placeholder="Pick Up Time">
                        <small class="ml-2" style="color: red; margin-left: -20px;">Example: 2:45PM or 245pm or 1445 all are same</small>
                    </p>
                    <p><input type="text" placeholder="Journey location" class="form-control" name="journey_location"></p>
                    <p><input type="text" placeholder="Center name" class="form-control" name="center_name"></p>
                </div>
                <div class="tab form-group"><span>Vehicle for Groom or Bride:</span>
                    <p></p>
                    <label for="placesToVisit" style="color: #000;">Select a vehicle</label>
                    <select id="placesToVisit" name="wedding_car" class="form-control mb-3">
                        <option value="Mercedes">Mercedes</option>
                        <option value="Prado 2015">Prado 2015</option>
                        <option value="Prado 2008">Prado 2008</option>
                        <option value="Alion 2014">Alion 2014</option>
                        <option value="Premio 2014">Premio 2014</option>
                        <option value="Axio 2013">Axio 2013</option>
                    </select>

                    <label for="" class="mr-5" style="color: #000;">Decoration included:</label>
                    <p></p>
                    <div class="form-check form-check-inline mr-5 ml-5">
                        <input class="form-check-input" type="radio" name="decoration" required id="decorationY" value="Yes">
                        <label class="form-check-label" for="decorationY">Yes</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="decoration" required id="decorationN" value="No">
                        <label class="form-check-label" for="decorationN">No</label>
                    </div>
                </div>
                <div class="tab form-group"><span>Vehicle for Guests:</span>
                    <ul class="nav nav-tabs mt-3" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="car-tab" data-toggle="tab" href="#car" role="tab" aria-controls="car" aria-selected="true">Car</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="noah-tab" data-toggle="tab" href="#noah" role="tab" aria-controls="noah" aria-selected="false">Noah</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="hi-ace-tab" data-toggle="tab" href="#hi-ace" role="tab" aria-controls="hi-ace" aria-selected="false">Hi-ace</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active ml-5 mt-3" id="car" role="tabpanel" aria-labelledby="car-tab">
                            <small style="color: red; margin-left: -20px;">*4 people max</small>
                            <p></p>
                            <div class="form-check form-check-inline mr-5">
                                <input class="form-check-input" type="radio" name="car_type" required id="ecoCar" value="Eco Car">
                                <label class="form-check-label" for="ecoCar">Eco Car</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="car_type" required id="executive" value="Executive Car">
                                <label class="form-check-label" for="executive">Executive Car</label>
                            </div>
                        </div>
                        <div class="tab-pane fade ml-5 mt-3" id="noah" role="tabpanel" aria-labelledby="noah-tab">
                            <small style="color: red; margin-left: -20px;">*7-8 people max</small>
                            <p></p>
                            <div class="form-check form-check-inline mr-5">
                                <input class="form-check-input" type="radio" name="car_type" required id="enoah" value="Eco Noah">
                                <label class="form-check-label" for="enoah">Eco Noah</label>
                            </div>
                            <div class="form-check form-check-inline mr-5">
                                <input class="form-check-input" type="radio" name="car_type" required id="executiveNoah" value="Executive Noah">
                                <label class="form-check-label" for="executiveNoah">Executive Noah</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="car_type" required id="deluxe" value="Deluxe Noah">
                                <label class="form-check-label" for="deluxe">Deluxe Noah</label>
                            </div>
                        </div>
                        <div class="tab-pane fade ml-5 mt-3" id="hi-ace" role="tabpanel" aria-labelledby="hi-ace-tab">
                            <small style="color: red; margin-left: -20px;">*10-11 people max</small>
                            <p></p>
                            <div class="form-check form-check-inline mr-5">
                                <input class="form-check-input" type="radio" name="car_type" required id="dHiAce" value="Deluxe Hi-ace">
                                <label class="form-check-label" for="dHiAce">Deluxe Hi-ace</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="car_type" required id="executiveHiAce" value="Executive Hi-ace">
                                <label class="form-check-label" for="executiveHiAce">Executive Hi-ace</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="overflow:auto;">
                    <div style="float:right;">
                        <button type="button" style="cursor: pointer;" class="btn btn-warning mr-3" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
                        <button type="button" style="cursor: pointer;" class="btn btn-outline-warning" id="nextBtn" onclick="nextPrev(1)">Next</button>
                    </div>
                </div>
                <!-- Circles which indicates the steps of the form: -->
                <div style="text-align:center;margin-top:40px;">
                    <span class="step"></span>
                    <span class="step"></span>
                    <span class="step"></span>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section ('footer')
    <script src="/css/styles/bootstrap4/popper.js"></script>
    <script src="/css/styles/bootstrap4/bootstrap.min.js"></script>
    <script src="/plugins/greensock/TweenMax.min.js"></script>
    <script src="/plugins/greensock/TimelineMax.min.js"></script>
    <script src="/plugins/scrollmagic/ScrollMagic.min.js"></script>
    <script src="/plugins/greensock/animation.gsap.min.js"></script>
    <script src="/plugins/greensock/ScrollToPlugin.min.js"></script>
    <script src="/plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
    <script src="/plugins/easing/easing.js"></script>
    <script src="/plugins/parallax-js-master/parallax.min.js"></script>
    <script src="/js/about_custom.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

    <script>
        var currentTab = 0; // Current tab is set to be the first tab (0)
        showTab(currentTab); // Display the crurrent tab

        function showTab(n) {
            // This function will display the specified tab of the form...
            var x = document.getElementsByClassName("tab");
            x[n].style.display = "block";
            //... and fix the Previous/Next buttons:
            if (n == 0) {
                document.getElementById("prevBtn").style.display = "none";
            } else {
                document.getElementById("prevBtn").style.display = "inline";
            }
            if (n == (x.length - 1)) {
                document.getElementById("nextBtn").innerHTML = "Submit";
            } else {
                document.getElementById("nextBtn").innerHTML = "Next";
            }
            //... and run a function that will display the correct step indicator:
            fixStepIndicator(n)
        }

        function nextPrev(n) {
            // This function will figure out which tab to display
            var x = document.getElementsByClassName("tab");
            // Exit the function if any field in the current tab is invalid:
            if (n == 1 && !validateForm()) return false;
            // Hide the current tab:
            x[currentTab].style.display = "none";
            // Increase or decrease the current tab by 1:
            currentTab = currentTab + n;
            // if you have reached the end of the form...
            if (currentTab >= x.length) {
                // ... the form gets submitted:
                document.getElementById("regForm").submit();
                return false;
            }
            // Otherwise, display the correct tab:
            showTab(currentTab);
        }

        function validateForm() {
            // This function deals with validation of the form fields
            var x, y, i, valid = true;
            x = document.getElementsByClassName("tab");
            y = x[currentTab].getElementsByTagName("input");
            // A loop that checks every input field in the current tab:
            for (i = 0; i < y.length; i++) {
                // If a field is empty...
                if (y[i].value == "") {
                    // add an "invalid" class to the field:
                    y[i].className += " invalid";
                    // and set the current valid status to false
                    valid = false;
                }
            }
            // If the valid status is true, mark the step as finished and valid:
            if (valid) {
                document.getElementsByClassName("step")[currentTab].className += " finish";
            }
            return valid; // return the valid status
        }

        function fixStepIndicator(n) {
            // This function removes the "active" class of all steps...
            var i, x = document.getElementsByClassName("step");
            for (i = 0; i < x.length; i++) {
                x[i].className = x[i].className.replace(" active", "");
            }
            //... and adds the "active" class on the current step:
            x[n].className += " active";
        }

    </script>
@endsection
