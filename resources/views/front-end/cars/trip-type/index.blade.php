@extends ('front-end.layouts.master')

@section ('styles')
    <script src="/js/jquery-3.2.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
    <link href="/plugins/colorbox/colorbox.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="/css/styles/about_styles.css">
    <link rel="stylesheet" type="text/css" href="/css/styles/about_responsive.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

    <style>
        .form-check-inline .form-check-label {
            vertical-align: middle;
            margin-left: -15px;
            margin-top: -5px;
        }

        h1 {
            text-align: center;
        }

        /* Mark input boxes that gets an error on validation: */
        input.invalid {
            background-color: #ffdddd;
        }

        /* Hide all steps by default: */
        .tab {
            display: none;
        }

        /* Make circles that indicate the steps of the form: */
        .step {
            height: 15px;
            width: 15px;
            margin: 0 2px;
            background-color: #fa9e1b;
            border: none;
            border-radius: 50%;
            display: inline-block;
            opacity: 0.7;
        }

        .step.active {
            opacity: 1;
        }

        /* Mark the steps that are finished and valid: */
        .step.finish {
            background-color: #8d4fff;
        }
        .home_background {
            background: rgba(0, 0, 0, 0.5);
        }

        .search_car {
            bottom: 85px;
        }
		.home_title {
			font-size: 50px;
		}
        @media screen and (max-width: 1040px) {
            .home_title {
                font-size: 40px;
            }
        }
        @media screen and (max-width: 690px) {
            .home_title {
                font-size: 20px;
            }
        }
        .ui-datepicker-trigger {
            background: none;
            color: #fa9e1b;
            font-size: 24px;
            position: relative;
            float: right;
            top: -43px;
            right: 10px;
            border: none;
            margin-bottom: -50px;
        }
    </style>
    <script>
        $(document).ready(function () {
            $("#tripDate").datepicker({
                dateFormat: 'dd-mm-yy',
                minDate: 0,
                showOn: 'both',
                buttonText: "<i class=\"far fa-calendar-alt\"></i>"
            }).datepicker('setDate', 'today');

            $('.timepicker').timepicker({
                timeFormat: 'h:mm p',
                dynamic: false,
                dropdown: false,
                scrollbar: false
            });
        });
    </script>
@endsection


@section('content')
    <div class="home">
        <div class="home_background parallax-window" data-parallax="scroll" data-image-src="/images/car_type.jpg"></div>
        <div class="home_content search_car">
            <div class="home_title">rent your desired car</div>
        </div>
    </div>

    @if (Route::getCurrentRoute()->uri() === 'trip-today')
        @include('front-end.cars.trip-type.trip-today')
    @elseif(Route::getCurrentRoute()->uri() === 'trip-local')
        @include('front-end.cars.trip-type.trip-local')
    @elseif(Route::getCurrentRoute()->uri() === 'trip-tourist')
        @include('front-end.cars.trip-type.trip-tourist')
    @elseif(Route::getCurrentRoute()->uri() === 'trip-hourly')
        @include('front-end.cars.trip-type.trip-hourly')
    @elseif(Route::getCurrentRoute()->uri() === 'trip-body-contract')
        @include('front-end.cars.trip-type.trip-body-contract')
    @endif
@endsection

@section ('footer')
    <script src="/css/styles/bootstrap4/popper.js"></script>
    <script src="/css/styles/bootstrap4/bootstrap.min.js"></script>
    <script src="/plugins/greensock/TweenMax.min.js"></script>
    <script src="/plugins/greensock/TimelineMax.min.js"></script>
    <script src="/plugins/scrollmagic/ScrollMagic.min.js"></script>
    <script src="/plugins/greensock/animation.gsap.min.js"></script>
    <script src="/plugins/greensock/ScrollToPlugin.min.js"></script>
    <script src="/plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
    <script src="/plugins/easing/easing.js"></script>
    <script src="/plugins/parallax-js-master/parallax.min.js"></script>
    <script src="/js/about_custom.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

    <script>
        var currentTab = 0; // Current tab is set to be the first tab (0)
        showTab(currentTab); // Display the crurrent tab

        function showTab(n) {
            // This function will display the specified tab of the form...
            var x = document.getElementsByClassName("tab");
            x[n].style.display = "block";
            //... and fix the Previous/Next buttons:
            if (n == 0) {
                document.getElementById("prevBtn").style.display = "none";
            } else {
                document.getElementById("prevBtn").style.display = "inline";
            }
            if (n == (x.length - 1)) {
                document.getElementById("nextBtn").innerHTML = "Submit";
            } else {
                document.getElementById("nextBtn").innerHTML = "Next";
            }
            //... and run a function that will display the correct step indicator:
            fixStepIndicator(n)
        }

        function nextPrev(n) {
            // This function will figure out which tab to display
            var x = document.getElementsByClassName("tab");
            // Exit the function if any field in the current tab is invalid:
            if (n == 1 && !validateForm()) return false;
            // Hide the current tab:
            x[currentTab].style.display = "none";
            // Increase or decrease the current tab by 1:
            currentTab = currentTab + n;
            // if you have reached the end of the form...
            if (currentTab >= x.length) {
                // ... the form gets submitted:
                document.getElementById("regForm").submit();
                return false;
            }
            // Otherwise, display the correct tab:
            showTab(currentTab);
        }

        function validateForm() {
            // This function deals with validation of the form fields
            var x, y, i, valid = true;
            x = document.getElementsByClassName("tab");
            y = x[currentTab].getElementsByTagName("input");
            // A loop that checks every input field in the current tab:
            for (i = 0; i < y.length; i++) {
                // If a field is empty...
                if (y[i].value == "") {
                    // add an "invalid" class to the field:
                    y[i].className += " invalid";
                    // and set the current valid status to false
                    valid = false;
                }
            }
            // If the valid status is true, mark the step as finished and valid:
            if (valid) {
                document.getElementsByClassName("step")[currentTab].className += " finish";
            }
            return valid; // return the valid status
        }

        function fixStepIndicator(n) {
            // This function removes the "active" class of all steps...
            var i, x = document.getElementsByClassName("step");
            for (i = 0; i < x.length; i++) {
                x[i].className = x[i].className.replace(" active", "");
            }
            //... and adds the "active" class on the current step:
            x[n].className += " active";
        }
    </script>
@endsection
