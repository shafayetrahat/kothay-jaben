
    <div class="intro">
        <div class="container" style="color: #000;">
            <div class="row">
                <div class="col-md-6 offset-md-3 mb-5 text-center">
                    <h2>Book Car For Local Trip</h2>
                </div>
            </div>
            <div class="col-md-8 offset-md-2">
                <form id="regForm" action="/local-trip-car-details">
                    <!-- One "tab" for each step in the form: -->
                    <div class="tab form-group">Basic Information:
                        <p class="mt-3">
							<label for="tripDate">Pick-up Date</label>
							<input type="text" id="tripDate" name="pick_up_date" class="destination  form-control" required="required" placeholder="DD-MM-YYYY">
						</p>
                        <p>
                            <input class="timepicker form-control" name="pick_up_time" placeholder="Pick Up Time">
                            <small class="ml-2" style="color: red; margin-left: -20px;">Example: 2:45PM or 245pm or 1445 all are same</small>
                        </p>
                        <p><input type="number" name="total_people" class="form-control" placeholder="Total people"></p>
						<p>
							<label for="pick_up">Pick-up Location</label>
							<select name="pick_up_location" id="pick_up" class="form-control">
								@foreach($pickUpLocation as $location)
									<option value="{{ $location->pick_up_location }}">{{ $location->pick_up_location }}</option>
								@endforeach
							</select>
						</p>
						<p>
							<label for="drop_off">Drop-off Location</label>
							<select name="drop_off_location" id="drop_off" class="form-control">
								@foreach($dropOffLocation as $location)
									<option value="{{ $location->drop_off_location }}">{{ $location->drop_off_location }}</option>
								@endforeach
							</select>
						</p>
                    </div>
                    <div class="tab form-group">Vehicle Type:
                        <ul class="nav nav-tabs mt-3" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="car-tab" data-toggle="tab" href="#car" role="tab" aria-controls="car" aria-selected="true">Car</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="noah-tab" data-toggle="tab" href="#noah" role="tab" aria-controls="noah" aria-selected="false">Noah</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="hi-ace-tab" data-toggle="tab" href="#hi-ace" role="tab" aria-controls="hi-ace" aria-selected="false">Hi-ace</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active ml-5 mt-3" id="car" role="tabpanel" aria-labelledby="car-tab">
                                <small style="color: red; margin-left: -20px;">*4 people max</small>
                                <p></p>
                                <div class="form-check form-check-inline mr-5">
                                    <input class="form-check-input" type="radio" name="car_type" required id="ecoCar" value="Eco Car">
                                    <label class="form-check-label" for="ecoCar">Eco Car</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="car_type" required id="executive" value="Executive Car">
                                    <label class="form-check-label" for="executive">Executive Car</label>
                                </div>
                            </div>
                            <div class="tab-pane fade ml-5 mt-3" id="noah" role="tabpanel" aria-labelledby="noah-tab">
                                <small style="color: red; margin-left: -20px;">*7-8 people max</small>
                                <p></p>
                                <div class="form-check form-check-inline mr-5">
                                    <input class="form-check-input" type="radio" name="car_type" required id="enoah" value="Eco Noah">
                                    <label class="form-check-label" for="enoah">Eco Noah</label>
                                </div>
                                <div class="form-check form-check-inline mr-5">
                                    <input class="form-check-input" type="radio" name="car_type" required id="executiveNoah" value="Executive Noah">
                                    <label class="form-check-label" for="executiveNoah">Executive Noah</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="car_type" required id="deluxe" value="Deluxe Noah">
                                    <label class="form-check-label" for="deluxe">Deluxe Noah</label>
                                </div>
                            </div>
                            <div class="tab-pane fade ml-5 mt-3" id="hi-ace" role="tabpanel" aria-labelledby="hi-ace-tab">
                                <small style="color: red; margin-left: -20px;">*10-11 people max</small>
                                <p></p>
                                <div class="form-check form-check-inline mr-5">
                                    <input class="form-check-input" type="radio" name="car_type" required id="dHiAce" value="Deluxe Hi-ace">
                                    <label class="form-check-label" for="dHiAce">Deluxe Hi-ace</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="car_type" required id="executiveHiAce" value="Executive Hi-ace">
                                    <label class="form-check-label" for="executiveHiAce">Executive Hi-ace</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="overflow:auto;">
                        <div style="float:right;">
                            <button type="button" style="cursor: pointer;"  class="btn btn-warning mr-3" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
                            <button type="button" style="cursor: pointer;" class="btn btn-outline-warning" id="nextBtn" onclick="nextPrev(1)">Next</button>
                        </div>
                    </div>
                    <!-- Circles which indicates the steps of the form: -->
                    <div style="text-align:center;margin-top:40px;">
                        <span class="step"></span>
                        <span class="step"></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
