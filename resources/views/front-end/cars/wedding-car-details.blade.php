@extends ('front-end.layouts.master')

@section ('styles')
	<script src="/js/jquery-3.2.1.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
	<link href="/plugins/colorbox/colorbox.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="/css/styles/blog_styles.css">
	<link rel="stylesheet" type="text/css" href="/css/styles/blog_responsive.css">

	<style>
		.home_background {
			background: rgba(0, 0, 0, 0.5);
		}
		.home_title {
			font-size: 59px;
		}
		.blog {
			padding-top: 0;
		}
		h1, h2, h3, h4, h5 {
			color: #000;
		}
		.blog_post_text p {
			color: #000;
		}
		hr {
			color: #31124b;
			margin-top: 2rem;
		}
		.blog_post_text ul {
			margin-left: 30px;
			color: #000;
			list-style: disc;
		}

		.blog_post_text ol {
			margin-left: 30px;
			color: #3e3e3e;
		}
		.card-text, .col-md-6 {
			color: #000;
		}
		.card-header {
			background: linear-gradient(to right, #fa9e1b, #8d4fff);
		}

		.car-fa-icon {
			color: #fa9e1b;
			font-size: 24px;
			position: relative;
			float: right;
			top: -30px;
			left: -4px;
		}
		.ui-datepicker-trigger {
			background: none;
			color: #fa9e1b;
			font-size: 24px;
			position: relative;
			float: right;
			top: -37px;
			right: 10px;
			border: none;
		}
		.text_bg_color {
			background: #fa9e1b;
			width: 100%;
			padding-top: 8px;
			color: #fff;
			height: 55px;
			text-align: center;
			line-height: 2;
			border-radius: 10px;
		}
		@media screen and (max-width: 680px) {
			.blog {
				padding-top: 90px;
			}
		}
		@media screen and (max-width: 980px) {
			.home_title {
				font-size: 30px;
			}
		}
	</style>

	<?php
		$guestCar = Request::get('car_type');
	?>
@endsection

@section('content')
	<div class="home">
		<div class="home_background parallax-window" data-parallax="scroll" style="height: 0px" data-image-src="/photos/cars/{{ $car->car_feature_image }}"></div>
		<div class="home_content search_car">
			<div class="home_title">{{ $car->car_name }}</div>
		</div>
	</div>

	<!-- Blog -->

	<div class="blog">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					@if (count($car->photos))
						<div class="blog_post_image">
							<h4 class="text_bg_color" style="margin-bottom: 40px;">{{ $car->car_name }}'s photo</h4>
							<div class="row">
								<div class="col-md-10 offset-md-1">
									<ul class="slider">
										@foreach($car->photos as $photo)
											<li>
												<img src="/{{ $photo->photos }}">
											</li>
										@endforeach
									</ul>
								</div>
							</div>
						</div>
					@endif
				</div>

				<div class="col-md-12" style="margin-top: 45px;">
					<h4 class="text_bg_color">Car Details</h4>
					<div class="row">
						<div class="col-md-10 offset-md-1">
							<div class="blog_post_text">
								<p>{!! $car->car_details !!}</p>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-12" style="margin-top: 45px;">
					<h4 class="text_bg_color">Booking Summary</h4>
					<div class="row">
						<div class="col-md-6 offset-md-3">
							<p style="float: left; font-size: 16px; color: #000;">Wedding Date</p>
							<p style="float: right; font-size: 16px; color: #000;">{{ Request::get('wedding_date') }}</p>
							<hr>
						</div>
						<div class="col-md-6 offset-md-3">
							<p style="float: left; font-size: 16px; color: #000;">Pick-up Time</p>
							<p style="float: right; font-size: 16px; color: #000;">{{ Request::get('pick_up_time') }}</p>
							<hr>
						</div>
						<div class="col-md-6 offset-md-3">
							<p style="float: left; font-size: 16px; color: #000;">Journey Location</p>
							<p style="float: right; font-size: 16px; color: #000;">{{ Request::get('journey_location') }}</p>
							<hr>
						</div>
						<div class="col-md-6 offset-md-3">
							<p style="float: left; font-size: 16px; color: #000;">Center Name</p>
							<p style="float: right; font-size: 16px; color: #000;">{{ Request::get('center_name') }}</p>
							<hr>
						</div>
						<div class="col-md-6 offset-md-3">
							<p style="float: left; font-size: 16px; color: #000;">Wedding Car</p>
							<p style="float: right; font-size: 16px; color: #000;">{{ Request::get('wedding_car') }}</p>
							<hr>
						</div>
						<div class="col-md-6 offset-md-3">
							<p style="float: left; font-size: 16px; color: #000;">Decoration Included</p>
							<p style="float: right; font-size: 16px; color: #000;">{{ Request::get('decoration') }}</p>
							<hr>
						</div>
						<div class="col-md-6 offset-md-3">
							<p style="float: left; font-size: 16px; color: #000;">Vehicle for Guest</p>
							<p style="float: right; font-size: 16px; color: #000;">{{ isset($guestCar) ? $guestCar : 'None' }}</p>
							<hr>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-8 offset-md-2">
							@include('back-end.layouts.messages')
							<form action="/hire-wedding-booking" method="POST" style="margin-bottom: 30px;">
								{!! csrf_field() !!}
								<div class="form-group d-none">
									<input type="number" name="car_id" value="{{ $car->id }}" class="form-control">
								</div>
								<div class="form-group d-none">
									<input type="text" name="wedding_car" value="{{ Request::get('wedding_car') }}" class="form-control">
								</div>
								<div class="form-group d-none">
									<input type="text" name="wedding_car_decoration" value="{{ Request::get('decoration') }}" class="form-control">
								</div>
								<div class="form-group d-none">
									<input type="text" name="guest_car" value="{{ isset($guestCar) ? $guestCar : 'None' }}" class="form-control">
								</div>
								<div class="form-group d-none">
									<input type="text" name="journey_location" value="{{ Request::get('journey_location') }}" class="form-control">
								</div>
								<div class="form-group d-none">
									<input type="text" name="wedding_date" value="{{ Request::get('wedding_date') }}" class="form-control" required="required" placeholder="DD-MM-YYYY">
								</div>
								<div class="form-group d-none">
									<input type="text" name="center_name" value="{{ Request::get('center_name') }}" class="form-control">
								</div>
								<div class="form-group d-none">
									<input class="form-control" id="time" name="pick_up_time" value="{{ Request::get('pick_up_time') }}">
								</div>
								<div class="row">
									<div class="col-md-6 offset-md-3 text-center">
										<button style="cursor: pointer;" type="submit" class="btn btn-outline-warning btn-block mt-5">Book Now</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
	</div>
@endsection



@section ('footer')
	<script src="/css/styles/bootstrap4/popper.js"></script>
	<script src="/css/styles/bootstrap4/bootstrap.min.js"></script>
	<script src="/plugins/greensock/TweenMax.min.js"></script>
	<script src="/plugins/greensock/TimelineMax.min.js"></script>
	<script src="/plugins/scrollmagic/ScrollMagic.min.js"></script>
	<script src="/plugins/greensock/animation.gsap.min.js"></script>
	<script src="/plugins/greensock/ScrollToPlugin.min.js"></script>
	<script src="/plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
	<script src="/plugins/easing/easing.js"></script>
	<script src="/plugins/parallax-js-master/parallax.min.js"></script>
	<script src="/js/about_custom.js"></script>
	<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>

	<script>
		$('#datepicker').datepicker();

		$(document).ready(function(){
			$('.slider').bxSlider({
				slideWidth: 1000
			});
		});
	</script>
@endsection
