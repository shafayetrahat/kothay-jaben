@extends('front-end.layouts.master')

@section('styles')
	<link rel="stylesheet" type="text/css" href="/css/styles/offers_styles.css">
	<link rel="stylesheet" type="text/css" href="/css/styles/offers_responsive.css">
@endsection

@section('content')
	<!-- Home -->

	<div class="home">
		<div class="home_background parallax-window" data-parallax="scroll" data-image-src="images/about_background.jpg"></div>
		<div class="home_content">
			<div class="home_title">our hotel packages</div>
		</div>
	</div>

	<!-- Offers -->

	<div class="offers">

		<!-- Offers -->

		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<!-- Offers Grid -->

					<div class="offers_grid">

						<!-- Offers Item -->

						@foreach($hotelPackages as $hotelPackage)
                            <div class="offers_item rating_4">
                                <div class="row">
                                    <div class="col-lg-1 temp_col"></div>
                                    <div class="col-lg-3 col-1680-4">
                                        <div class="offers_image_container">
                                            <div class="offers_image_background" style="background-image:url(photos/packages/{{ $hotelPackage->package_image }})"></div>
                                            <div class="offer_name"><a href="/detailed-hotel-package/{{ $hotelPackage->id }}">{{ $hotelPackage->hotel_name }}</a></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="offers_content">
                                            <h3 style="color: #fa9e1b; text-transform: uppercase;">{{ $hotelPackage->package_name }}</h3>
                                            <p class="offers_text">
                                                {{ substr(strip_tags($hotelPackage->package_details), 0, 210) }}{{ strlen(strip_tags($hotelPackage->package_details)) > 210 ? "..." : "" }}
                                            </p>
                                            <div class="offers_icons" style="color: #fa9e1b;">
                                                <p  style="color: #fa9e1b; margin-bottom: 0; font-weight: 700;">Regular Price: 
                                                    <span style="color: red; text-decoration:line-through">
                                                        <span style="color: #fa9e1b;">
                                                            ৳ {{ number_format($hotelPackage->previous_rent) }}
                                                        </span>
                                                    </span>
                                                </p>
                                                <p  style="color: #fa9e1b; margin-bottom: -30px; font-weight: 700;">Current Price: ৳ {{ number_format($hotelPackage->current_rent) }}</p>
                                            </div>
                                            <div class="button book_button"><a href="/detailed-hotel-package/{{ $hotelPackage->id }}">book<span></span><span></span><span></span></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
						<!-- Offers Item -->
					</div>
				</div>

			</div>
		</div>
	</div>
    <div class="row">
        <div class="col-lg-12"></div>
        <div class="col-md-4"></div>
        <div class="col-md-4 ml-3">
            <p>{!! $hotelPackages->links() !!}</p>
        </div>
        <div class="col-md-4"></div>
    </div>
@endsection

@section('footer')
	<script src="/js/jquery-3.2.1.min.js"></script>
	<script src="/css/styles/bootstrap4/popper.js"></script>
	<script src="/css/styles/bootstrap4/bootstrap.min.js"></script>
	<script src="/plugins/Isotope/isotope.pkgd.min.js"></script>
	<script src="/plugins/easing/easing.js"></script>
	<script src="/plugins/parallax-js-master/parallax.min.js"></script>
	<script src="/js/offers_custom.js"></script>
@endsection
