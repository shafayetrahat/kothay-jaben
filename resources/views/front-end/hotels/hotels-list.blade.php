@extends('front-end.layouts.master')

@section('styles')
	<link rel="stylesheet" type="text/css" href="/css/styles/offers_styles.css">
	<link rel="stylesheet" type="text/css" href="/css/styles/offers_responsive.css">

	<style>
		.home_background {
			background: rgba(0, 0, 0, .3);
		}
	</style>
@endsection

@section('content')
	<!-- Home -->

	<div class="home">
		<div class="home_background parallax-window" data-parallax="scroll" data-image-src="images/hotel_view.jpg"></div>
		<div class="home_content">
			<div class="home_title">our hotels</div>
		</div>
	</div>

	<!-- Offers -->

	<div class="offers">

		<!-- Offers -->

		<div class="container">
			<div class="row">
				@foreach($hotels as $hotel)
				<div class="col-lg-12">
					<!-- Offers Grid -->

					<div class="offers_grid">

						<!-- Offers Item -->

							<form action="/hotels-list/{{ $hotel->id }}/hotel-rooms" class="pass-data">
								{!! csrf_field() !!}
								<div class="col-md-3 form-group d-none">
									<input type="text" name="check_in" value="{{ Request::get('check_in') }}" class="form-control" id="pick_up">
								</div>
								<div class="col-md-3 form-group d-none">
									<input type="text" name="check_out" value="{{ Request::get('check_out') }}" class="form-control" id="drop_off">
								</div>
								<div class="col-md-3 form-group d-none">
									<input type="number" name="adults" value="{{ Request('adults') }}" class="form-control" id="people">
								</div>
								<div class="col-md-3 form-group d-none">
									<input type="number" name="children" value="{{ Request('children') }}" class="form-control" id="people">
								</div>

								<div class="offers_item rating_4">
									<div class="row">
										<div class="col-lg-1 temp_col"></div>
										<div class="col-lg-3 col-1680-4">
											<div class="offers_image_container">
												<div class="offers_image_background" style="background-image:url(photos/hotels/{{ $hotel->hotel_image }})"></div>
												<div class="offer_name"><a href="#">{{ $hotel->hotel_name }}</a></div>
											</div>
										</div>
										<div class="col-lg-8">
											<div class="offers_content">
												<h3 style="color: #fa9e1b; text-transform: uppercase;">{{ $hotel->hotel_name }}</h3>
												<p class="offers_text">
													{{ substr(strip_tags($hotel->hotel_details), 0, 210) }}{{ strlen(strip_tags($hotel->hotel_details)) > 210 ? "..." : "" }}
												</p>
												<div class="offers_icons" style="color: #fa9e1b;">
													Location: {{ $hotel->hotel_location }}
												</div>
												<button type="submit" class="btn btn-outline-warning mt-4" style="cursor: pointer">See Rooms</button>
											</div>
										</div>
									</div>
								</div>
							</form>
					<!-- Offers Item -->
					</div>
				</div>
				@endforeach

			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12"></div>
		<div class="col-md-4"></div>
		<div class="col-md-4 ml-3">
			<p>{!! $hotels->links() !!}</p>
		</div>
		<div class="col-md-4"></div>
	</div>
@endsection

@section('footer')
	<script src="/js/jquery-3.2.1.min.js"></script>
	<script src="/css/styles/bootstrap4/popper.js"></script>
	<script src="/css/styles/bootstrap4/bootstrap.min.js"></script>
	<script src="/plugins/Isotope/isotope.pkgd.min.js"></script>
	<script src="/plugins/easing/easing.js"></script>
	<script src="/plugins/parallax-js-master/parallax.min.js"></script>
	<script src="/js/offers_custom.js"></script>
@endsection
