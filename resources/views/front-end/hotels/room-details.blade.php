@extends ('front-end.layouts.master')

@section ('styles')
	<script src="/js/jquery-3.2.1.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
	<link href="/plugins/colorbox/colorbox.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="/css/styles/blog_styles.css">
	<link rel="stylesheet" type="text/css" href="/css/styles/blog_responsive.css">

	<style>
		.home_background {
			background: rgba(0, 0, 0, 0.5);
		}
		.blog {
			padding-top: 0;
		}
		h1, h2, h3, h4, h5 {
			color: #000;
		}
		.blog_post_text p {
			color: #000;
		}
		hr {
			color: #31124b;
			margin-top: 2rem;
		}
		.blog_post_text ul {
			margin-left: 30px;
			color: #000;
			list-style: disc;
		}

		.blog_post_text ol {
			margin-left: 30px;
			color: #3e3e3e;
		}
		.card-text, .col-md-6 {
			color: #000;
		}
		.card-header {
			background: linear-gradient(to right, #fa9e1b, #8d4fff);
		}

		.car-fa-icon {
			color: #fa9e1b;
			font-size: 24px;
			position: relative;
			float: right;
			top: -30px;
			left: -4px;
		}
		.ui-datepicker-trigger {
			background: none;
			color: #fa9e1b;
			font-size: 24px;
			position: relative;
			float: right;
			top: -37px;
			right: 10px;
			border: none;
		}
		.text_bg_color {
			background: #fa9e1b;
			width: 100%;
			padding-top: 8px;
			color: #fff;
			height: 55px;
			text-align: center;
			line-height: 2;
			border-radius: 10px;
		}
		@media screen and (max-width: 680px) {
			.blog {
				padding-top: 90px;
			}
		}
		@media screen and (max-width: 980px) {
			.home_title {
				font-size: 30px;
			}
		}
	</style>

	<script>
        $(document).ready(function () {
            $("#checkin").datepicker({
                dateFormat: 'dd-mm-yy',
                minDate: 0,
                showOn: 'both',
                buttonText: "<i class=\"far fa-calendar-alt\"></i>"
            });

            $("#checkout").datepicker({
                dateFormat: 'dd-mm-yy',
                minDate: 1,
                showOn: 'both',
                buttonText: "<i class=\"far fa-calendar-alt\"></i>"
            });
        })
	</script>

    <?php
    $start = \Carbon\Carbon::parse( Request::get('check_in'));
    $end = \Carbon\Carbon::parse( Request::get('check_out'));

    $days = $end->diffInDays($start);
    ?>
@endsection

@section('content')
	<div class="home">
		<div class="home_background parallax-window" data-parallax="scroll" style="height: 0px" data-image-src="/photos/hotel-rooms/{{ $hotelRooms->room_image }}"></div>
		<div class="home_content search_car">
			<div class="home_title">{{ $hotelRooms->room_title }}</div>
		</div>
	</div>

	<!-- Blog -->

	<div class="blog">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					@if (count($hotelRooms->roomPhotos))
						<div class="blog_post_image">
							<h4 class="text_bg_color" style="margin-bottom: 40px;">{{ $hotelRooms->room_title }}'s photos</h4>
							<div class="row">
								<div class="col-md-10 offset-md-1">
									<ul class="slider">
										@foreach($hotelRooms->roomPhotos as $photo)
											<li>
												<img src="/{{ $photo->photos }}">
											</li>
										@endforeach
									</ul>
								</div>
							</div>
						</div>
					@endif
				</div>

				<div class="col-md-12" style="margin-top: 45px;">
					<h4 class="text_bg_color">Room Details</h4>
					<div class="row">
						<div class="col-md-10 offset-md-1">
							<div class="blog_post_text">
								<p>{!! $hotelRooms->room_details !!}</p>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-12" style="margin-top: 45px;">
					<h4 class="text_bg_color">Booking Details</h4>
					<div class="row">
						<div class="col-md-6 offset-md-3">
							<p style="float: left; font-size: 16px; color: #000;">Hotel Location</p>
							<p style="float: right; font-size: 16px; color: #000;">{{ $hotelRooms->hotel->hotel_location }}</p>
							<hr>
						</div>
						<div class="col-md-6 offset-md-3">
							<p style="float: left; font-size: 16px; color: #000;">Check-In Date</p>
							<p class="checkInDate" style="float: right; font-size: 16px; color: #000;">{{ Request::get('check_in') }}</p>
							<p class="ck"></p>
							<hr>
						</div>
						<div class="col-md-6 offset-md-3">
							<p style="float: left; font-size: 16px; color: #000;">Check-Out Date</p>
							<p class="checkOutDate" style="float: right; font-size: 16px; color: #000;">{{ Request::get('check_out') }}</p>
							<hr>
						</div>
						<div class="col-md-6 offset-md-3">
							<p style="float: left; font-size: 16px; color: #000;">Adults</p>
							<p class="tot_adult" style="float: right; font-size: 16px; color: #000;">{{ Request::get('adults') }}</p>
							<hr>
						</div>
						<div class="col-md-6 offset-md-3">
							<p style="float: left; font-size: 16px; color: #000;">Children</p>
							<p class="tot_children" style="float: right; font-size: 16px; color: #000;">{{ Request::get('children') }}</p>
							<hr>
						</div>
					</div>
				</div>

				<div class="col-md-12" style="margin-top: 45px;">
					<h4 class="text_bg_color">Pricing Details</h4>
					<div class="row">
						<div class="col-md-6 offset-md-3">
							<p style="float: left; font-size: 16px; color: #000;">Rent per night</p>
							<p id="package_price" style="float: right; font-size: 16px; color: #000;">৳ {{ number_format($hotelRooms->room_rent) }}</p>
							<hr>
						</div>
						<div class="col-md-6 offset-md-3">
							<p style="float: left; font-size: 16px; color: #000;">Rent for <span class="noOfDays"></span> nights</p>
							<p id="total_price" style="float: right; font-size: 16px; color: #000;">{{--৳ {{ number_format($hotelRooms->room_rent * $days) }}--}}</p>
							<hr>
						</div>
					</div>
				</div>

				<div class="col-md-12">
					<h4 class="text-center text_bg_color" style="margin: 50px 0;">Book Now</h4>
					<div class="row">
						<div class="col-md-8 offset-md-2">
							@include('back-end.layouts.messages')
							<form action="/hotel/booking" method="POST" style="margin-bottom: 30px;">
								{!! csrf_field() !!}
								<div class="form-group d-none">
									<input type="number" name="room_id" value="{{ $hotelRooms->id }}" class="form-control">
								</div>
								<div class="form-group d-none">
									<input type="text" name="hotel_name" value="{{ $hotelRooms->hotel->hotel_name }}" class="form-control">
								</div>
								<div class="form-group d-none">
									<input type="text" name="room_title" value="{{ $hotelRooms->room_title }}" class="form-control">
								</div>
								<div class="form-group d-none">
									<input type="number" name="room_rent" class="form-control subPrice">
								</div>
								<div class="form-group">
									<label for="checkin">Check-In Date</label>
									<input type="text" id="checkin"  name="check_in" value="{{ Request::get('check_in') }}" class="form-control">
								</div>
								<div class="form-group">
									<label for="checkout">Check-Out Date</label>
									<input type="text" id="checkout"  name="check_out" value="{{ Request::get('check_out') }}" class="form-control">
								</div>
								<div class="form-group">
									<label for="adult">Adults</label>
									<input type="number" id="adult" name="adults" min="1" max="20" value="{{ Request::get('adults') }}" class="form-control" required>
								</div>
								<div class="form-group">
									<label for="children">Children</label>
									<input type="number" id="children" name="children" min="0" max="20" value="{{ Request('children') }}" class="form-control" required>
								</div>
								<div class="row">
									<div class="col-md-6 offset-md-3 text-center">
										<button style="cursor: pointer;" type="submit" class="btn btn-outline-warning btn-block mt-5">Book Now</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
	</div>
@endsection



@section ('footer')
	<script src="/css/styles/bootstrap4/popper.js"></script>
	<script src="/css/styles/bootstrap4/bootstrap.min.js"></script>
	<script src="/plugins/greensock/TweenMax.min.js"></script>
	<script src="/plugins/greensock/TimelineMax.min.js"></script>
	<script src="/plugins/scrollmagic/ScrollMagic.min.js"></script>
	<script src="/plugins/greensock/animation.gsap.min.js"></script>
	<script src="/plugins/greensock/ScrollToPlugin.min.js"></script>
	<script src="/plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
	<script src="/plugins/easing/easing.js"></script>
	<script src="/plugins/parallax-js-master/parallax.min.js"></script>
	<script src="/js/about_custom.js"></script>
	<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>

	<script>
        $('#datepicker').datepicker();

        $(document).ready(function(){
            $('.slider').bxSlider({
                slideWidth: 1000
            });
        });

        // Number of days from date
		var totPrice = "{{ $hotelRooms->room_rent }}";
        function parseDate(str) {
            var mdy = str.split('-');
            return new Date(mdy[2], mdy[1]-1, mdy[0]);
        }

        function datediff(first, second) {
            return Math.round((second-first)/(1000*60*60*24));
        }

        var date = datediff(parseDate(checkin.value), parseDate(checkout.value));

        document.querySelector('.noOfDays').textContent = date;
        document.querySelector('#total_price').textContent = '৳ ' + (parseInt(totPrice) * date).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        document.querySelector('.subPrice').value = parseInt(totPrice) * date;

        document.getElementById('checkin').onchange = function() {
            var date = datediff(parseDate(checkin.value), parseDate(checkout.value));
            var checkIn = checkin.value;

            document.querySelector('.noOfDays').textContent = date;
            document.querySelector('#total_price').textContent = '৳ ' + (parseInt(totPrice) * date).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            document.querySelector('.subPrice').value = parseInt(totPrice) * date;
            document.querySelector('.checkInDate').textContent = checkIn;
        };

        document.getElementById('checkout').onchange = function() {
            var date = datediff(parseDate(checkin.value), parseDate(checkout.value));
            var checkOut = checkout.value;

            document.querySelector('.noOfDays').textContent = date;
            document.querySelector('#total_price').textContent = '৳ ' + (parseInt(totPrice) * date).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            document.querySelector('.subPrice').value = parseInt(totPrice) * date;
            document.querySelector('.checkOutDate').textContent = checkOut;
        };

        document.getElementById('adult').onchange = function() {
            var totAdult = adult.value;

            document.querySelector('.tot_adult').textContent = totAdult;
        }

        document.getElementById('children').onchange = function() {
            var totChildren = children.value;

            document.querySelector('.tot_children').textContent = totChildren;
        }
	</script>
@endsection