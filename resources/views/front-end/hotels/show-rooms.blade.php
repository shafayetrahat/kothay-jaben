@extends('front-end.layouts.master')

@section('styles')
	<link rel="stylesheet" type="text/css" href="/css/styles/offers_styles.css">
	<link rel="stylesheet" type="text/css" href="/css/styles/offers_responsive.css">

	<style>
		.home_background {
			background: rgba(0, 0, 0, .3);
		}
	</style>
@endsection

@section('content')
	<!-- Home -->

	<div class="home">
		<div class="home_background parallax-window" data-parallax="scroll" data-image-src="/photos/hotels/{{ $hotels->hotel_image }}"></div>
		<div class="home_content">
			<div class="home_title">{{ $hotels->hotel_name }}</div>
		</div>
	</div>

	<!-- Offers -->

	<div class="offers">

		<!-- Offers -->

		<div class="container">
			<div class="row">
				@foreach($hotels->hotelRooms as $room)
				<div class="col-lg-12">
					<!-- Offers Grid -->

					<div class="offers_grid">

						<!-- Offers Item -->

							<form action="/hotels-list/{{ $room->id }}/hotel-rooms-details" class="pass-data">
								{!! csrf_field() !!}
								<div class="col-md-3 form-group d-none">
									<input type="text" name="check_in" value="{{ Request::get('check_in') }}" class="form-control" id="pick_up">
								</div>
								<div class="col-md-3 form-group d-none">
									<input type="text" name="check_out" value="{{ Request::get('check_out') }}" class="form-control" id="drop_off">
								</div>
								<div class="col-md-3 form-group d-none">
									<input type="number" name="adults" value="{{ Request('adults') }}" class="form-control" id="people">
								</div>
								<div class="col-md-3 form-group d-none">
									<input type="number" name="children" value="{{ Request('children') }}" class="form-control" id="people">
								</div>

								<div class="offers_item rating_4">
									<div class="row">
										<div class="col-lg-1 temp_col"></div>
										<div class="col-lg-3 col-1680-4">
											<div class="offers_image_container">
												<div class="offers_image_background" style="background-image:url(/photos/hotel-rooms/{{ $room->room_image }})"></div>
												<div class="offer_name"><a href="#">for {{ $room->room_capacity }} person</a></div>
											</div>
										</div>
										<div class="col-lg-8">
											<div class="offers_content">
												<h3 style="color: #fa9e1b; text-transform: uppercase;">{{ $room->room_title }}</h3>
												<p class="offers_text">
													{{ substr(strip_tags($room->room_details), 0, 210) }}{{ strlen(strip_tags($room->room_details)) > 210 ? "..." : "" }}
												</p>
												<div class="offers_icons" style="color: #fa9e1b;">
													৳ {{ $room->room_rent }} per night
												</div>
												<button type="submit" class="btn btn-outline-warning mt-4" style="cursor: pointer">See Details</button>
											</div>
										</div>
									</div>
								</div>
							</form>
					<!-- Offers Item -->
					</div>
				</div>
				@endforeach

			</div>
		</div>
	</div>
@endsection

@section('footer')
	<script src="/js/jquery-3.2.1.min.js"></script>
	<script src="/css/styles/bootstrap4/popper.js"></script>
	<script src="/css/styles/bootstrap4/bootstrap.min.js"></script>
	<script src="/plugins/Isotope/isotope.pkgd.min.js"></script>
	<script src="/plugins/easing/easing.js"></script>
	<script src="/plugins/parallax-js-master/parallax.min.js"></script>
	<script src="/js/offers_custom.js"></script>
@endsection
