<!-- Footer -->

	<footer class="footer">
		<div class="container">
			<div class="row">

				<!-- Footer Column -->
				<div class="col-lg-4 footer_column">
					<div class="footer_col">
						<div class="footer_content footer_about">
							<div class="logo_container footer_logo">
								@foreach($contactInfo as $item)
									<div class="logo"><a href="/"><img src="/photos/logo/{{ $item->site_logo }}" alt="logo"></a></div>
								@endforeach
							</div>
							{{--<p class="footer_about_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus quis vu lputate eros, iaculis consequat nisl. Nunc et suscipit urna. Integer eleme ntum orci eu vehicula pretium.</p>--}}
							<ul class="footer_social_list">
								<li class="footer_social_item"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
							</ul>
						</div>
					</div>
				</div>

				<!-- Footer Column -->
				<div class="col-lg-4 footer_column">
					<div class="footer_col">
						<div class="footer_title">blog posts</div>
						<div class="footer_content footer_blog">
							
							<!-- Footer blog item -->
							@foreach($blogPosts as $post)
								<div class="footer_blog_item clearfix">
									<div class="footer_blog_image">
										<a href="/blog-post/{{ $post->id }}">
											<img src="/photos/blog/{{ $post->featured_image }}" alt="https://unsplash.com/@avidenov">
										</a>
									</div>
									<div class="footer_blog_content">
										<div class="footer_blog_title">
											<a href="/blog-post/{{ $post->id }}">
												{{ $post->title }}
											</a>
										</div>
										<div class="footer_blog_date">{{ $post->created_at->toFormattedDateString() }}</div>
									</div>
								</div>
							@endforeach
						</div>
					</div>
				</div>

				<!-- Footer Column -->
				{{--<div class="col-lg-4 footer_column">
					<div class="footer_col">
						<div class="footer_title">tags</div>
						<div class="footer_content footer_tags">
							<ul class="tags_list clearfix">
								<li class="tag_item"><a href="#">design</a></li>
								<li class="tag_item"><a href="#">fashion</a></li>
								<li class="tag_item"><a href="#">music</a></li>
								<li class="tag_item"><a href="#">video</a></li>
								<li class="tag_item"><a href="#">party</a></li>
								<li class="tag_item"><a href="#">photography</a></li>
								<li class="tag_item"><a href="#">adventure</a></li>
								<li class="tag_item"><a href="#">travel</a></li>
							</ul>
						</div>
					</div>
				</div>--}}

				<!-- Footer Column -->
				<div class="col-lg-4 footer_column">
					<div class="footer_col">
						<div class="footer_title">contact info</div>
						<div class="footer_content footer_contact">
							@foreach($contactInfo as $info)
							<ul class="contact_info_list">
								<li class="contact_info_item d-flex flex-row">
									<div><div class="contact_info_icon"><img src="/images/placeholder.svg" alt=""></div></div>
									<div class="contact_info_text">{{ $info->address }}</div>
								</li>
								<li class="contact_info_item d-flex flex-row">
									<div><div class="contact_info_icon"><img src="/images/phone-call.svg" alt=""></div></div>
									<div class="contact_info_text">{{ $info->phone_number }}</div>
								</li>
								<li class="contact_info_item d-flex flex-row">
									<div><div class="contact_info_icon"><img src="/images/message.svg" alt=""></div></div>
									<div class="contact_info_text">{{ $info->email }}</div>
								</li>
							</ul>
							@endforeach
						</div>
					</div>
				</div>

			</div>
		</div>
	</footer>

	<!-- Copyright -->

	<div class="copyright">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 order-lg-1 order-2  ">
					<div class="copyright_content d-flex flex-row align-items-center">
						<div><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> KothayJaben | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></div>
					</div>
				</div>
				<div class="col-lg-9 order-lg-2 order-1">
					<div class="footer_nav_container d-flex flex-row align-items-center justify-content-lg-end">
						<div class="footer_nav">
							<ul class="footer_nav_list">
								<li class="footer_nav_item"><a href="/">home</a></li>
								<li class="footer_nav_item"><a href="/hotel-packages">hotels</a></li>
								<li class="footer_nav_item"><a href="/hiring-type">cars</a></li>
								<li class="footer_nav_item"><a href="/tour-pckages">tour</a></li>
								<li class="footer_nav_item"><a href="#">flight</a></li>
								<li class="footer_nav_item"><a href="/blog-post">blog</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

