<!DOCTYPE html>
<html lang="en">
<head>
    <title>KothayJaben</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Travelix Project">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/styles/bootstrap4/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <style>
        .main_nav_list {
            margin-right: -90px;
        }
        .fa, .fas, .fab {
            font-size: 25px;
            color: white;
        }
        .badge-warning {
            position: relative;
            bottom: 27px;
            right: 10px;
        }
        .bg_color {
            background: #fa9e1b;
            width: 55px;
            text-align: center;
            height: 55px;
            line-height: 2.25;
            border-radius: 9999999px;
        }
        ::selection {
	        color: #fa9e1b;
	        background: #000;
        }
    </style>
    @yield ('styles')
</head>

<body>

    <div class="super_container">
        @include('front-end.layouts.navbar')

        @yield('content')

        @include('front-end.layouts.footer')
    </div>

    @yield ('footer')
</body>
</html>
