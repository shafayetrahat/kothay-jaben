<!-- Header -->
<header class="header">

    <!-- Top Bar -->

    <div class="top_bar">
        <div class="container">
            <div class="row">
                <div class="col d-flex flex-row">
	                @foreach($contactInfo as $info)
                    <div class="phone"><i class="fas fa-phone-square" style="font-size: 12px; margin-right: 5px;"></i> {{ $info->phone_number }}</div>
				@endforeach
                    <div class="social">
                        <ul class="social_list">
                            <li class="social_list_item"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        </ul>
                    </div>
                    <div class="user_box ml-auto">
                        @guest
	                        <div class="user_box_login user_box_link"><a href="/login">login</a></div>
	                        <div class="user_box_register user_box_link"><a href="/register">register</a></div>
                        @else
							<div class="nav-item dropdown">
                                <a style="color:#fff;" id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
	                                {{ ucfirst(Auth::user()->name) }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
	                                <a class="dropdown-item" href="/user-profile">
		                                Profile
	                                </a>
	                                <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
	                                    {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
							</div>
                        @endguest
                    </div>
                </div>
            </div>
        </div>		
    </div>

    <!-- Main Navigation -->

    <nav class="main_nav">
        <div class="container">
            <div class="row">
                <div class="col main_nav_col d-flex flex-row align-items-center justify-content-start">
                    <div class="logo_container">
                        @foreach($contactInfo as $item)
                            <div class="logo"><a href="/"><img src="/photos/logo/{{ $item->site_logo }}" alt="Logo"></a></div>
                        @endforeach
                    </div>
                    <div class="main_nav_container ml-auto">
                        <ul class="main_nav_list">
                            <li class="main_nav_item" style="text-align: center;">
                                <a href="/">
                                    <i class="fas fa-hotel bg_color" style="margin-bottom: 10px;"></i><br>
                                    home
                                </a>
                            </li>
                            <li class="main_nav_item" style="text-align: center;">
                                <a href="/hotel-packages">
                                    <i class="fas fa-hotel bg_color" style="margin-bottom: 10px;"></i><br>
                                    hotels
                                </a>
                            </li>
                            <li class="main_nav_item" style="text-align: center;">
                                <a href="/hiring-type">
                                    <i class="fas fa-car bg_color" style="margin-bottom: 10px; background: #a923d6;"></i><br>
                                    cars
                                </a>
                            </li>
                            <li class="main_nav_item" style="text-align: center;">
                                <a href="/tour-packages">
                                    <i class="fas fa-umbrella-beach bg_color" style="margin-bottom: 10px; background: #a96a5f;"></i><br>
                                    tours
                                </a>
                            </li>
                            <li class="main_nav_item" style="text-align: center;">
                                <a href="#">
                                    <i class="fas fa-plane-departure bg_color" style="margin-bottom: 10px; background: #86cd97;"></i><br>
                                    flight
                                </a>
                            </li>
                            <li class="main_nav_item" style="text-align: center;">
                                <a href="/our-products">
                                    <i class="fab fa-product-hunt bg_color" style="margin-bottom: 10px; background: #7d73b2;"></i><br>
                                    products
                                </a>
                            </li>
                            <li class="main_nav_item" style="text-align: center;">
                                <a href="/blog-post">
                                    <i class="fab fa-blogger bg_color" style="margin-bottom: 10px; background: #c4ad5b;"></i><br>
                                    blog
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="content_search ml-lg-0 ml-auto" >
	                    <a href="{{ route('product.shoppingCart') }}">
	                        <i class="fas fa-cart-plus"></i>
	                        <span class="badge badge-warning">{{ Session::has('cart') ? Session::get('cart')->totalQuantity : '' }}</span>
	                    </a>
                    </div>

                    <div class="hamburger" style="right: 0px">
                        <i class="fa fa-bars trans_200"></i>
                    </div>
                </div>
            </div>
        </div>	
    </nav>

</header>

<div class="menu trans_500">
    <div class="menu_content d-flex flex-column align-items-center justify-content-center text-center">
        <div class="menu_close_container"><div class="menu_close"></div></div>
        @foreach($contactInfo as $item)
            <div class="logo menu_logo"><a href="#"><img src="/photos/logo/{{ $item->site_logo }}" alt=""></a></div>
        @endforeach
        <ul>
            <li class="menu_item"><a href="/">home</a></li>
            <li class="menu_item"><a href="/about-us">about us</a></li>
            {{--<li class="menu_item"><a href="/our-offers">offers</a></li>--}}
            <li class="menu_item"><a href="/our-products">products</a></li>
            <li class="menu_item"><a href="/blog-post">blog</a></li>
            <li class="menu_item"><a href="/contact-us">contact</a></li>
        </ul>
    </div>
</div>
