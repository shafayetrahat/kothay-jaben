@extends ('front-end.layouts.master')

@section ('styles')
	<script src="/js/jquery-3.2.1.min.js"></script>
	<link href="/plugins/colorbox/colorbox.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" id="main-stylesheet" data-version="1.0.0" href="/css/styles/shards-dashboards.1.0.0.min.css">
	<link rel="stylesheet" type="text/css" href="/css/styles/blog_styles.css">
	<link rel="stylesheet" type="text/css" href="/css/styles/blog_responsive.css">

	<style>
		.home_background {
			background: rgba(0, 0, 0, 0.5);
		}
		.price {
			margin-top: 30px;
		}
		.card-title {
			margin-bottom: -15px;
			color: #fff;
		}
		.modal-body {
			color: #000;
		}
		.btn-info {
			margin-top: 22px;
		}
		.card {
			background: linear-gradient(to right, #fa9e1b, #8d4fff);
		}
		button.button_modal {
			position: absolute;
			left: 40%;
			top: 50%;
			display: none;
			transition: all .5s;
		}
		.card-post__image:hover .card-post__category {
			display: none;
		}
		.card-post__image:hover button.button_modal {
			display: block;
		}
		.close {
			cursor: pointer;
		}
		.btn-info, .button_modal {
			color: #fff;
			border-color: #fa9e1b;
			background-color: #fa9e1b;
		}
		.button_modal:hover, .btn-info:hover {
			border-color: #31124b;
			background-color: #31124b;
		}
	</style>
@endsection


@section('content')
	<div class="home">
		<div class="home_background parallax-window" data-parallax="scroll" data-image-src="/images/our_products.jpg"></div>
		<div class="home_content search_car">
			<div class="home_title">our products</div>
		</div>
	</div>

	<!-- Blog -->

	<div class="blog">
		<div class="container">
			<div class="row">
				@foreach ($products as $product)
					<div class="col-lg-4 col-md-4 col-sm-6 mb-4">
						<div class="card card-small card-post card-post--1" id="products">
							<div class="card-post__image" style="background-image: url({{ asset('/photos/products/' . $product->product_image) }});">
								<span class="card-post__category badge badge-pill badge-primary">Quantity: {{ $product->product_quantity }}</span>
								<span class="card-post__category badge badge-pill badge-primary price">Price: ৳ {{ number_format($product->product_price) }}</span>
								<button type="button" class="btn btn-primary button_modal" data-toggle="modal" data-target="#exampleModalCenter">
									Details
								</button>
								<!-- Modal -->
								<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
									<div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLongTitle">{{ $product->product_name }}</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												{!! $product->product_details !!} <br>
												<span class="badge badge-pill badge-primary">Quantity: {{ $product->product_quantity }}</span>
												<span class="badge badge-pill badge-primary price">Price: ৳ {{ number_format($product->product_price) }}</span>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="card-body text-center">
								<h5 class="card-title">
									{{ $product->product_name }}<br>
								</h5>
								<a href="{{ route('product.addToCart', $product->id) }}" type="button" class="btn btn-info">Add to cart</a>
							</div>
						</div>
					</div>
				@endforeach
			</div>
			<div class="row">
				<div class="col-lg-12"></div>
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<p>{!! $products->links() !!}</p>
				</div>
				<div class="col-md-4"></div>
			</div>
		</div>
	</div>
@endsection

@section ('footer')
	<script src="/css/styles/bootstrap4/popper.js"></script>
	<script src="/css/styles/bootstrap4/bootstrap.min.js"></script>
	<script src="/plugins/greensock/TweenMax.min.js"></script>
	<script src="/plugins/greensock/TimelineMax.min.js"></script>
	<script src="/plugins/scrollmagic/ScrollMagic.min.js"></script>
	<script src="/plugins/greensock/animation.gsap.min.js"></script>
	<script src="/plugins/greensock/ScrollToPlugin.min.js"></script>
	<script src="/plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
	<script src="/plugins/easing/easing.js"></script>
	<script src="/plugins/parallax-js-master/parallax.min.js"></script>
	<script src="/js/about_custom.js"></script>
@endsection