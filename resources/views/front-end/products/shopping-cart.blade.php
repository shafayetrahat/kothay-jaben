@extends ('front-end.layouts.master')

@section ('styles')
	<script src="/js/jquery-3.2.1.min.js"></script>
	<link href="/plugins/colorbox/colorbox.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="/css/styles/blog_styles.css">
	<link rel="stylesheet" type="text/css" href="/css/styles/blog_responsive.css">

	<style>
		.home_background {
			background: rgba(0, 0, 0, 0.5);
		}
		.fa-icon {
			font-size: 12px;
		}
		@media only screen and (max-width: 790px) {
			.txt-size {
				font-size: 10px;
			}
		}
	</style>
@endsection


@section('content')
	<div class="home">
		<div class="home_background parallax-window" data-parallax="scroll" data-image-src="/images/our_products.jpg"></div>
		<div class="home_content search_car">
			<div class="home_title">cart details</div>
		</div>
	</div>

	<!-- Cart -->

	<div class="blog">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-sm-8 offset-md-2 offset-sm-2 text-center">
					@if(Session::has('cart'))
						<a href="/our-products" class="btn btn-success mb-5 float-right">Add More</a>
						<table class="table table-striped table-dark">
							<thead>
								<tr>
									<th class="txt-size" scope="col">No. Of Items</th>
									<th class="txt-size" scope="col">Item Name</th>
									<th class="txt-size" scope="col">Reduce/Increase</th>
									<th class="txt-size" scope="col">Item Price</th>
								</tr>
							</thead>
							<tbody>
								@foreach($products as $product)
									<tr>
										<td>{{ $product['qty'] }}</td>
										<td class="txt-size">{{ $product['item']['product_name'] }}</td>
										<td>
											<span class="badge badge-danger">
												<a href="{{ route('product.reduceByOne', $product['item']['id']) }}">
													<i class="fas fa-minus fa-icon"></i>
												</a>
											</span>
											<span class="badge badge-primary">
												<a href="{{ route('product.increaseByOne', $product['item']['id']) }}">
													<i class="fas fa-plus fa-icon"></i>
												</a>
											</span>
										</td>
										<td class="txt-size">৳ {{ $product['product_price'] }}</td>
										<td>
											<span class="badge badge-danger">
												<a href="{{ route('product.removeItem', $product['item']['id']) }}">
													<i class="fas fa-times fa-icon"></i>
												</a>
											</span>
										</td>
									</tr>
								@endforeach
									<tr>
										<td colspan="3"></td>
										<td class="txt-size">Total: ৳ {{ $totalPrice }}</td>
										<td></td>
									</tr>
							</tbody>
						</table>
						<a href="/our-products" class="btn btn-outline-success mt-5">Checkout</a>
					@else
					<div class="col-md-8 col-sm-8 offset-md-2 offset-sm-2 text-center">
						<h2>Your Cart Is Empty</h2>
						<a href="/our-products" class="btn btn-success mt-5">Add something</a>
					</div>
					@endif
				</div>
			</div>
		</div>
	</div>
@endsection

@section ('footer')
	<script src="/css/styles/bootstrap4/popper.js"></script>
	<script src="/css/styles/bootstrap4/bootstrap.min.js"></script>
	<script src="/plugins/greensock/TweenMax.min.js"></script>
	<script src="/plugins/greensock/TimelineMax.min.js"></script>
	<script src="/plugins/scrollmagic/ScrollMagic.min.js"></script>
	<script src="/plugins/greensock/animation.gsap.min.js"></script>
	<script src="/plugins/greensock/ScrollToPlugin.min.js"></script>
	<script src="/plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
	<script src="/plugins/easing/easing.js"></script>
	<script src="/plugins/parallax-js-master/parallax.min.js"></script>
	<script src="/js/about_custom.js"></script>
@endsection