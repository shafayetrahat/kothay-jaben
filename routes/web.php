<?php

/*
|--------------------------------------------------------------------------
| Front End Routes
|--------------------------------------------------------------------------
*/
use App\User;
use Illuminate\Support\Facades\Auth;
// PagesController

/*Route::get('/about-us', 'FrontEndController\PagesController@getAbout');
Route::get('/contact-us', 'FrontEndController\PagesController@getContact');*/
// HotelPackagesController
Route::get('/hotel-packages', 'FrontEndController\PackageController\HotelPackagesController@hotelPackage');
Route::get('/detailed-hotel-package/{id}', 'FrontEndController\PackageController\HotelPackagesController@detailedPackage');
Route::post('/hotel-package-booking', 'FrontEndController\PackageController\HotelPackagesController@hpBook');

// TourPackagesController
Route::get('/tour-packages', 'FrontEndController\PackageController\TourPackagesController@tourPackage');
Route::get('/detailed-tour-package/{id}', 'FrontEndController\PackageController\TourPackagesController@detailedPackage');
Route::post('/tour-package-booking', 'FrontEndController\PackageController\TourPackagesController@tourPBook');

// CarPackagesController
Route::get('/detailed-car-package/{id}', 'FrontEndController\PackageController\CarPackagesController@detailedPackage');
Route::post('/car-package-booking', 'FrontEndController\PackageController\CarPackagesController@carBook');

// BlogController
Route::resource('/blog-post', 'FrontEndController\BlogController');

// IndexHomeController
Route::get('/', 'FrontEndController\IndexHomeController@index');

// CarsController
Route::get('/hiring-type', 'FrontEndController\CarController\CarsController@index');
Route::get('/today-trip-car-details', 'FrontEndController\CarController\CarsController@todayShow');
Route::get('/local-trip-car-details', 'FrontEndController\CarController\CarsController@localShow');
Route::get('/tourist-car-details', 'FrontEndController\CarController\CarsController@touristShow');
Route::get('/hourly-car-details', 'FrontEndController\CarController\CarsController@hourlyShow');
Route::get('/body-contract-car-details', 'FrontEndController\CarController\CarsController@bodyShow');
Route::get('/wedding-car-details', 'FrontEndController\CarController\CarsController@weddingShow');

//CarBookingTypeController
Route::get('/trip-today', 'FrontEndController\CarController\CarBookingTypeController@tripToday');
Route::post('/trip-today-booking', 'FrontEndController\CarController\CarBookingTypeController@tripTodayBooking');

Route::get('/trip-local', 'FrontEndController\CarController\CarBookingTypeController@tripLocal');
Route::post('/trip-local-booking', 'FrontEndController\CarController\CarBookingTypeController@tripLocalBooking');

Route::get('/trip-tourist', 'FrontEndController\CarController\CarBookingTypeController@tripTourist');
Route::post('/trip-tourist/booking', 'FrontEndController\CarController\CarBookingTypeController@tripTouristBooking');

Route::get('/trip-hourly', 'FrontEndController\CarController\CarBookingTypeController@tripHourly');
Route::post('/trip-hourly-booking', 'FrontEndController\CarController\CarBookingTypeController@tripHourlyBooking');

Route::get('/trip-body-contract', 'FrontEndController\CarController\CarBookingTypeController@tripBodyContract');
Route::post('/trip-body-contract/booking', 'FrontEndController\CarController\CarBookingTypeController@tripBodyContractBooking');

Route::get('/hire-wedding', 'FrontEndController\CarController\CarBookingTypeController@hireWedding');
Route::post('/hire-wedding-booking', 'FrontEndController\CarController\CarBookingTypeController@weddingBooking');



// ProductsController
Route::get('/our-products', 'FrontEndController\ProductsController\ProductsController@index');
Route::get('/add-to-cart/{id}', [
    'uses' => 'FrontEndController\ProductsController\ProductsController@getAddToCart',
    'as' => 'product.addToCart'
]);
Route::get('/shopping-cart', [
    'uses' => 'FrontEndController\ProductsController\ProductsController@getCart',
    'as' => 'product.shoppingCart'
]);
Route::get('/reduce/{id}', [
    'uses' => 'FrontEndController\ProductsController\ProductsController@getReduceByOne',
    'as' => 'product.reduceByOne'
]);
Route::get('/increase/{id}', [
    'uses' => 'FrontEndController\ProductsController\ProductsController@getIncreaseByOne',
    'as' => 'product.increaseByOne'
]);
Route::get('/remove/{id}', [
    'uses' => 'FrontEndController\ProductsController\ProductsController@getRemoveItem',
    'as' => 'product.removeItem'
]);

// HotelsController
Route::get('/hotels-list', 'FrontEndController\HotelController\HotelsController@index');
Route::get('/hotels-list/{id}/hotel-rooms', 'FrontEndController\HotelController\HotelsController@showRoom');
Route::get('/hotels-list/{id}/hotel-rooms-details', 'FrontEndController\HotelController\HotelsController@roomDetails');
Route::post('/hotel/booking', 'FrontEndController\HotelController\HotelsController@store');

// Auth Rahat
Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');

Auth::routes();


Route::get('/user-profile', 'HomeController@index')->name('home');
Route::get('/mobile_auth', function () {
    return view('auth.fcm');
});
Route::get('/mobile_auth/confirm', function () {
    return \File::get(public_path() . '/signedIn.blade.php');
});

Route::post('/mobile_auth/confirm', function (\Illuminate\Http\Request $request) {
    if ($request->ajax()) {
        \Illuminate\Support\Facades\Auth::user()->mobile_number = \request('number');
        \Illuminate\Support\Facades\Auth::user()->save();
        return response()->json(\request()->all());
    }
});

Route::get('/admin_home', 'AdminController\AdminController@index')->name('admin_home');

// End Rahat

/*
|--------------------------------------------------------------------------
| Back End Routes
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'admin', 'middleware' => ['role', 'verify']], function () {
    //DashBoardController
    Route::get('dashboard', 'BackEndController\DashboardController\DashBoardController@index');
    Route::get('get-booking-request', 'BackEndController\DashboardController\DashBoardController@getBookingRequest');

    //BlogPostsController
    Route::resource('blog', 'BackEndController\BlogPostsController');

    // SliderController
    Route::get('all-sliders', 'BackEndController\HomeController\SliderController@index');
    Route::get('home', 'BackEndController\HomeController\SliderController@create');
    Route::get('sliders/{id}/edit', 'BackEndController\HomeController\SliderController@edit');
    Route::match(['put', 'patch'], 'slider/{id}', 'BackEndController\HomeController\SliderController@update');
    Route::delete('sliders/{id}', 'BackEndController\HomeController\SliderController@destroy');
    Route::post('home/photos', 'BackEndController\HomeController\SliderController@addSlider');
    Route::delete('home/photos/{id}', 'BackEndController\HomeController\SliderController@destroy');

    // ContactInfoController
    Route::get('home/contact', 'BackEndController\HomeController\ContactInfoController@create');
    Route::post('home/info', 'BackEndController\HomeController\ContactInfoController@store');
    Route::get('home/info/{id}/edit', 'BackEndController\HomeController\ContactInfoController@edit');
    Route::match(['put', 'patch'], 'home/info/{id}', 'BackEndController\HomeController\ContactInfoController@update');

    // CarsController
    Route::resource('cars', 'BackEndController\CarController\CarsController');
    Route::post('cars/photos/{id}', 'BackEndController\CarController\CarsController@addPhoto');
    Route::delete('cars/photos/{id}', 'BackEndController\CarController\CarsController@deletePhoto');
	//TouristTripController
	Route::resource('car/tourist-trip-location', 'BackEndController\CarController\TouristTripController');
	//TouristTripCarPricingController
	Route::resource('car/tourist-trip', 'BackEndController\CarController\TouristTripCarPricingController');
	//BodyContractsController
	Route::resource('car/body-contract-location', 'BackEndController\CarController\BodyContractsController');
	//BodyContractCarPricingController
	Route::resource('car/body-contract', 'BackEndController\CarController\BodyContractCarPricingController');

	// TodayTripCarBookingController
	Route::get('car/today-booking/list/', 'BackEndController\CarController\TodayTripCarBookingController@index');
	Route::get('car/today-booking/list/{book}', 'BackEndController\CarController\TodayTripCarBookingController@show');
	Route::match(['put', 'patch'], 'car/today-booking/list/{id}', 'BackEndController\CarController\TodayTripCarBookingController@confirmedBooking');
	Route::match(['put', 'patch'], 'car/today-booking/list/unconfirmed/{id}', 'BackEndController\CarController\TodayTripCarBookingController@unconfirmedBooking');
	Route::delete('car/today-booking/list/{id}', 'BackEndController\CarController\TodayTripCarBookingController@destroy');

	// LocalTripCarBookingController
	Route::get('car/local-booking/list/', 'BackEndController\CarController\LocalTripCarBookingController@index');
	Route::get('car/local-booking/list/{book}', 'BackEndController\CarController\LocalTripCarBookingController@show');
	Route::match(['put', 'patch'], 'car/local-booking/list/{id}', 'BackEndController\CarController\LocalTripCarBookingController@confirmedBooking');
	Route::match(['put', 'patch'], 'car/local-booking/list/unconfirmed/{id}', 'BackEndController\CarController\LocalTripCarBookingController@unconfirmedBooking');
	Route::delete('car/local-booking/list/{id}', 'BackEndController\CarController\LocalTripCarBookingController@destroy');

	// TouristCarBookingController
	Route::get('car/tourist-booking/list/', 'BackEndController\CarController\TouristCarBookingController@index');
	Route::get('car/tourist-booking/list/{book}', 'BackEndController\CarController\TouristCarBookingController@show');
	Route::match(['put', 'patch'], 'car/tourist-booking/list/{id}', 'BackEndController\CarController\TouristCarBookingController@confirmedBooking');
	Route::match(['put', 'patch'], 'car/tourist-booking/list/unconfirmed/{id}', 'BackEndController\CarController\TouristCarBookingController@unconfirmedBooking');
	Route::delete('car/tourist-booking/list/{id}', 'BackEndController\CarController\TouristCarBookingController@destroy');

	// HourlyTripCarBookingController
	Route::get('car/hourly-booking/list/', 'BackEndController\CarController\HourlyTripCarBookingController@index');
	Route::get('car/hourly-booking/list/{book}', 'BackEndController\CarController\HourlyTripCarBookingController@show');
	Route::match(['put', 'patch'], 'car/hourly-booking/list/{id}', 'BackEndController\CarController\HourlyTripCarBookingController@confirmedBooking');
	Route::match(['put', 'patch'], 'car/hourly-booking/list/unconfirmed/{id}', 'BackEndController\CarController\HourlyTripCarBookingController@unconfirmedBooking');
	Route::delete('car/hourly-booking/list/{id}', 'BackEndController\CarController\HourlyTripCarBookingController@destroy');

	// BodyContractCarBookingController
	Route::get('car/body-contract-booking/list/', 'BackEndController\CarController\BodyContractCarBookingController@index');
	Route::get('car/body-contract-booking/list/{book}', 'BackEndController\CarController\BodyContractCarBookingController@show');
	Route::match(['put', 'patch'], 'car/body-contract-booking/list/{id}', 'BackEndController\CarController\BodyContractCarBookingController@confirmedBooking');
	Route::match(['put', 'patch'], 'car/body-contract-booking/list/unconfirmed/{id}', 'BackEndController\CarController\BodyContractCarBookingController@unconfirmedBooking');
	Route::delete('car/body-contract-booking/list/{id}', 'BackEndController\CarController\BodyContractCarBookingController@destroy');

	// WeddingCarBookingController
	Route::get('car/wedding-booking/list/', 'BackEndController\CarController\WeddingCarBookingController@index');
	Route::get('car/wedding-booking/list/{book}', 'BackEndController\CarController\WeddingCarBookingController@show');
	Route::match(['put', 'patch'], 'car/wedding-booking/list/{id}', 'BackEndController\CarController\WeddingCarBookingController@confirmedBooking');
	Route::match(['put', 'patch'], 'car/wedding-booking/list/unconfirmed/{id}', 'BackEndController\CarController\WeddingCarBookingController@unconfirmedBooking');
	Route::delete('car/wedding-booking/list/{id}', 'BackEndController\CarController\WeddingCarBookingController@destroy');

    //PickUpLocationsController
    Route::get('pick-up/location', 'BackEndController\PlacesController\PickUpLocationsController@pickUpLocation');
    Route::post('pick-up/location/save', 'BackEndController\PlacesController\PickUpLocationsController@storePickUpLocation');
    Route::get('pick-up/location/{id}/edit', 'BackEndController\PlacesController\PickUpLocationsController@editPickUpLocation');
    Route::match(['put', 'patch'], 'pick-up/location/{id}', 'BackEndController\PlacesController\PickUpLocationsController@updatePickUpLocation');
    Route::delete('pick-up/location/{id}', 'BackEndController\PlacesController\PickUpLocationsController@destroy');
    //DropOffLocationsController
    Route::resource('drop-off/location', 'BackEndController\PlacesController\DropOffLocationsController');

    // ProductsController
    Route::resource('products', 'BackEndController\ProductsController\ProductsController');

    // HotelsController
    Route::resource('hotels', 'BackEndController\HotelController\HotelsController');
    //HotelRoomsController
    Route::get('hotels/{hotel}/hotel-room/create', 'BackEndController\HotelController\HotelRoomsController@create');
    Route::post('hotels/{id}/hotel-room/store', 'BackEndController\HotelController\HotelRoomsController@store');
    Route::post('hotels/{id}/hotel-room/addPhoto', 'BackEndController\HotelController\HotelRoomsController@addPhoto');
    Route::get('hotels/{hotelRoom}/hotel-rooms', 'BackEndController\HotelController\HotelRoomsController@show');
    Route::get('hotels/{hotelRoom}/hotel-rooms/edit', 'BackEndController\HotelController\HotelRoomsController@edit');
    Route::match(['put', 'patch'], 'hotels/{id}/hotel-rooms/update', 'BackEndController\HotelController\HotelRoomsController@update');
    Route::delete('hotels/{id}/delete-room', 'BackEndController\HotelController\HotelRoomsController@destroy');
    Route::delete('hotels/{id}/room-photo', 'BackEndController\HotelController\HotelRoomsController@deletePhoto');

    //HotelBookingController
    Route::get('hotel/booking/list/', 'BackEndController\HotelController\HotelBookingController@index');
    Route::get('hotel/booking/list/{book}', 'BackEndController\HotelController\HotelBookingController@show');
	Route::match(['put', 'patch'], 'hotel/booking/list/{id}', 'BackEndController\HotelController\HotelBookingController@confirmedBooking');
	Route::match(['put', 'patch'], 'hotel/booking/list/unconfirmed/{id}', 'BackEndController\HotelController\HotelBookingController@unconfirmedBooking');
	Route::delete('hotel/booking/list/{id}', 'BackEndController\HotelController\HotelBookingController@destroy');

    //TourPackagesController
    Route::resource('packages', 'BackEndController\PackageController\TourPackagesController');
    Route::post('package/{id}/hotel-room/addPhoto', 'BackEndController\PackageController\TourPackagesController@addPhoto');
    Route::delete('package/{id}/room-photo', 'BackEndController\PackageController\TourPackagesController@deletePhoto');
    //TourPackagesBookingController
    Route::get('tour/package-booking/list', 'BackEndController\PackageController\TourPackagesBookingController@index');
    Route::get('tour/package-booking/{id}', 'BackEndController\PackageController\TourPackagesBookingController@show');
	Route::match(['put', 'patch'], 'tour/package-booking/{id}', 'BackEndController\PackageController\TourPackagesBookingController@confirmedBooking');
	Route::match(['put', 'patch'], 'tour/package-booking/unconfirmed/{id}', 'BackEndController\PackageController\TourPackagesBookingController@unconfirmedBooking');
	Route::delete('tour/package-booking/{id}', 'BackEndController\PackageController\TourPackagesBookingController@destroy');

    //HotelPackagesController
    Route::resource('packages-hotels', 'BackEndController\PackageController\HotelPackagesController');
    Route::post('packages-hotels/{id}/hotel-room/addPhoto', 'BackEndController\PackageController\HotelPackagesController@addPhoto');
    Route::delete('packages-hotels/{id}/room-photo', 'BackEndController\PackageController\HotelPackagesController@deletePhoto');
    //HotelPackagesBookingController
    Route::get('hotel/package-booking/list', 'BackEndController\PackageController\HotelPackagesBookingController@index');
    Route::get('hotel/package-booking/{id}', 'BackEndController\PackageController\HotelPackagesBookingController@show');
	Route::match(['put', 'patch'], 'hotel/package-booking/{id}', 'BackEndController\PackageController\HotelPackagesBookingController@confirmedBooking');
	Route::match(['put', 'patch'], 'hotel/package-booking/unconfirmed/{id}', 'BackEndController\PackageController\HotelPackagesBookingController@unconfirmedBooking');
	Route::delete('hotel/package-booking/{id}', 'BackEndController\PackageController\HotelPackagesBookingController@destroy');

    //CarPackagesController
    Route::resource('packages-car', 'BackEndController\PackageController\CarPackagesController');
    Route::post('packages-car/{id}/car-photo/addPhoto', 'BackEndController\PackageController\CarPackagesController@addPhoto');
    Route::delete('packages-car/{id}/car-photo', 'BackEndController\PackageController\CarPackagesController@deletePhoto');
    //CarPackagesBookingController
    Route::get('car/package-booking/list', 'BackEndController\PackageController\CarPackagesBookingController@index');
    Route::get('car/package-booking/{id}', 'BackEndController\PackageController\CarPackagesBookingController@show');
	Route::match(['put', 'patch'], 'car/package-booking/{id}', 'BackEndController\PackageController\CarPackagesBookingController@confirmedBooking');
	Route::match(['put', 'patch'], 'car/package-booking/unconfirmed/{id}', 'BackEndController\PackageController\CarPackagesBookingController@unconfirmedBooking');
	Route::delete('car/package-booking/{id}', 'BackEndController\PackageController\CarPackagesBookingController@destroy');
});
