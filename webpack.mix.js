const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css')
	.styles([
		'public/css/styles/about_responsive.css',
		'public/css/styles/about_styles.css',
		'public/css/styles/blog_responsive.css',
		'public/css/styles/blog_styles.css',
		'public/css/styles/contact_responsive.css',
		'public/css/styles/contact_styles.css',
		'public/css/styles/elements_responsive.css',
		'public/css/styles/elements_styles.css',
		'public/css/styles/extras.1.0.0.min.css',
		'public/css/styles/main.css',
		'public/css/styles/mani_styles.css',
		'public/css/styles/offers_responsive.css',
		'public/css/styles/offers_styles.css',
		'public/css/styles/responsive.css',
		'public/css/styles/single_listing_responsive.css',
		'public/css/styles/single_listing_styles.css',
		'public/css/styles/util.css',
	], 'public/css/kothay_jaben.css');

/*Shards-dashboard wasn't added*/
